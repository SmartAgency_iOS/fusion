//
//  AlertHelper.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/9/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class AlertHelper {
    
    class func show(_ strTitle: String, strMessage: String, strOK: String, caller: UIViewController) {
        let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
        let alertActionOK = UIAlertAction(title: strOK, style: UIAlertActionStyle.default) { (alertAction: UIAlertAction) -> Void in
            
        }
        alert.addAction(alertActionOK)
        
        caller.present(alert, animated: true) { () -> Void in
            
        }
    }
    
}
