//
//  AppDelegate.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/4/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        let counter: Int = 11111
        if UserDefaults.standard.object(forKey: "counter") == nil {
            UserDefaults.standard.set(counter, forKey: "counter")
        }
        
        UserDefaults.standard.synchronize()
        
        application.isStatusBarHidden = true
        return true
    }
    
    func showHomeStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
    }
    
    func showCategoriesStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        
        let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showWineListStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "WineListViewController") as! WineListViewController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showMealsListStoryboard() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = MenuViewController()
        let viewController = storyboard.instantiateViewController(withIdentifier: "MealsViewController") as! MealsViewController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showFavouritesStoryboard() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationFavourites") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showPhilosophyStoryboard() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationPhilosophy") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showGalleryStoryboard() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "NavigationGallery") as! UINavigationController
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showCategoryStoryboard(_ category: SelectedCategory) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // ViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectedCategoryViewController") as! SelectedCategoryViewController
        viewController.category = category
        
        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
            slideMenuController.changeMainViewController(viewController, close: true)
        }
        else {
            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }
    }
    
//    func newsletterStoryboard() {
//        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        
//        // ViewController
//        let menuViewController = storyboard.instantiateViewControllerWithIdentifier("MenuViewController") as! MenuViewController
//        let viewController = storyboard.instantiateViewControllerWithIdentifier("NavigationNewsletter") as! UINavigationController
//        
//        if let slideMenuController = self.window?.rootViewController as? SlideMenuController {
//            slideMenuController.changeMainViewController(viewController, close: true)
//        }
//        else {
//            let slideMenuController = SlideMenuController(mainViewController: viewController, leftMenuViewController: menuViewController)
//            self.window?.rootViewController = slideMenuController
//            self.window?.makeKeyAndVisible()
//        }
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
