//
//  Array+Extensions.swift
//  GuessWhat
//
//  Created by Angel Antonov on 4/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    func arrayRemovingObject(_ object: Element) -> [Element] {
        return filter { $0 != object }
    }
}

extension Array where Element: Equatable {
    mutating func removeItem(_ toRemove: Element) {
        var index = -1
        
        for el in self {
            if el == toRemove {
                index = self.index(of: el)!
                break
            }
        }
        
        if index != -1 {
            self.remove(at: index)
        }
    }
}
