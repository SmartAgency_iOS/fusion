//
//  CGFloat+Extensions.swift
//  GuessWhat
//
//  Created by Angel Antonov on 4/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    var half: CGFloat {
        if self == 0.0 {
            return 0.0
        }
        
        return 0.5 * self
    }
}