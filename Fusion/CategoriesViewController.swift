//
//  CategoryViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class CategoriesViewController: NavigationExtensions {
    
    let kWidth = ScreenWidth * 0.7
    
    var imgViewBackground: UIImageView!
    var btnCocktails: UIButton!
    var btnEntries: UIButton!
    var btnMeals: UIButton!
    var btnDesserts: UIButton!
    var btnWines: UIButton!
    var btnDailyMenu: UIButton!
    var btnNewsletter: UIButton!
    var viewFirstLine: UIView!
    var viewSecondLine: UIView!
    var viewThirdLine: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Background
        let imgBackground = UIImage(named: "background")!
        self.imgViewBackground = UIImageView(image: imgBackground)
        self.imgViewBackground.frame = CGRect(x: 0.0, y: 0.0, width: ScreenWidth, height: ScreenHeight)
        
        self.view.addSubview(imgViewBackground)
        
        // Add navigation bar with menu buttons
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        
        // Cocktails
        self.btnCocktails = UIButton(type: .custom)
        self.btnCocktails.frame = CGRect(x: ScreenWidth /^ kWidth , y: ScreenHeight * 0.18, width: kWidth, height: 40.0)
        self.btnCocktails.setTitle("categories.cocktails".localized(), for: .normal)
        self.btnCocktails.setTitleColor(Colors.GreenOcean, for: UIControlState())
        self.btnCocktails.titleLabel?.font = Fonts.Bold(24.0)
        self.btnCocktails.addTarget(self, action: #selector(CategoriesViewController.openCategory), for: .touchUpInside)
        self.btnCocktails.tag = 101
        
        self.view.addSubview(btnCocktails)
        
        // Entries
        self.btnEntries = UIButton(type: .custom)
        self.btnEntries.frame = CGRect(x: ScreenWidth /^ kWidth, y: self.btnCocktails.bottomYPoint + 30.0, width: kWidth, height: 40.0)
        self.btnEntries.setTitle("categories.entries".localized(), for: .normal)
        self.btnEntries.setTitleColor(Colors.GreenOcean, for: UIControlState())
        self.btnEntries.titleLabel?.font = Fonts.Bold(24.0)
        self.btnEntries.addTarget(self, action: #selector(CategoriesViewController.openCategory), for: .touchUpInside)
        self.btnEntries.tag = 102
        
        self.view.addSubview(btnEntries)
        
        // Meals
        self.btnMeals = UIButton(type: .custom)
        self.btnMeals.frame = CGRect(x: ScreenWidth /^ kWidth, y: self.btnEntries.bottomYPoint + 30.0, width: kWidth, height: 40.0)
        self.btnMeals.setTitle("categories.meals".localized(), for: .normal)
        self.btnMeals.setTitleColor(Colors.GreenOcean, for: UIControlState())
        self.btnMeals.titleLabel?.font = Fonts.Bold(24.0)
        self.btnMeals.addTarget(self, action: #selector(CategoriesViewController.openMealsList(_:)), for: .touchUpInside)
        
        self.view.addSubview(btnMeals)
        
        // Desserts
        self.btnDesserts = UIButton(type: .custom)
        self.btnDesserts.frame = CGRect(x: ScreenWidth /^ kWidth, y: self.btnMeals.bottomYPoint + 30.0, width: kWidth, height: 40.0)
        self.btnDesserts.setTitle("categories.desserts".localized(), for: .normal)
        self.btnDesserts.setTitleColor(Colors.GreenOcean, for: UIControlState())
        self.btnDesserts.titleLabel?.font = Fonts.Bold(24.0)
        self.btnDesserts.addTarget(self, action: #selector(CategoriesViewController.openCategory), for: .touchUpInside)
        self.btnDesserts.tag = 103
        
        self.view.addSubview(btnDesserts)
        
        // Vertical line
        viewFirstLine = UIView(frame: CGRect(x: ScreenWidth /^ 2.0, y: self.btnDesserts.bottomYPoint + 30.0, width: 2.0, height: 60.0))
        viewFirstLine.backgroundColor = Colors.GreenOcean
        
        self.view.addSubview(viewFirstLine)
        
        // Wines
        self.btnWines = UIButton(type: .custom)
        self.btnWines.frame = CGRect(x: ScreenWidth /^ kWidth, y: self.viewFirstLine.bottomYPoint + 30.0, width: kWidth, height: 40.0)
        self.btnWines.setTitle("categories.wines".localized(), for: .normal)
        self.btnWines.setTitleColor(Colors.GreenOcean, for: UIControlState())
        self.btnWines.titleLabel?.font = Fonts.Bold(24.0)
        self.btnWines.addTarget(self, action: #selector(CategoriesViewController.openWineList(_:)), for: .touchUpInside)
        
        
        self.view.addSubview(btnWines)
        
        // Second line
        viewSecondLine = UIView(frame: CGRect(x: ScreenWidth /^ 2.0, y: self.btnWines.bottomYPoint + 30.0, width: 2.0, height: 60.0))
        viewSecondLine.backgroundColor = Colors.GreenOcean
        
        self.view.addSubview(viewSecondLine)
        
        // Daily menu
        self.btnDailyMenu = UIButton(type: .custom)
        self.btnDailyMenu.frame = CGRect(x: ScreenWidth /^ kWidth, y: self.viewSecondLine.bottomYPoint + 30.0, width: kWidth, height: 40.0)
        self.btnDailyMenu.setTitle("categories.daily.menu".localized(), for: .normal)
        self.btnDailyMenu.setTitleColor(Colors.GreenOcean, for: UIControlState())
        self.btnDailyMenu.titleLabel?.font = Fonts.Bold(24.0)
        self.btnDailyMenu.addTarget(self, action: #selector(CategoriesViewController.openCategory), for: .touchUpInside)
        self.btnDailyMenu.tag = 104
        
        self.view.addSubview(btnDailyMenu)
        
        // Third line
        viewThirdLine = UIView(frame: CGRect(x: ScreenWidth /^ 2.0, y: self.btnDailyMenu.bottomYPoint + 30.0, width: 2.0, height: 60.0))
        viewThirdLine.backgroundColor = Colors.GreenOcean
        
        self.view.addSubview(viewThirdLine)
        
        // Newsletter
        self.btnNewsletter = UIButton(type: .custom)
        self.btnNewsletter.frame = CGRect(x: ScreenWidth /^ kWidth, y: self.viewThirdLine.bottomYPoint + 30.0, width: kWidth, height: 40.0)
        self.btnNewsletter.setTitle("categories.newsletter".localized(), for: .normal)
        self.btnNewsletter.setTitleColor(Colors.GreenOcean, for: UIControlState())
        self.btnNewsletter.titleLabel?.font = Fonts.Bold(24.0)
        self.btnNewsletter.addTarget(self, action: #selector(CategoriesViewController.openNewsletter), for: .touchUpInside)
        
        self.view.addSubview(btnNewsletter)
        
    }
    
    func openNewsletter(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "OpenNewsletter", sender: sender)
    }
    
    func openCategory(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "SelectedCategory", sender: sender)
    }
    
    func openWineList(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "OpenWineList", sender: sender)
    }
    
    func openMealsList(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "OpenMealsList", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectedCategory" {
            let destinationVC = segue.destination as! SelectedCategoryViewController
            switch (sender! as AnyObject).tag {
            case 101:
                destinationVC.category = SelectedCategory.Cocktails
            case 102:
                destinationVC.category = SelectedCategory.Entries
            case 103:
                destinationVC.category = SelectedCategory.Desserts
            case 104:
                destinationVC.category = SelectedCategory.DailyMenu
            default:
                destinationVC.category = SelectedCategory.Cocktails
            }
            
        }
    }
    
}
