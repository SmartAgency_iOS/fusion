//
//  Category.swift
//  Fusion
//
//  Created by Admin on 8/18/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class Category {
    
    var descr: String!
    var descrEN: String!
    var photo1: Photo!
    var photo2: Photo!
    var subtitle: String!
    var subtitleEN: String!
    var title: String!
    var titleEN: String!
    
    class func parse(_ dictCategory: [String: AnyObject]) -> Category {
        let category = Category()
        
        if isNotNull(dictCategory[WS.Params.title]) {
            category.title = dictCategory[WS.Params.title] as! String
        }
        
        if isNotNull(dictCategory[WS.Params.titleEN]) {
            category.titleEN = dictCategory[WS.Params.titleEN] as! String
        }
        
        if isNotNull(dictCategory[WS.Params.description]) {
            category.descr = dictCategory[WS.Params.description] as! String
        }
        
        if isNotNull(dictCategory[WS.Params.descriptionEN]) {
            category.descrEN = dictCategory[WS.Params.descriptionEN] as! String
        }
        
        if isNotNull(dictCategory[WS.Params.subtitle]) {
            category.subtitle = dictCategory[WS.Params.subtitle] as! String
        }
        
        if isNotNull(dictCategory[WS.Params.subtitleEN]) {
            category.subtitleEN = dictCategory[WS.Params.subtitleEN] as! String
        }
        
        if isNotNull(dictCategory[WS.Params.photo1]) {
            category.photo1 = Photo.parseURL(dictCategory[WS.Params.photo1] as! String)
        }
        
        if isNotNull(dictCategory[WS.Params.photo2]) {
            category.photo2 = Photo.parseURL(dictCategory[WS.Params.photo2] as! String)
        }
        
        return category
    }
    
}
