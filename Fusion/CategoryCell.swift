//
//  CategoryCell.swift
//  Fusion
//
//  Created by Admin on 8/10/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    var category: SelectedCategory!
    
    var imgViewFav: FavouriteView!
    var lblTitle: UILabel!
    var lblDescription: UILabel!
    var lblPrice: UILabel!
    var btnDetails: UIButton!
    var menuItem: MenuItem
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, category: SelectedCategory, item: MenuItem) {
        self.menuItem = item
        
        super.init(style: style, reuseIdentifier: "CategoryCell")
        self.backgroundColor = Colors.Dark
        self.category = category
        self.selectionStyle = .none
        
        // Add to favourites
        imgViewFav = FavouriteView(coordinates: CGPoint(x: 30.0, y: 5.0), item: self.menuItem)
        imgViewFav.frame.size = CGSize(width: imgViewFav.frame.size.width + 12.0, height: imgViewFav.frame.size.height + 12.0)
        
        self.addSubview(imgViewFav)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 15.0, y: 8.0, width: ScreenWidth * 0.6, height: 22.0))
        if language == "en" {
            lblTitle.text = menuItem.titleEN != nil ? menuItem.titleEN.frenchalize : ""
        } else {
            lblTitle.text = menuItem.title != nil ? menuItem.title.frenchalize : ""
        }
        lblTitle.font = Fonts.Medium(20.0)
        lblTitle.textAlignment = .left
        lblTitle.textColor = Colors.White
        lblTitle.adjustHeight()
        
        self.addSubview(lblTitle)
        
        // Price
        lblPrice = UILabel(frame: CGRect(x: ScreenWidth - 30.0 - 80.0, y: 8.0, width: 80.0, height: 22.0))
        lblPrice.text = menuItem.price != nil ? String(format: "\(menuItem.price!.cleanValue)") : ""
        lblPrice.font = Fonts.Medium(20.0)
        lblPrice.textAlignment = .right
        lblPrice.textColor = Colors.White
        lblPrice.adjustsFontSizeToFitWidth = true
        
        self.addSubview(lblPrice)
        
        // Description
        lblDescription = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 17.0, y: lblTitle.bottomYPoint + 5.0, width: ScreenWidth * 0.85, height: 34.0))
        lblDescription.font = Fonts.Regular(16.0)
        if language == "en" {
            lblDescription.text = menuItem.excerptEN != nil ? menuItem.excerptEN.frenchalize : ""
        } else {
            lblDescription.text = menuItem.excerpt != nil ? menuItem.excerpt.frenchalize : ""
        }
        
        lblDescription.numberOfLines = 2
        lblDescription.textAlignment = .left
        lblDescription.textColor = Colors.White
        
        self.addSubview(lblDescription)
        
        // Details button
        btnDetails = UIButton(type: .custom)
        btnDetails.frame = CGRect(x: imgViewFav.mostRightPoint + 5.0, y: 78.0, width: 100.0, height: 30.0)
        let attributes = [
            NSFontAttributeName : Fonts.Medium(18.0),
            NSForegroundColorAttributeName : Colors.GreenOcean,
            NSUnderlineStyleAttributeName : 1
        ] as [String : Any]
        let strButtonTitle = NSMutableAttributedString(string: "favourites.details".localized(), attributes: attributes)
        btnDetails.setAttributedTitle(strButtonTitle, for: .normal)
        btnDetails.addTarget(self, action: #selector(self.openDetails), for: .touchUpInside)
        if "language".localized() == "fr" {
            btnDetails.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -40.0, bottom: 0.0, right: 0.0)
        }
        btnDetails.titleLabel?.textAlignment = .left
        self.addSubview(btnDetails)
        
        if category != SelectedCategory.Cocktails {
            self.addSubview(btnDetails)
        }
        
        let viewLine = UIView(frame: CGRect(x: 30.0, y: CategoryCell.cellHeight - 1.0, width: ScreenWidth - 60.0, height: 1.0))
        viewLine.backgroundColor = Colors.White
        
        self.addSubview(viewLine)
        
    }
    
    func openDetails() {
        if self.parentViewController?.navigationController != nil {
            self.parentViewController?.performSegue(withIdentifier: "DetailsFromCategory", sender: menuItem)
        } else {
            let detailsVC = self.parentViewController?.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
            detailsVC.menuItem = menuItem
            detailsVC.modalTransitionStyle = .crossDissolve
            detailsVC.fromCategory = true
            let menuController = MenuViewController()
            let slideViewController = SlideMenuController(mainViewController: detailsVC, leftMenuViewController: menuController)
            slideViewController.modalTransitionStyle = .crossDissolve
            self.parentViewController!.present(slideViewController, animated: true, completion: nil)
        }
    }
    
    class var cellHeight: CGFloat {
        return 108.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension Double {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f.-", self) : String(self)
    }
}

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let detailsVC = storyboard.instantiateViewControllerWithIdentifier("DetailsViewController") as! DetailsViewController
//        detailsVC.menuItem = self.menuItem
//        detailsVC.fromCategory = true
//        detailsVC.categoryy = category
//        detailsVC.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
//
//        let menuViewController = MenuViewController()
//        let slideMenuController = SlideMenuController(mainViewController: detailsVC, leftMenuViewController: menuViewController)
//        self.window?.rootViewController = slideMenuController
//        self.window?.makeKeyAndVisible()
//
//        self.parentViewController?.presentViewController(detailsVC, animated: true, completion: nil)
