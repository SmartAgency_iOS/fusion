//
//  CategoryHeaderView.swift
//  Fusion
//
//  Created by Admin on 8/10/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

@objc protocol CategoryDelegate: class {
    @objc optional func slideBottom()
    func slide()
}

class CategoryHeaderView: UIView {
    
    var lblTitle: UILabel!
    var lblSubtitle: UILabel!
    var lblDescription: UILabel!
    var imgViewPortrait: UIImageView!
    var imgViewLandscape: UIImageView!
    var lblSlide: UILabel!
    var btnSlide: UIButton!
    
    var category: SelectedCategory!
    
    weak var delegate: CategoryDelegate?
    
    init(selectedCategory: SelectedCategory) { // TODO: add images to init
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight - Constants.NavBarHeight))
        
        self.category = selectedCategory
        self.backgroundColor = Colors.Black
        
        // Portrait image
        let imgPortrait = UIImage(named: "portrait_image_example")!
        imgViewPortrait = UIImageView(frame: CGRect(x: 10.0 + ScreenWidth - imgPortrait.size.width, y: -10.0, width: imgPortrait.size.width, height: imgPortrait.size.height))
        imgViewPortrait.layer.borderWidth = 10.0
        imgViewPortrait.layer.borderColor = Colors.Black.cgColor
        
        self.addSubview(imgViewPortrait)
        
        // Landscape image
        let imgLandscape = UIImage(named: "landscape")!
        imgViewLandscape = UIImageView(frame: CGRect(x: ScreenWidth * 0.1, y: imgViewPortrait.bottomYPoint - imgLandscape.size.height - 25.0, width: imgLandscape.size.width, height: imgLandscape.size.height))
        imgViewLandscape.layer.borderWidth = 10.0
        imgViewLandscape.layer.borderColor = Colors.Black.cgColor
        
        self.addSubview(imgViewLandscape)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: 50.0, width: ScreenWidth - imgViewPortrait.frame.origin.x - 20.0, height: 86.0))
        lblTitle.font = Fonts.Bold(42.0)
        lblTitle.textColor = Colors.White
        lblTitle.textAlignment = .left
        lblTitle.numberOfLines = 2
        
        self.addSubview(lblTitle)
        
        // Subtitle
        lblSubtitle = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: lblTitle.bottomYPoint + 4.0, width: ScreenWidth - imgViewPortrait.frame.origin.x - 20.0, height: 24.0))
        lblSubtitle.font = Fonts.Medium(22.0)
        lblSubtitle.textColor = Colors.LighterGray
        lblSubtitle.textAlignment = .left
        
        self.addSubview(lblSubtitle)
        
        // Description
        lblDescription = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: lblSubtitle.bottomYPoint + 6.0, width: ScreenWidth - imgViewPortrait.frame.origin.x - 20.0, height: imgViewLandscape.frame.origin.y - lblSubtitle.bottomYPoint - 10.0))
        lblDescription.font = Fonts.Medium(18.0)
        lblDescription.textColor = Colors.White
        lblDescription.textAlignment = .left
        lblDescription.numberOfLines = 0
        lblDescription.adjustsFontSizeToFitWidth = true
        
        self.addSubview(lblDescription)
        
        // Slide label
        lblSlide = UILabel(frame: CGRect(x: 0.0, y: imgViewPortrait.bottomYPoint + 48.0, width: ScreenWidth, height: 16.0))
        lblSlide.font = Fonts.Medium(14.0)
        lblSlide.textColor = Colors.White
        lblSlide.textAlignment = .center
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CategoryHeaderView.slide))
        lblSlide.addGestureRecognizer(tapGesture)
        lblSlide.isUserInteractionEnabled = true
        
        self.addSubview(lblSlide)
        
        // Slide button
        btnSlide = UIButton(type: .custom)
        let imgSlide = UIImage(named: "btn_arrow")!
        btnSlide.setImage(imgSlide, for: UIControlState())
        btnSlide.frame = CGRect(x: ScreenWidth /^ imgSlide.size.width, y: lblSlide.bottomYPoint + 4.0, width: imgSlide.size.width, height: imgSlide.size.height)
        btnSlide.addTarget(self, action: #selector(CategoryHeaderView.slide), for: .touchUpInside)
        
        self.addSubview(btnSlide)
        
        self.update()
    }
    
    func updateInfo(_ title: String, subtitle: String, description: String, image1: UIImage?, image2: UIImage?) {
        lblDescription.text = description
        if image1 != nil {
            imgViewPortrait.image = image1
        }
        if image2 != nil {
           imgViewLandscape.image = image2
        }
    }
    
    func update() {
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            lblTitle.text = "category.cocktails".localized()
            lblSlide.text = "slider.cocktails".localized()
        case SelectedCategory.Desserts.rawValue:
            lblTitle.text = "category.desserts".localized()
            lblSlide.text = "slider.desserts".localized()
        case SelectedCategory.Entries.rawValue:
            lblTitle.text = "category.entries".localized()
            lblSlide.text = "slider.entries".localized()
        case SelectedCategory.DailyMenu.rawValue:
            lblTitle.text = "category.daily.menu".localized()
            lblSlide.text = "slider.daily.menu".localized()
        case SelectedCategory.Wines.rawValue:
            lblTitle.text = "category.wines".localized()
            lblSlide.text = "slider.wines".localized()
        case SelectedCategory.Meals.rawValue:
            if self.tag == 101 {
                lblTitle.text = "category.meals2".localized()
                lblSubtitle.text = "main.courses.subtitle2".localized()
                lblSlide.text = "slider.main.courses2".localized()
            } else {
                lblTitle.text = "category.meals1".localized()
                lblSubtitle.text = "main.courses.subtitle1".localized()
                lblSlide.text = "slider.main.courses1".localized()
            }
        default:
            print("default")
        }
    }

    func slide() {
        if self.tag == 101 {
            self.delegate?.slideBottom!()
        } else {
            self.delegate?.slide()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}
