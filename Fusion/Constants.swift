//
//  Constants.swift
//  GuessWhat
//
//  Created by Angel Antonov on 8/31/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

import UIKit
import MapKit

let ScreenWidth = UIScreen.main.bounds.width
let ScreenHeight = UIScreen.main.bounds.height
let isiPhone4 = ScreenHeight == 480.0
let isiPhone5 = ScreenHeight == 568.0
let isiPhone6 = ScreenHeight == 667.0
let isiPhone6Plus = ScreenHeight == 736.0
// TODO: Add options for iPad

let isFrench = "language".localized == "fr"
let isEnglish = "language".localized == "en"
var dataLoaded = false

func showAlert(_ title: String, message: String, vc: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: { (action) in
        
    }))
    
    vc.present(alert, animated: true) {
        
    }
}

var appDelegate = (UIApplication.shared.delegate as! AppDelegate)
var language: String = "en"

/**
 Returns the installation date of the app.
 Retrieving it from the creation date of the Documents directory of the app.
 */
var appInstallationDate: Date {
    let urlToDocumentsFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
    do {
        let installDate = try FileManager.default.attributesOfItem(atPath: (urlToDocumentsFolder!.path))[FileAttributeKey.creationDate] as! Date
        
        return installDate
    }
    catch {
        
    }
    
    return Date()
}

var arrFavourites: [MenuItem] = [MenuItem]()
var arrCachedPhotos: [Photo] = [Photo]()
var arrCachedGalleryImages: [UIImage] = [UIImage]()
var dictRestaurantImages: [String: UIImage] = [String: UIImage]()
var dictCocktailsImages: [String: UIImage] = [String: UIImage]()
var dictEntriesImages: [String: UIImage] = [String: UIImage]()
var dictDessertsImages: [String: UIImage] = [String: UIImage]()
var dictWinesImages: [String: UIImage] = [String: UIImage]()
var dictDailyMenuImages: [String: UIImage] = [String: UIImage]()
var dictMealsImages: [String: UIImage] = [String: UIImage]()

func isNotNull(_ it: AnyObject?) -> Bool {
    if it == nil {
        return false
    }
    if it! as! NSObject == NSNull() {
        return false
    }
    
    if let itStr = it as? String {
        if itStr == "" {
            return false
        }
        else if itStr == "<null>" {
            return false
        }
    }
    
    return true
}

// MARK: - Degrees and Radians

let π:CGFloat = CGFloat(M_PI)

func degreesToRadians(_ degrees: CGFloat) -> CGFloat {
    return (degrees + 90.0) * π / 180.0
}

func radiansToDegrees(_ radians: CGFloat) -> CGFloat {
    return radians * 180.0 / π
}

// MARK: - (Un)Hide Views

func hideViews(_ someViews: UIView...) {
    for view in someViews {
        view.isHidden = true
    }
}

func unhideViews(_ someViews: UIView...) {
    for view in someViews {
        view.isHidden = false
    }
}

// MARK: - WS Constants

enum OperationResult: Int {
    case success = 200
    case server_ERROR = 300
}

enum Language: String {
    case English
    case French
}

public enum OperationType: String {
    case COCKTAILS_AND_APPETIZERS = "cocktails-and-appetizers"
    case STARTERS = "starters"
    case MAIN_COURSES = "main-courses"
    case DESSERTS = "desserts"
    case WINES = "wines"
    case DAILY_MENU = "daily-menu"
    case GALLERY_PHOTOS = "gallery-photos"
    case RESTAURANT_INFO = "restaurant-info"
    case ALL_DATA = "all-data"
}

struct WS {
    struct Params {
        static let statusCode = "status_code"
        static let title = "title"
        static let titleEN = "titleEN"
        static let category = "category"
        static let price = "price"
        static let excerpt = "excerpt"
        static let excerptEN = "excerptEN"
        static let wineSuggestionDescription = "wine_suggestion_description"
        static let wineSuggestionDescriptionEN = "wine_suggestion_descriptionEN"
        static let wineSuggestionTitle = "wine_suggestion_title"
        static let wineSuggestionTitleEN = "wine_suggestion_titleEN"
        static let wineSuggestionPhoto = "wine_suggestion_photo"
        static let wineSuggestionID = "wine_suggestion_id"
        static let wineSuggestionPrice = "wine_suggestion_price"
        static let wineSuggestionPriceBottle = "wine_suggestion_price_bottle"
        static let wineSuggestionPriceGlass = "wine_suggestion_price_glass"
        static let description = "description"
        static let descriptionEN = "descriptionEN"
        static let photo1 = "photo1"
        static let photo2 = "photo2"
        static let subtitle = "subtitle"
        static let subtitleEN = "subtitleEN"
        static let id = "id"
        static let photoURL = "photo"
        static let bottlePrice = "bottle_price"
        static let glassPrice = "glass_price"
        static let cellar = "cellar"
        static let cellarEN = "cellarEN"
        static let color = "colour"
        static let country = "country"
        static let image = "image"
    }
}

// MARK: - Colors

func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
    return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: 1.0)
}

func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {
    return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
}

func rgb(_ rgb: CGFloat) -> UIColor {
    return UIColor(red: rgb / 255.0, green: rgb / 255.0, blue: rgb / 255.0, alpha: 1.0)
}

func rgb(_ rgb: CGFloat, _ alpha: CGFloat) -> UIColor {
    return UIColor(red: rgb / 255.0, green: rgb / 255.0, blue: rgb / 255.0, alpha: alpha)
}

struct Colors {
    
    static let White = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    static let Black = UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    static let GreenOcean = rgb(21.0, 163.0, 156.0)
    static let Dark = rgb(28.0, 29.0, 30.0)
    static let DarkGray = rgb(44.0, 45.0, 46.0)
    static let LightGray = rgb(58.0, 59.0, 60.0)
    static let LighterGray = rgb(161.0, 161.0, 161.0)
}

// MARK: - Constants

struct URL {
    
}

struct Constants {
    
    static let NavBarHeight: CGFloat = 78.0//isiPhone6Plus ? 90.0 : isiPhone6 ? 73.0 : 68.0
    static let SuggestionViewHeight: CGFloat = 220.0
    
    struct Notifications {
        static let REFRESH_HOME = "REFRESH_HOME"
        static let LOGOUT = "LOGOUT"
        static let REFRESH_NOTIFICATION_CELL = "REFRESH_NOTIFICATION_CELL"
        static let HOME_SHOWN = "HOME_SHOWN"
    }
    
    struct DateFormats {
        static let Server = "EEE, dd MMM yyyy HH:mm:ss ZZZ"
        static let BirthDate = "dd/MM/yyyy"
    }
}

struct Sizes {
}

// MARK: - Fonts

class Fonts {
    class func Regular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gotham-Light", size: size)!
    }
    
    class func Medium(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gotham-Medium", size: size)!
    }
    
    class func Bold(_ size: CGFloat) -> UIFont {
        return UIFont(name: "Gotham-Bold", size: size)!
    }
    
    
    class func _printall() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("Font Names = [\(names)]")
        }
    }
}

