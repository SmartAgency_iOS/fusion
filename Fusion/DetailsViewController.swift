//
//  DetailsViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class DetailsViewController: NavigationExtensions {
    
    var scrollView: UIScrollView?
    var imgViewItem: UIImageView!
    var lblTitle: UILabel!
    var imgViewFav: FavouriteView!
    var lblFavourite: UILabel!
    var lblExcerpt: UILabel!
    var lblDescription: UILabel!
    var lblPriceDescription: UILabel!
    var lblPrice: UILabel!
    var viewLine: UIView!
    var menuItem: MenuItem!
    var suggestionView: SuggestionView?
    var suggestion: (String, String, UIImage, Int, Double) = ("", "", UIImage(), -999, 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        self.view.backgroundColor = Colors.Black
        
        if menuItem.hasSuggestion == true {
            if language == "en" {
                self.suggestion.0 = menuItem.wineSuggestionTitleEN != nil ? menuItem.wineSuggestionTitleEN : ""
                self.suggestion.1 = menuItem.wineSuggestionDescriptionEN != nil ? menuItem.wineSuggestionDescriptionEN : ""
            } else {
                self.suggestion.0 = menuItem.wineSuggestionTitle != nil ? menuItem.wineSuggestionTitle.frenchalize : ""
                self.suggestion.1 = menuItem.wineSuggestionDescription != nil ? menuItem.wineSuggestionDescription.frenchalize : ""
            }
            
            self.suggestion.3 = menuItem.wineSuggestionID != nil ? menuItem.wineSuggestionID : -999
            self.suggestion.4 = menuItem.wineSuggestionPrice != nil ? menuItem.wineSuggestionPrice : menuItem.wineSuggestionPriceBottle != nil ? menuItem.wineSuggestionPriceBottle : 0.0
            if menuItem.wineSuggestionPhoto != nil {
                if menuItem.wineSuggestionImage != nil {
                    self.suggestion.2 = menuItem.wineSuggestionImage
                    self.suggestionView?.imgViewItem.image = menuItem.wineSuggestionImage
                } else {
                    let photo = Photo.parseURL(menuItem.wineSuggestionPhoto)
                    photo.getImage({ (downloadedImage) in
                        if downloadedImage != nil {
                            self.suggestion.2 = downloadedImage!
                            self.suggestionView?.imgViewItem.image = downloadedImage
                        }
                    })
                }
//                let photo = Photo.parseURL(menuItem.wineSuggestionPhoto)
//                photo.getImage({ (downloadedImage) in
//                    if downloadedImage != nil {
//                        self.suggestion.2 = downloadedImage
//                        self.suggestionView?.imgViewItem.image = downloadedImage
//                        if downloadedImage != nil && self.menuItem.id != nil {
//                            self.cacheSuggestionImage(downloadedImage!, id: self.menuItem.id)
//                        }
//                    }
//                })
            }
        }
        
        // Item image
        let imgItem = UIImage(named: "detail_image_example")!
        imgViewItem = UIImageView(frame: CGRect(x: 0.0, y: Constants.NavBarHeight, width: ScreenWidth, height: imgItem.size.height))
       // if menuItem.realImage != nil {
          //  imgViewItem.image = menuItem.realImage
        //} else {
            if let img = menuItem.image {
                if let imaage = img.getImage({ (downloadedImage) in
                    self.imgViewItem.image = downloadedImage
                }) {
                    self.imgViewItem.image = imaage
                }
            }
        //}

        
        self.view.addSubview(imgViewItem)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: 16.0, y: imgViewItem.bottomYPoint + 20.0, width: ScreenWidth * 0.8, height: 52.0))
        if language == "en" {
            lblTitle.text = menuItem.titleEN != nil ? menuItem.titleEN : ""
        } else {
            lblTitle.text = menuItem.title != nil ? menuItem.title : ""
        }
        
        lblTitle.textColor = Colors.White
        lblTitle.textAlignment = .left
        lblTitle.font = Fonts.Bold(40.0)
        lblTitle.adjustsFontSizeToFitWidth = true
        
        self.view.addSubview(lblTitle)
        
        // Add to favourites image
        imgViewFav = FavouriteView(coordinates: CGPoint(x: 16.0, y: lblTitle.bottomYPoint + 8.0), item: menuItem)
        imgViewFav.frame.size = CGSize(width: imgViewFav.frame.size.width + 12.0, height: imgViewFav.frame.size.height + 12.0)
        
        self.view.addSubview(imgViewFav)
        
        // Add to favourites label
        lblFavourite = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 15.0, y: imgViewFav.frame.origin.y + 4.0, width: ScreenWidth * 0.7, height: 16.0))
        lblFavourite.text = String(format: "details.add.to.favourites".localized())
        lblFavourite.textColor = Colors.LightGray
        lblFavourite.textAlignment = .left
        lblFavourite.font = Fonts.Medium(14.0)
        
        self.view.addSubview(lblFavourite)
        
        // Excerpt
        lblExcerpt = UILabel(frame: CGRect(x: 16.0, y: lblFavourite.bottomYPoint + 18.0, width: ScreenWidth - 32.0, height: 20.0))
        if language == "en" {
            lblExcerpt.text = menuItem.excerptEN != nil ? menuItem.excerptEN : ""
        } else {
            lblExcerpt.text = menuItem.excerpt != nil ? menuItem.excerpt.frenchalize : ""
        }
        
        lblExcerpt.textColor = Colors.White
        lblExcerpt.textAlignment = .left
        lblExcerpt.font = Fonts.Regular(18.0)
        lblExcerpt.adjustHeight()
        
        self.view.addSubview(lblExcerpt)
        
        // Description
        lblDescription = UILabel(frame: CGRect(x: 16.0, y: lblExcerpt.bottomYPoint + 10.0, width: ScreenWidth - 32.0, height: 20.0))
        if language == "en" {
            lblDescription.text = menuItem.descrEN != nil ? menuItem.descrEN : ""
        } else {
            lblDescription.text = menuItem.descr != nil ? menuItem.descr.frenchalize : ""
        }
        
        lblDescription.textColor = Colors.White
        lblDescription.textAlignment = .left
        lblDescription.font = Fonts.Regular(18.0)
        lblDescription.adjustHeight()
        
        self.view.addSubview(lblDescription)
        
        // Price
        lblPrice = UILabel(frame: CGRect(x: ScreenWidth - 110.0, y: lblDescription.bottomYPoint + 60.0, width: 100.0, height: 32.0))
        lblPrice.text = menuItem.price != nil ? "\(menuItem.price!.cleanValue)" : ".-"
        lblPrice.textColor = Colors.White
        lblPrice.textAlignment = .right
        lblPrice.font = Fonts.Bold(30.0)
        
        self.view.addSubview(lblPrice)
        
        // Price description
        lblPriceDescription = UILabel(frame: CGRect(x: lblPrice.frame.origin.x - 300.0, y: lblPrice.bottomYPoint - 18.0, width: 290.0, height: 14.0))
        lblPriceDescription.text = String(format: "details.price.description".localized())
        lblPriceDescription.textColor = Colors.LightGray
        lblPriceDescription.textAlignment = .right
        lblPriceDescription.font = Fonts.Medium(12.0)
        
        self.view.addSubview(lblPriceDescription)
        
        // End of description line
        viewLine = UIView(frame: CGRect(x: 16.0, y: lblPriceDescription.bottomYPoint + 10.0, width: ScreenWidth - 22.0, height: 2.0))
        viewLine.backgroundColor = Colors.LightGray
        
        self.view.addSubview(viewLine)
        
        // TODO: Check if there is a suggestion and add
        if menuItem.hasSuggestion == true {
            self.addSuggestionView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if suggestionView != nil {
            if self.menuItem != nil {
                if self.menuItem.wineSuggestionID != nil {
                    suggestionView?.update(id: self.menuItem.wineSuggestionID)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        var dictInfo = Dictionary<String, AnyObject>()
        dictInfo["menuItem"] = self.menuItem
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "REFRESH_ITEM"), object: nil, userInfo: dictInfo)
    }
    
    func addSuggestionView() {
        suggestionView = SuggestionView(frame: CGRect(x: 0.0, y: viewLine.bottomYPoint + 5.0, width: ScreenWidth, height: Constants.SuggestionViewHeight), suggestion: suggestion)
//        if menuItem.id != nil {
//            self.loadSuggestionImage(menuItem.id)
//        }
        self.view.addSubview(suggestionView!)
    }
    
    func loadDetailImage(_ id: Int) {
        var image1: UIImage?
        let userDefaults = UserDefaults.standard
        if let imagee1 = userDefaults.object(forKey: "detail\(id)") as? Data {
            image1 = UIImage(data: imagee1)
            
        }
        self.imgViewItem.image = image1
    }
    
    func loadSuggestionImage(_ id: Int) {
        var image2: UIImage?
        let userDefaults = UserDefaults.standard
        if let imagee2 = userDefaults.object(forKey: "suggestion\(id)") as? Data {
            image2 = UIImage(data: imagee2)
            
        }
        self.suggestionView?.imgViewItem.image = image2
    }
    
    func cacheDetailImage(_ image: UIImage, id: Int) {
        let userDefaults = UserDefaults.standard
        let data = UIImagePNGRepresentation(image)
        userDefaults.set(data, forKey: "detail\(id)")
    }
    
    func cacheSuggestionImage(_ image: UIImage, id: Int) {
        let userDefaults = UserDefaults.standard
        let data = UIImagePNGRepresentation(image)
        userDefaults.set(data, forKey: "suggestion\(id)")
    }
    
}
