//
//  Dictionary+Extensions.swift
//  GuessWhat
//
//  Created by Angel Antonov on 4/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation

extension NSDictionary {
    func printJson() {
        if let dict = self as? Dictionary<String, AnyObject> {
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions(rawValue: UInt.allZeros))
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    print(string)
                }
            } catch {
                print(error)
            }
        }
    }
}

extension Dictionary {
    func printJson() {
        if let dict = (self as? AnyObject) as? Dictionary<String, AnyObject> {
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions(rawValue: UInt.allZeros))
                if let string = String(data: data, encoding: String.Encoding.utf8) {
                    print(string)
                }
            } catch {
                print(error)
            }
        }
    }
}
