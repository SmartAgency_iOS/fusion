//
//  Double+Extensions.swift
//  GuessWhat
//
//  Created by Angel Antonov on 4/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation

extension Double {
    func roundedInteger() -> Int {
        if self == 0.0 {
            return 0
        }
        else if self < 0.5 {
            return 1
        }
        
        let leftOver = self.truncatingRemainder(dividingBy: 1.0)
        
        if leftOver >= 0.5 {
            return Int(self) + 1
        }
        
        return Int(self)
    }
    
    var distanceString: String {
        let d = Double(self)
        if 0 < d && d < 1000 {
            return String(format: "%.0f m", d)
        } else {
            return String(format: "%.0f km", d/1000)
        }
    }
}
