//
//  EmptyCell.swift
//  Fusion
//
//  Created by Admin on 8/19/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class EmptyCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        self.backgroundColor = Colors.Dark
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
