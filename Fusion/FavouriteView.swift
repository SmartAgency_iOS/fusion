//
//  FavouriteView.swift
//  Fusion
//
//  Created by Admin on 8/11/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class FavouriteView: UIView {
    
    var btnFav: UIButton!
    var isFavourite: Bool!
    var menuItem: MenuItem!
    
    init(coordinates: CGPoint, item: MenuItem) {
        menuItem = item

        let imgFav = UIImage(named: menuItem.isFavourite ? "small_heart_full" : "small_heart_empty")!
        self.btnFav = UIButton(type: .custom)
        self.btnFav.setImage(imgFav, for: UIControlState())
        self.btnFav.frame = CGRect(x: 0.0, y: 0.0, width: imgFav.size.width + 16, height: imgFav.size.height + 16)
        self.btnFav.imageEdgeInsets = UIEdgeInsets(top: -6.0, left: 0.0, bottom: 0.0, right: 0.0)
        
        super.init(frame: CGRect(x: coordinates.x, y: coordinates.y, width: imgFav.size.width, height: imgFav.size.height))
        self.backgroundColor = UIColor.clear
        
        self.btnFav.addTarget(self, action: #selector(self.clicked), for: .touchUpInside)
        self.addSubview(btnFav)
    }
    
    class var size: CGFloat {
        let imgFav = UIImage(named: "small_heart_full")!
        return imgFav.size.width
    }
    
    func update(isFav: Bool) {
        let imgFav = UIImage(named: !isFav ? "small_heart_empty" : "small_heart_full")!
        self.btnFav.setImage(imgFav, for: UIControlState())
    }
    
    func clicked() {
        let imgFav = UIImage(named: menuItem.isFavourite ? "small_heart_empty" : "small_heart_full")!
        menuItem.toFavourite()
        self.btnFav.setImage(imgFav, for: UIControlState())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
