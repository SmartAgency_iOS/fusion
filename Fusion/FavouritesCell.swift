//
//  FavouritesCell.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/8/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class FavouritesCell: UITableViewCell {
    
    //var imgViewFav: UIImageView!
    var imgViewFav: FavouriteView!
    var lblTitle: UILabel!
    var lblPrice: UILabel!
    var lblExcerpt: UILabel!
    var btnDetails: UIButton!
    var menuItem: MenuItem!
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, item: MenuItem) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = Colors.Black
        self.menuItem = item
        
        // Heart image
        imgViewFav = FavouriteView(coordinates: CGPoint(x: 0.0, y: 15.0), item: item)
        imgViewFav.frame.size = CGSize(width: imgViewFav.frame.size.width + 12.0, height: imgViewFav.frame.size.height + 12.0)
        
        self.addSubview(imgViewFav)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 5.0, y: 10.0, width: ScreenWidth * 0.65, height: 38.0))
        lblTitle.text = item.title != nil ? item.title.frenchalize : ""
        lblTitle.font = Fonts.Medium(18.0)
        lblTitle.textColor = Colors.White
        lblTitle.numberOfLines = 2
        
        self.addSubview(lblTitle)
        
        // Excerpt
        lblExcerpt = UILabel(frame: CGRect(x: lblTitle.frame.origin.x, y: lblTitle.bottomYPoint + 4.0, width: ScreenWidth * 0.65, height: 18.0))
        lblExcerpt.text = item.excerpt != nil ? item.excerpt.frenchalize : ""
        lblExcerpt.font = Fonts.Regular(16.0)
        lblExcerpt.textColor = Colors.White
        lblExcerpt.numberOfLines = 1
        
        self.addSubview(lblExcerpt)
        
        // Price
        lblPrice = UILabel(frame: CGRect(x: (ScreenWidth * 0.8) - 46.0, y: lblTitle.frame.origin.y, width: 32.0, height: 20.0))
        lblPrice.text = item.price != nil ? String(format: "\(item.price!.cleanValue)") : "-"
        lblPrice.font = Fonts.Medium(18.0)
        lblPrice.textColor = Colors.White
        lblPrice.adjustWidth()
        
        self.addSubview(lblPrice)
        
        // Details button
        if menuItem.isWine == false {
            btnDetails = UIButton(type: .custom)
            btnDetails.frame = CGRect(x: imgViewFav.mostRightPoint + 5.0, y: 78.0, width: 100.0, height: 30.0)
            let attributes = [
                NSFontAttributeName : Fonts.Medium(18.0),
                NSForegroundColorAttributeName : Colors.GreenOcean,
                NSUnderlineStyleAttributeName : 1
                ] as [String : Any]
            let strButtonTitle = NSMutableAttributedString(string: "favourites.details".localized(), attributes: attributes)
            btnDetails.setAttributedTitle(strButtonTitle, for: .normal)
            btnDetails.addTarget(self, action: #selector(FavouritesCell.openDetails), for: .touchUpInside)
            btnDetails.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -22.0, bottom: 0.0, right: 0.0)
            
            self.addSubview(btnDetails)
        }

        // Separator
        let viewLine = UIView(frame: CGRect(x: 0.0, y: FavouritesCell.cellHeight - 2.0, width: ScreenWidth * 0.8 , height: 2.0))
        viewLine.backgroundColor = Colors.White
        
        self.addSubview(viewLine)
    }
    
    func openDetails() {
        self.parentViewController?.performSegue(withIdentifier: "Details", sender: self.menuItem)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class var cellHeight: CGFloat {
        return 108.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
