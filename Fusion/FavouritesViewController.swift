//
//  FavouritesViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class FavouritesViewController: NavigationExtensions, UITableViewDelegate, UITableViewDataSource {
    
    var imgViewFav: UIImageView!
    var lblTitle: UILabel!
    var lblSubtitle: UILabel!
    var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        self.fromFavourites = true
        self.view.backgroundColor = Colors.Black
        
        // Heart image
        let imgHeart = UIImage(named: "big_heart")!
        imgViewFav = UIImageView(frame: CGRect(x: ScreenWidth * 0.1, y: Constants.NavBarHeight + 80.0, width: imgHeart.size.width, height: imgHeart.size.height))
        imgViewFav.image = imgHeart
        
        self.view.addSubview(imgViewFav)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 10.0, y: Constants.NavBarHeight + 80.0, width: ScreenWidth * 0.7, height: 44.0))
        lblTitle.text = String(format: "favourites.title".localized())
        lblTitle.textColor = Colors.GreenOcean
        lblTitle.font = Fonts.Bold(42.0)
        lblTitle.textAlignment = .left
        
        self.view.addSubview(lblTitle)
        
        // Subtitle
        lblSubtitle = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: lblTitle.bottomYPoint + 5.0, width: ScreenWidth * 0.8, height: 22.0))
        lblSubtitle.text = String(format: "favourites.subtitle".localized())
        lblSubtitle.textColor = Colors.LightGray
        lblSubtitle.font = Fonts.Bold(20.0)
        lblSubtitle.textAlignment = .left
        
        self.view.addSubview(lblSubtitle)
        
        // Table
        tableView = UITableView(frame: CGRect(x: ScreenWidth * 0.1, y: self.lblSubtitle.bottomYPoint + 40.0, width: ScreenWidth * 0.8, height: ScreenHeight - (lblSubtitle.bottomYPoint + 40.0)))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Colors.Black
        tableView.separatorStyle = .none
        
        self.view.addSubview(tableView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if tableView != nil {
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FavouritesCell(style: .default, reuseIdentifier: "FavouritesCell", item: arrFavourites[(indexPath as NSIndexPath).row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFavourites.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FavouritesCell.cellHeight
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Details" {
            let detailsVC = segue.destination as! DetailsViewController
            if let item = sender as? MenuItem {
                detailsVC.menuItem = item
            }
        }
    }
    
}
