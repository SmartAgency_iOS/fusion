//
//  GalleryViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import Alamofire

class GalleryViewController: NavigationExtensions {
    
    var lblTitle: UILabel!
    var lblSubtitle: UILabel!
    var btnPrev: UIButton!
    var btnNext: UIButton!
    var imgViewSlider: UIImageView!
    var currentPhoto: Int = 1
    var arrSliderPhotos: [Photo] = [Photo]()
    var arrImages: [UIImage] = [UIImage]()
    var arrNames: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.Black
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        self.fromFavourites = true
        
//        for name in arrNames {
//            let image = UIImage(named: name)!
//            arrImages.append(image)
//        }
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: Constants.NavBarHeight + 100.0, width: ScreenWidth * 0.8, height: 42.0))
        lblTitle.text = String(format: "gallery.title".localized())
        lblTitle.textColor = Colors.GreenOcean
        lblTitle.textAlignment = .left
        lblTitle.font = Fonts.Bold(42.0)
        
        self.view.addSubview(lblTitle)
        
        // Subtitle
        lblSubtitle = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: lblTitle.bottomYPoint + 16.0, width: ScreenWidth * 0.8, height: 24.0))
        lblSubtitle.text = String(format: "gallery.subtitle".localized())
        lblSubtitle.textColor = Colors.LighterGray
        lblSubtitle.textAlignment = .left
        lblSubtitle.font = Fonts.Medium(22.0)
        
        self.view.addSubview(lblSubtitle)
        
        self.getPhotos {
            // Image slider
            self.imgViewSlider = UIImageView(frame: CGRect(x: 20.0, y: self.lblSubtitle.bottomYPoint + 40.0, width: ScreenWidth - 40.0, height: ScreenHeight * 0.45))
            if self.arrImages.count > 0 {
                self.imgViewSlider.image = self.arrImages[0]
            }
            self.imgViewSlider.isUserInteractionEnabled = true
            let swipeGestureNext = UISwipeGestureRecognizer(target: self, action: #selector(GalleryViewController.nextPhoto))
            swipeGestureNext.direction = .left
            self.imgViewSlider.addGestureRecognizer(swipeGestureNext)
            let swipeGesturePrev = UISwipeGestureRecognizer(target: self, action: #selector(GalleryViewController.previousPhoto))
            swipeGesturePrev.direction = .right
            self.imgViewSlider.addGestureRecognizer(swipeGesturePrev)
            
            self.view.addSubview(self.imgViewSlider)
            
            // Previous button
            let imgPrev = UIImage(named: "left_arrow")!
            self.btnPrev = UIButton(type: .custom)
            self.btnPrev.frame = CGRect(x: 20.0, y: self.imgViewSlider.bottomYPoint + 40.0, width: 100.0, height: imgPrev.size.height)
            self.btnPrev.addTarget(self, action: #selector(GalleryViewController.previousPhoto), for: .touchUpInside)
            self.btnPrev.setTitle("\(self.arrImages.count)/\(self.arrImages.count)", for: UIControlState())
            self.btnPrev.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 0.0)
            self.btnPrev.setTitleColor(Colors.White, for: UIControlState())
            self.btnPrev.setImage(imgPrev, for: UIControlState())
            
            self.view.addSubview(self.btnPrev)
            
            // Next button
            let imgNext = UIImage(named: "right_arrow")!
            self.btnNext = UIButton(type: .custom)
            self.btnNext.frame = CGRect(x: ScreenWidth - 100.0, y: self.imgViewSlider.bottomYPoint + 40.0, width: 100.0, height: imgNext.size.height)
            self.btnNext.addTarget(self, action: #selector(GalleryViewController.nextPhoto), for: .touchUpInside)
            if self.currentPhoto + 1 > self.arrImages.count {
                self.btnNext.setTitle("\(self.arrImages.count)/\(self.arrImages.count)", for: UIControlState())
            } else {
                self.btnNext.setTitle("\(self.currentPhoto + 1)/\(self.arrImages.count)", for: UIControlState())
            }
            self.btnNext.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -90.0, bottom: 0.0, right: 0.0)
            self.btnNext.setTitleColor(Colors.White, for: UIControlState())
            self.btnNext.setImage(imgNext, for: UIControlState())
            self.btnNext.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 40.0, bottom: 0.0, right: 0.0)
            
            self.view.addSubview(self.btnNext)

        }
    }
    
    func getPhotos(_ success: @escaping (() -> ())) {
        
        if let items = UserDefaults.standard.object(forKey: "gallery") as? [[String: AnyObject]] {
            let myGroup = DispatchGroup()
            for item in items {
                myGroup.enter()
                let photo = Photo.parse(item)
                if let img = photo.getImage({ (image) in
                    if image != nil {
                        self.arrImages.append(image!)
                    }
                }) {
                    self.arrImages.append(img)
                }
                myGroup.leave()
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                if self.arrImages.count > 0 {
                    success()
                }
            })
        }
        
//        let myGroup = DispatchGroup()
//        for item in arrCachedGalleryImages {
//            myGroup.enter()
//            self.arrImages.append(item)
//            myGroup.leave()
//        }
//        myGroup.notify(queue: DispatchQueue.main, execute: {
//            if self.arrImages.count > 0 {
//                success()
//            }
//        })
    }
    
    func nextPhoto() {
        currentPhoto += 1
        if currentPhoto == arrImages.count + 1 {
            currentPhoto = 1
            self.imgViewSlider.image = arrImages.first
            btnNext.setTitle("\(currentPhoto + 1)/\(self.arrImages.count)", for: UIControlState())
            btnPrev.setTitle("\(self.arrImages.count)/\(self.arrImages.count)", for: UIControlState())
        } else if currentPhoto == self.arrImages.count {
            self.imgViewSlider.image = arrImages.last
            btnNext.setTitle("1/\(self.arrImages.count)", for: UIControlState())
            btnPrev.setTitle("\(currentPhoto - 1)/\(self.arrImages.count)", for: UIControlState())
        } else {
            self.imgViewSlider.image = arrImages[currentPhoto - 1]
            btnNext.setTitle("\(currentPhoto + 1)/\(self.arrImages.count)", for: UIControlState())
            btnPrev.setTitle("\(currentPhoto - 1)/\(self.arrImages.count)", for: UIControlState())
        }
        self.imgViewSlider.slideInFromRight()
    }
    
    func previousPhoto() {
        currentPhoto -= 1
        if currentPhoto == 1 {
            self.imgViewSlider.image = arrImages[0]
            btnNext.setTitle("\(currentPhoto + 1)/\(self.arrImages.count)", for: UIControlState())
            btnPrev.setTitle("\(self.arrImages.count)/\(self.arrImages.count)", for: UIControlState())
        } else if currentPhoto == 0 {
            self.imgViewSlider.image = arrImages.last
            currentPhoto = self.arrImages.count
            btnNext.setTitle("1/\(self.arrImages.count)", for: UIControlState())
            btnPrev.setTitle("\(currentPhoto)/\(self.arrImages.count)", for: UIControlState())
        } else {
            self.imgViewSlider.image = arrImages[currentPhoto - 1]
            btnNext.setTitle("\(currentPhoto + 1)/\(self.arrImages.count)", for: UIControlState())
            btnPrev.setTitle("\(currentPhoto - 1)/\(self.arrImages.count)", for: UIControlState())
        }
        self.imgViewSlider.slideInFromLeft()
    }
    
}

extension UIImageView {
    // Name this function in a way that makes sense to you...
    // slideFromLeft, slideRight, slideLeftToRight, etc. are great alternative names
    func slideInFromLeft(_ duration: TimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
//        if let delegate: AnyObject = completionDelegate {
//            slideInFromLeftTransition.delegate = delegate
//        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    
    func slideInFromRight(_ duration: TimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromLeftTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
//        if let delegate: AnyObject = completionDelegate {
//            slideInFromLeftTransition.delegate = delegate
//        }
        
        // Customize the animation's properties
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromRight
        slideInFromLeftTransition.duration = duration
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromLeftTransition, forKey: "slideInFromRightTransition")
    }
}

/*
 func getPhotos(_ success: @escaping (() -> ())) {
 //        Alamofire.request(URL.RestServer + OperationType.GALLERY_PHOTOS.rawValue, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
 //            if let result = validateResponse(response) {
 //                if let items = result["items"] as? [[String: AnyObject]] {
 //                    let myGroup = DispatchGroup()
 //                    for item in items {
 //                        myGroup.enter()
 //                        let photo = Photo.parse(item)
 //                        arrCachedPhotos.append(photo)
 //                        photo.getImage({ (downloadedImage) in
 //                            if downloadedImage != nil {
 //                                self.arrImages.append(downloadedImage!)
 //                                myGroup.leave()
 //                            }
 //                        })
 //                    }
 //                    myGroup.notify(queue: DispatchQueue.main, execute: {
 //                        success()
 //                    })
 //                }
 //            } else {
 //                let myGroup = DispatchGroup()
 //                for item in arrCachedPhotos {
 //                    myGroup.enter()
 //                    item.getImage({ (downloadedImage) in
 //                        if downloadedImage != nil {
 //                            self.arrImages.append(downloadedImage!)
 //                            myGroup.leave()
 //                        }
 //                    })
 //                }
 //                myGroup.notify(queue: DispatchQueue.main, execute: {
 //                    if self.arrImages.count > 0 {
 //                        success()
 //                    }
 //                })
 //            }
 //        }
 }

 */
