//
//  ViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/4/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import Localize_Swift
import Alamofire

class HomeViewController: UIViewController {

    var imgViewBackground: UIImageView!
    var imgViewLogo: UIImageView!
    var lblTitle: UILabel!
    var lblMenu: UILabel!
    var lblLanguage: UILabel!
    var btnEnglish: UIButton!
    var btnFrench: UIButton!
    var btnRefresh: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // show loading screen
        
        // Background image
        let imgBackground = UIImage(named: "Background".imageName)!
        self.imgViewBackground = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: ScreenWidth, height: ScreenHeight))
        self.imgViewBackground.image = imgBackground
        
        self.view.addSubview(imgViewBackground)
        
        // Logo
        let imgLogo = UIImage(named: "LOGO".imageName)!
        self.imgViewLogo = UIImageView(frame: CGRect(x: ScreenWidth /^ imgLogo.size.width, y: 100.0, width: imgLogo.size.width, height: imgLogo.size.height))
        self.imgViewLogo.image = imgLogo
        
        self.view.addSubview(imgViewLogo)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: 0.0, y: imgViewLogo.bottomYPoint + 28.0, width: ScreenWidth, height: 126.0))
        lblTitle.text = String(format: "home.title".localized())
        lblTitle.textColor = Colors.GreenOcean
        lblTitle.textAlignment = .center
        lblTitle.setFont(Fonts.Medium(36.0), string: "home.restaurant".localized())
        lblTitle.setFont(Fonts.Bold(64.0), string: "home.fusion".localized())
        lblTitle.setFont(Fonts.Regular(24.0), string: "home.by.sushizen".localized())
        lblTitle.adjustHeight()
        
        self.view.addSubview(lblTitle)
        
        // English button
        let imgEnglish = UIImage(named: "EN")!
        self.btnEnglish = UIButton(type: .custom)
        self.btnEnglish.frame = CGRect(x: (ScreenWidth / 2) - 70.0, y: ScreenHeight - 120.0, width: imgEnglish.size.width, height: imgEnglish.size.height)
        self.btnEnglish.setBackgroundImage(imgEnglish, for: UIControlState())
        self.btnEnglish.addTarget(self, action: #selector(self.setEnglishLanguage), for: .touchUpInside)
        
        self.view.addSubview(btnEnglish)
        
        // French button
        let imgFrench = UIImage(named: "FR")!
        self.btnFrench = UIButton(type: .custom)
        self.btnFrench.frame = CGRect(x: (ScreenWidth / 2) + ((ScreenWidth / 2) - btnEnglish.mostRightPoint), y: ScreenHeight - 120.0, width: imgFrench.size.width, height: imgFrench.size.height)
        self.btnFrench.setBackgroundImage(imgFrench, for: UIControlState())
        self.btnFrench.addTarget(self, action: #selector(self.setFrenchLanguage), for: .touchUpInside)
        
        self.view.addSubview(btnFrench)
        
        // Language selection
        lblLanguage = UILabel(frame: CGRect(x: 0.0, y: btnFrench.frame.origin.y - 100.0, width: ScreenWidth, height: 120.0))
        lblLanguage.text = String(format: "home.language".localized())
        lblLanguage.textColor = Colors.GreenOcean
        lblLanguage.textAlignment = .center
        lblLanguage.font = Fonts.Regular(16.0)
        
        self.view.addSubview(lblLanguage)
        
        // Menu title
        lblMenu = UILabel(frame: CGRect(x: 0.0, y: lblLanguage.frame.origin.y - 40.0, width: ScreenWidth, height: 120.0))
        lblMenu.text = String(format: "home.menu".localized())
        lblMenu.textColor = Colors.GreenOcean
        lblMenu.textAlignment = .center
        lblMenu.font = Fonts.Bold(46.0)
        lblMenu.setTextUnderline(Colors.GreenOcean, string: "home.menu".localized())
        
        self.view.addSubview(lblMenu)
        
        btnRefresh = UIButton(type: .custom)
        btnRefresh.frame = CGRect(x: ScreenWidth - 100.0, y: ScreenHeight - 100.0, width: 100.0, height: 100.0)
        btnRefresh.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        
        self.view.addSubview(btnRefresh)
        
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        let stringDate = dateFormatter.string(from: todayDate)
        
        if let strDate = UserDefaults.standard.object(forKey: "date") as? String {
            if stringDate == strDate {
                //self.loadDataFromDefaults()
                dataLoaded = true
            }
        }
        
        if dataLoaded == false {
            self.getData { (success) in
                if success == true {
//                    let dataSave: Data = NSKeyedArchiver.archivedData(withRootObject: dictMealsImages)
//                    UserDefaults.standard.set(dataSave, forKey: "dictMealsImages")
//                    UserDefaults.standard.synchronize()
                } else {
                    // show error
                }
            }
        }
        
    }
    
    func setEnglishLanguage() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(["en"], forKey: "AppleLanguages")
        userDefaults.synchronize()
        language = "en"
        Localize.setCurrentLanguage("en")
        appDelegate.showCategoriesStoryboard()
    }
    
    func setFrenchLanguage() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(["fr"], forKey: "AppleLanguages")
        userDefaults.synchronize()
        Localize.setCurrentLanguage("fr")
        language = "fr"
        appDelegate.showCategoriesStoryboard()
    }
    
    func loadDataFromDefaults() {
        if let obj = UserDefaults.standard.object(forKey: "dictRestaurantImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [String: UIImage] {
                dictRestaurantImages = savedObj
            }
        }
        if let obj = UserDefaults.standard.object(forKey: "arrCachedGalleryImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [UIImage] {
                arrCachedGalleryImages = savedObj
            }
        }
        if let obj = UserDefaults.standard.object(forKey: "dictCocktailsImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [String: UIImage] {
                dictCocktailsImages = savedObj
            }
        }
        if let obj = UserDefaults.standard.object(forKey: "dictEntriesImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [String: UIImage] {
                dictEntriesImages = savedObj
            }
        }
        if let obj = UserDefaults.standard.object(forKey: "dictDessertsImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [String: UIImage] {
                dictDessertsImages = savedObj
            }
        }
        if let obj = UserDefaults.standard.object(forKey: "dictWinesImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [String: UIImage] {
                dictWinesImages = savedObj
            }
        }
        if let obj = UserDefaults.standard.object(forKey: "dictDailyMenuImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [String: UIImage] {
                dictDailyMenuImages = savedObj
            }
        }
        if let obj = UserDefaults.standard.object(forKey: "dictMealsImages") as? Data {
            if let savedObj = NSKeyedUnarchiver.unarchiveObject(with: obj) as? [String: UIImage] {
                dictMealsImages = savedObj
            }
        }
    }
    
    func getData(success: @escaping (_ success: Bool) -> Void) {
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        let stringDate = dateFormatter.string(from: todayDate)
        
        UserDefaults.standard.set(stringDate, forKey: "date")
        
        self.showLoadingScreen()
        Alamofire.request(URL.RestServer + OperationType.ALL_DATA.rawValue, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = validateAllData(response) {
                if result is [String: AnyObject] {
                    dataLoaded = true
                    
                    if let dailyMenu = result[OperationType.DAILY_MENU.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchDailyMenuData(dictJSON: dailyMenu, { (success) in
                            // leave group
                            print("DAILY MENU")
                        })
                    }
                    if let cocktails = result[OperationType.COCKTAILS_AND_APPETIZERS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchCocktailsData(dictJSON: cocktails, { (success) in
                            //leave group
                            print("COCKTAILS")
                        })
                    }
                    if let desserts = result[OperationType.DESSERTS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchDessertsData(dictJSON: desserts, { (success) in
                            //leave group
                            print("DESSERTS")
                        })
                    }
                    if let starters = result[OperationType.STARTERS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchEntriesData(dictJSON: starters, { (success) in
                            //leave group
                            print("STARTERS")
                        })
                    }
                    if let wines = result[OperationType.WINES.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchWinesData(dictJSON: wines, { (success) in
                            print("WINES")
                        })
                    }
                    if let mainCourses = result[OperationType.MAIN_COURSES.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchMealsData(dictJSON: mainCourses, { (success) in
                            print("MEALS")

                        })
                        
                    }
                    if let restaurantInfo = result[OperationType.RESTAURANT_INFO.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchRestaurantData(dictJSON: restaurantInfo, { (success) in
                            // leave group
                            print("RESTAURANT")
                        })
                    }
                    if let galleryPhotos = result[OperationType.GALLERY_PHOTOS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchGalleryPhotos(dictJSON: galleryPhotos, { (success) in
                            // leave group
                            print("GALLERY")
                            self.hideLoadingScreen()
                        })
                    }
                    success(true)
                } else {
                    success(false)
                    self.hideLoadingScreen()
                }
            } else {
                success(false)
                self.hideLoadingScreen()
            }
        }
    }
    
    func refresh() {
        self.showLoadingScreen()
        Alamofire.request(URL.RestServer + OperationType.ALL_DATA.rawValue, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = validateAllData(response) {
                if result is [String: AnyObject] {
                    dataLoaded = true
                    
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                    let photosPath = String("\(documentsPath)/Photos")
                    
                    if (PhotoUtil.directoryExistsAtPath(photosPath!)) {
                        do  {
                           try FileManager.default.removeItem(atPath: photosPath!)
                            } catch {
                        }
                    }
                    
                    if let dailyMenu = result[OperationType.DAILY_MENU.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchDailyMenuData(dictJSON: dailyMenu, { (success) in
                            // leave group
                            print("DAILY MENU")
                        })
                    }
                    if let cocktails = result[OperationType.COCKTAILS_AND_APPETIZERS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchCocktailsData(dictJSON: cocktails, { (success) in
                            //leave group
                            print("COCKTAILS")
                        })
                    }
                    if let desserts = result[OperationType.DESSERTS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchDessertsData(dictJSON: desserts, { (success) in
                            //leave group
                            print("DESSERTS")
                        })
                    }
                    if let starters = result[OperationType.STARTERS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchEntriesData(dictJSON: starters, { (success) in
                            //leave group
                            print("STARTERS")
                        })
                    }
                    if let wines = result[OperationType.WINES.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchWinesData(dictJSON: wines, { (success) in
                            print("WINES")
                        })
                    }
                    if let mainCourses = result[OperationType.MAIN_COURSES.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchMealsData(dictJSON: mainCourses, { (success) in
                            print("MEALS")
                        })
                        
                    }
                    if let restaurantInfo = result[OperationType.RESTAURANT_INFO.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchRestaurantData(dictJSON: restaurantInfo, { (success) in
                            // leave group
                            print("RESTAURANT")
                        })
                    }
                    if let galleryPhotos = result[OperationType.GALLERY_PHOTOS.rawValue] as? [String: AnyObject] {
                        ProjectManager.fetchGalleryPhotos(dictJSON: galleryPhotos, { (success) in
                            // leave group
                            print("GALLERY")
                            self.hideLoadingScreen()
                        })
                    }
                } else {
                    self.hideLoadingScreen()
                }
            } else {
                self.hideLoadingScreen()
            }
        }

    }
    
//    func downloadAllData(allDataDownloadedCompletionHandler:()->Void) {
//        let dispatchGroup: dispatch_group_t = dispatch_group_create()
//        let types = ["one", "two", "three"]  // there are actually about 10 requests called, but to make it simple I set it to 3
//        for type in types {
//            // enter group and run request
//            dispatch_group_enter(dispatchGroup)
//            self.downloadDataForType(type, group: dispatchGroup)
//        }
//        
//        dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), {
//            allDataDownloadedCompletionHandler()
//        });
//    }
//    
//    func downloadDataForType(type:String, group: dispatch_group_t) {
//        Alamofire.request(Router.TypeData(type: type)).response({ (request, response, xmlResponse, error) -> Void in
//            // request finished
//            println("Data for type \(type) downloaded")
//            
//            // let's parse response in different queue, because we don't want to hold main UI queue
//            var db_queue = dispatch_queue_create("db_queue", nil)
//            dispatch_async(db_queue, {
//                if response?.statusCode == 200 {
//                    saveToDatabase(xmlResponse)
//                }
//                
//                // leave group
//                dispatch_group_leave(group)
//            })
//        })
//    }

}
