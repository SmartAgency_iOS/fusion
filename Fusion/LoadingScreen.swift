//
//  KNRLoadingScreen.swift
//  KNR
//
//  Created by Angel Antonov on 2/24/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import UIKit

extension UIViewController {
    func showLoadingScreen() {
        let viewHolder = self.view
        
        if let _ = viewHolder?.viewWithTag(100) as? LoadingScreen {
            return
        }
        
        let loadingScreen =
            LoadingScreen()
        loadingScreen.tag = 100
        
        viewHolder?.addSubview(loadingScreen)
        
        loadingScreen.shouldAnimate = true
        
        loadingScreen.startAnimating()
    }
    
    func hideLoadingScreen() {
        let viewHolder = self.view
        
        let loadingScreen = viewHolder?.viewWithTag(100) as? LoadingScreen
        
        if loadingScreen != nil {
            loadingScreen!.stopAnimating()
            loadingScreen!.removeFromSuperview()
        }
    }
}

class LoadingScreen: UIView {
    var activity: UIActivityIndicatorView!
    
    let screenFrame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
    var shouldAnimate = false
    
    override init (frame : CGRect) {
        super.init(frame : screenFrame)
        addBehavior()
    }
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func addBehavior() {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        
        activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activity.center = self.center
        
        self.addSubview(activity)
    }
    
    func startAnimating() {
        if !self.shouldAnimate {
            return
        }
        
        activity.startAnimating()
    }
    
    func stopAnimating() {
        shouldAnimate = false
        
        activity.stopAnimating()
    }
}
