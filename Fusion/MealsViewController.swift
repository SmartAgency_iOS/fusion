//
//  MealsViewController.swift
//  Fusion
//
//  Created by Admin on 8/12/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import Alamofire

class MealsViewController: NavigationExtensions, UITableViewDataSource, UITableViewDelegate, CategoryDelegate {
    
    var tableView: UITableView!
    var arrMeat: [MenuItem] = [MenuItem]()
    var arrFish: [MenuItem] = [MenuItem]()
    var header1: CategoryHeaderView!
    var header2: CategoryHeaderView!
    var header1Info: (String, String, String, UIImage, UIImage) = ("", "", "", UIImage(), UIImage())
    var header2Info: (String, String, String, UIImage, UIImage) = ("", "", "", UIImage(), UIImage())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.Dark
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        
        
        // First table
        tableView = UITableView(frame: CGRect(x: 0.0, y: Constants.NavBarHeight, width: ScreenWidth, height: ScreenHeight - Constants.NavBarHeight), style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Colors.Black
        tableView.separatorStyle = .none
        tableView.bounces = false
        
        self.view.addSubview(tableView)
        
        DispatchQueue.main.async {
            //self.getItems()
            self.loadData {
                print("Success")
            }
        }
        
        // Test - load cache before API call
        //let settings = self.loadCachedData().0
        //let items = self.loadCachedData().1
        //self.setCachedItems(settings, items: items)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "REFRESH_ITEM"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadMission), name: NSNotification.Name(rawValue: "REFRESH_ITEM"), object: nil)
    }
    
    func reloadMission(notification: NSNotification) {
        if let item = notification.userInfo?["menuItem"] as? MenuItem {
            for meat in arrMeat {
                if meat == item {
                    meat.isFavourite = item.isFavourite
                    self.tableView.reloadData()
                    return
                }
            }
            for fish in arrFish {
                if fish == item {
                    fish.isFavourite = item.isFavourite
                    self.tableView.reloadData()
                    return
                }
            }
        }
    }
    
    // MARK - Functionalities
    
    func getItems() {
        arrFish.removeAll()
        arrMeat.removeAll()
        
        var set = [String: AnyObject]()
        var it = [[String: AnyObject]]()
        
        let language: String = "language".localized()
        let params = ["lang" : language] as [String: String]
        
        Alamofire.request(URL.RestServer + OperationType.MAIN_COURSES.rawValue, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = validateResponse(response) {
                print(result)
                // get category
                if let settings = result["settings"] as? [String: AnyObject] {
                    set = settings
                    if let fishSettings = settings["fish"] as? [String: AnyObject] {
                        let fishCategory = Category.parse(fishSettings)
                        self.header2Info.0 = fishCategory.title != nil ? fishCategory.title : ""
                        self.header2Info.1 = fishCategory.subtitle != nil ? fishCategory.subtitle.frenchalize : ""
                        self.header2Info.2 = fishCategory.descr != nil ? fishCategory.descr.frenchalize : ""
                        fishCategory.photo1?.getImage({ (downloadedImage1) in
                            fishCategory.photo2?.getImage({ (downloadedImage2) in
                                self.header2Info.3 = downloadedImage1!
                                self.header2Info.4 = downloadedImage2!
                                self.cacheFishImages(downloadedImage1!, image2: downloadedImage2!)
                                if self.header2 != nil {
                                    self.header2.imgViewPortrait.image = self.header2Info.3
                                    self.header2.imgViewLandscape.image = self.header2Info.4
                                }
                            })
                        })
                    }
                    if let meatSettings = settings["meat"] as? [String: AnyObject] {
                        let meatCategory = Category.parse(meatSettings)
                        self.header1Info.0 = meatCategory.title != nil ? meatCategory.title : ""
                        self.header1Info.1 = meatCategory.subtitle != nil ? meatCategory.subtitle.frenchalize : ""
                        self.header1Info.2 = meatCategory.descr != nil ? meatCategory.descr.frenchalize : ""
                        meatCategory.photo1?.getImage({ (downloadedImage1) in
                            meatCategory.photo2?.getImage({ (downloadedImage2) in
                                self.header1Info.3 = downloadedImage1!
                                self.header1Info.4 = downloadedImage2!
                                self.header1.updateInfo(self.header1Info.0, subtitle: self.header1Info.1, description: self.header1Info.2, image1: self.header1Info.3, image2: self.header1Info.4)
                                // cache images
                                self.cacheImages(downloadedImage1!, image2: downloadedImage2!)
                            })
                        })
                    }
                }
                // get items
                if let items = result["items"] as? [[String: AnyObject]] {
                    it = items
                    for item in items {
                        let item = MenuItem.parse(item)
                        for fav in arrFavourites {
                            if item == fav {
                                item.isFavourite = true
                            }
                        }
                        if item.category == 12 {
                            self.arrFish.append(item)
                        } else { // category == 11
                            self.arrMeat.append(item)
                        }
                        // Check if item is in favourites list
                        
                        
                    }
                    
                    self.tableView.reloadData()
                }
                self.cacheData(set, items: it)
            } else {
                let settings = self.loadCachedData().0
                let items = self.loadCachedData().1
                self.setCachedItems(settings, items: items)
            }
   
        }
    }
    
    func cacheImages(_ image1: UIImage, image2: UIImage) {
        let userDefaults = UserDefaults.standard
        let data1 = UIImagePNGRepresentation(image1)
        let data2 = UIImagePNGRepresentation(image2)
        userDefaults.set(data1, forKey: "meals-image-portrait")
        userDefaults.set(data2, forKey: "meals-image-landscape")
    }
    
    func cacheFishImages(_ image1: UIImage, image2: UIImage) {
        let userDefaults = UserDefaults.standard
        let data1 = UIImagePNGRepresentation(image1)
        let data2 = UIImagePNGRepresentation(image2)
        userDefaults.set(data1, forKey: "fish-image-portrait")
        userDefaults.set(data2, forKey: "fish-image-landscape")
    }
    
    func loadFishImages() -> (UIImage?, UIImage?) {
        var image1: UIImage?
        var image2: UIImage?
        let userDefaults = UserDefaults.standard
        if let imagee1 = userDefaults.object(forKey: "fish-image-portrait") as? Data {
            image1 = UIImage(data: imagee1)
        }
        if let imagee2 = userDefaults.object(forKey: "fish-image-landscape") as? Data {
            image2 = UIImage(data: imagee2)
        }
        return (image1, image2)
    }
    
    func loadCachedImages() -> (UIImage?, UIImage?) {
        var image1: UIImage?
        var image2: UIImage?
        let userDefaults = UserDefaults.standard
        if let imagee1 = userDefaults.object(forKey: "meals-image-portrait") as? Data {
            image1 = UIImage(data: imagee1)
        }
        if let imagee2 = userDefaults.object(forKey: "meals-image-landscape") as? Data {
            image2 = UIImage(data: imagee2)
        }
        return (image1, image2)
    }
    
    func setCachedItems(_ settings: [String: AnyObject], items: [[String: AnyObject]]) {
        if let fishSettings = settings["fish"] as? [String: AnyObject] {
            let fishCategory = Category.parse(fishSettings)
            self.header2Info.0 = fishCategory.title != nil ? fishCategory.title : ""
            self.header2Info.1 = fishCategory.subtitle != nil ? fishCategory.subtitle.frenchalize : ""
            self.header2Info.2 = fishCategory.descr != nil ? fishCategory.descr.frenchalize : ""
            self.header2Info.3 = self.loadFishImages().0 != nil ? self.loadFishImages().0! : UIImage()
            self.header2Info.4 = self.loadFishImages().1 != nil ? self.loadFishImages().1! : UIImage()
//            fishCategory.photo1?.getImage({ (downloadedImage1) in
//                fishCategory.photo2?.getImage({ (downloadedImage2) in
//                    self.header2Info.3 = downloadedImage1!
//                    self.header2Info.4 = downloadedImage2!
//                })
//            })
        }
        if let meatSettings = settings["meat"] as? [String: AnyObject] {
            let meatCategory = Category.parse(meatSettings)
            self.header1Info.0 = meatCategory.title != nil ? meatCategory.title : ""
            self.header1Info.1 = meatCategory.subtitle != nil ? meatCategory.subtitle.frenchalize : ""
            self.header1Info.2 = meatCategory.descr != nil ? meatCategory.descr.frenchalize : ""
            self.header1Info.3 = self.loadCachedImages().0 != nil ? self.loadCachedImages().0! : UIImage()
            self.header1Info.4 = self.loadCachedImages().1 != nil ? self.loadCachedImages().1! : UIImage()
            if self.header1 != nil {
                self.header1.updateInfo(self.header1Info.0, subtitle: self.header1Info.1, description: self.header1Info.2, image1: self.header1Info.3, image2: self.header1Info.4)    
            }
            
            
//            meatCategory.photo1?.getImage({ (downloadedImage1) in
//                meatCategory.photo2?.getImage({ (downloadedImage2) in
//                    self.header1Info.3 = downloadedImage1!
//                    self.header1Info.4 = downloadedImage2!
//                    
//                })
//            })
        }
        // get all items
        print(items)
        for item in items {
            let item = MenuItem.parse(item)
            for fav in arrFavourites {
                if item == fav {
                    item.isFavourite = true
                }
            }
            if item.category == 12 {
                self.arrFish.append(item)
            } else {
                self.arrMeat.append(item)
            }
        }
        
        self.tableView.reloadData()
    }
    
    
    func loadCachedData() -> ([String: AnyObject], [[String: AnyObject]]) {
        let userDefaults = UserDefaults.standard
        var settings = [String: AnyObject]()
        var items = [[String: AnyObject]]()
        
        if let set = userDefaults.object(forKey: "meals-settings") as? [String: AnyObject] {
            settings = set
        }
        if let it = userDefaults.object(forKey: "meals-items") as? [[String: AnyObject]] {
            items = it
        }
        return (settings, items)
    }
    
    func cacheData(_ settings: [String: AnyObject], items: [[String: AnyObject]]) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(settings, forKey: "meals-settings")
        userDefaults.set(items, forKey: "meals-items")
        userDefaults.synchronize()
        
    }
    
    func slide() {
        let headerView = tableView.rectForHeader(inSection: 0)
        tableView.setContentOffset(CGPoint(x: 0.0, y: headerView.origin.y + headerView.size.height), animated: true)
    }
    
    func slideBottom() {
        let headerView = tableView.rectForHeader(inSection: 1)
        tableView.setContentOffset(CGPoint(x: 0.0, y: headerView.origin.y + headerView.size.height), animated: true)
    }
    // MARK - Table View
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).section == 0 {
            if (indexPath as NSIndexPath).row >= arrMeat.count {
                let cell = EmptyCell(style: .default, reuseIdentifier: "EmptyCell")
                return cell
            } else {
                let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: .Meals, item: arrMeat[(indexPath as NSIndexPath).row])
                return cell
            }
        } else {
            if (indexPath as NSIndexPath).row >= arrFish.count {
                let cell = EmptyCell(style: .default, reuseIdentifier: "EmptyCell")
                return cell
            } else {
                let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: .Meals, item: arrFish[(indexPath as NSIndexPath).row])
                return cell
            }

        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            header1 = CategoryHeaderView(selectedCategory: .Meals)
            header1.delegate = self
            // load cached images
            //header1.imgViewPortrait.image = self.loadCachedImages().0
            //header1.imgViewLandscape.image = self.loadCachedImages().1
            header1.imgViewPortrait.image = dictMealsImages["photo3"] != nil ? dictMealsImages["photo3"] : UIImage()
            header1.imgViewLandscape.image = dictMealsImages["photo4"] != nil ? dictMealsImages["photo4"] : UIImage()
            header1.lblDescription.text = header1Info.2
            //
            header1.updateInfo(header1Info.0, subtitle: header1Info.1, description: header1Info.2, image1: header1Info.3, image2: header1Info.4)
            return header1
        } else {
            header2 = CategoryHeaderView(selectedCategory: .Meals)
            header2.tag = 101
            header2.delegate = self
            self.header2.updateInfo(self.header2Info.0, subtitle: self.header2Info.1, description: self.header2Info.2, image1: self.header2Info.3, image2: self.header2Info.4)
            self.header2.update()
            return header2
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let sectionFooter = SectionFooter(frame: CGRect(x: 0.0, y: 0.0, width: ScreenWidth, height: 80.0), parent: tableView)
            return sectionFooter
        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            // if items are more than 8, then scroll is OK
            if arrMeat.count > 8 {
                return arrMeat.count
            } else {
                return 8
            }
        } else {
            // if items are more than 8, then scroll is OK
            if arrFish.count > 8 {
                return arrFish.count
            } else {
                return 9
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 108.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return ScreenHeight - Constants.NavBarHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 80.0
        }
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailsFromCategory" {
            let detailsVC = segue.destination as! DetailsViewController
            if let item = sender as? MenuItem {
                detailsVC.menuItem = item
                detailsVC.fromCategory = true
            }
        }
    }
    
    func loadData(success: (() -> Void)) {
        var itemsArray = [[String: AnyObject]] ()
        var settingsDict = [String: AnyObject]()
        var imagesDict = [String: UIImage]()
        var image1 = UIImage()
        var image2 = UIImage()
        var image3 = UIImage()
        var image4 = UIImage()
        
        if let items = UserDefaults.standard.object(forKey: "main-courses-items") as? [[String: AnyObject]] {
            itemsArray = items
            imagesDict = dictMealsImages
        }
        if let settings = UserDefaults.standard.object(forKey: "main-courses-settings") as? [String: AnyObject] {
            settingsDict = settings
            image1 = dictMealsImages["photo1"] != nil ? dictMealsImages["photo1"]! : UIImage()
            image2 = dictMealsImages["photo2"] != nil ? dictMealsImages["photo2"]! : UIImage()
            image3 = dictMealsImages["photo3"] != nil ? dictMealsImages["photo3"]! : UIImage()
            image4 = dictMealsImages["photo4"] != nil ? dictMealsImages["photo4"]! : UIImage()
        }
        
        if let fishSettings = settingsDict["fish"] as? [String: AnyObject] {
            let fishCategory = Category.parse(fishSettings)
            if language == "en" {
                self.header2Info.0 = fishCategory.titleEN != nil ? fishCategory.titleEN.frenchalize : ""
                self.header2Info.1 = fishCategory.subtitleEN != nil ? fishCategory.subtitleEN.frenchalize : ""
                self.header2Info.2 = fishCategory.descrEN != nil ? fishCategory.descrEN.frenchalize : ""
            } else {
                self.header2Info.0 = fishCategory.title != nil ? fishCategory.title.frenchalize : ""
                self.header2Info.1 = fishCategory.subtitle != nil ? fishCategory.subtitle.frenchalize : ""
                self.header2Info.2 = fishCategory.descr != nil ? fishCategory.descr.frenchalize : ""
            }

            if let img1 = fishCategory.photo1.getImage({ (image) in
                if image != nil {
                    self.header2Info.3 = image!
                }
            }) {
                self.header2Info.3 = img1
            }
            
            if let img2 = fishCategory.photo2.getImage({ (image) in
                if image != nil {
                    self.header2Info.4 = image!
                }
            }) {
                self.header2Info.4 = img2
            }
            
            if self.header2 != nil {
                self.header2.imgViewPortrait.image = self.header2Info.3
                self.header2.imgViewLandscape.image = self.header2Info.4
            }
        }
        if let meatSettings = settingsDict["meat"] as? [String: AnyObject] {
            let meatCategory = Category.parse(meatSettings)
            if language == "en" {
                self.header1Info.0 = meatCategory.titleEN != nil ? meatCategory.titleEN.frenchalize : ""
                self.header1Info.1 = meatCategory.subtitleEN != nil ? meatCategory.subtitleEN.frenchalize : ""
                self.header1Info.2 = meatCategory.descrEN != nil ? meatCategory.descrEN.frenchalize : ""
            } else {
                self.header1Info.0 = meatCategory.title != nil ? meatCategory.title.frenchalize : ""
                self.header1Info.1 = meatCategory.subtitle != nil ? meatCategory.subtitle.frenchalize : ""
                self.header1Info.2 = meatCategory.descr != nil ? meatCategory.descr.frenchalize : ""
            }

            if let img1 = meatCategory.photo1?.getImage({ (image) in
                if image != nil {
                    self.header1Info.3 = image!
                }
            }) {
                self.header1Info.3 = img1
            }
            
            if let img2 = meatCategory.photo2?.getImage({ (image) in
                if image != nil {
                    self.header1Info.4 = image!
                }
            }) {
                self.header1Info.4 = img2
            }
            
            self.header1.updateInfo(self.header1Info.0, subtitle: self.header1Info.1, description: self.header1Info.2, image1: self.header1Info.3, image2: self.header1Info.4)
        }
        // get items
        for item in itemsArray {
            let itemec = MenuItem.parse(item)
            //itemec.realImage = dictMealsImages["\(itemec.id!)"] != nil ? dictMealsImages["\(itemec.id!)"] : UIImage()
            for fav in arrFavourites {
                if itemec == fav {
                    itemec.isFavourite = true
                }
            }
            if itemec.category == 12 {
                self.arrFish.append(itemec)
            } else { // category == 11
                self.arrMeat.append(itemec)
            }
            // Check if item is in favourites list
        }
        self.tableView.reloadData()
    }

    
}

class SectionFooter: UIView {
    
    var lblSlide: UILabel!
    var btnSlide: UIButton!
    var parentView: UITableView!
    
    init(frame: CGRect, parent: UITableView) {
        super.init(frame: frame)
        
        self.parentView = parent
        self.backgroundColor = Colors.Dark
        
        // Slide label
        lblSlide = UILabel(frame: CGRect(x: 0.0, y: 10.0, width: ScreenWidth, height: 16.0))
        lblSlide.text = "slider.main.courses2".localized()
        lblSlide.font = Fonts.Medium(14.0)
        lblSlide.textColor = Colors.White
        lblSlide.textAlignment = .center
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.slide))
        lblSlide.addGestureRecognizer(tapGesture)
        lblSlide.isUserInteractionEnabled = true
        
        self.addSubview(lblSlide)
        
        // Slide button
        btnSlide = UIButton(type: .custom)
        let imgSlide = UIImage(named: "btn_arrow")!
        btnSlide.setImage(imgSlide, for: UIControlState())
        btnSlide.frame = CGRect(x: ScreenWidth /^ imgSlide.size.width, y: lblSlide.bottomYPoint + 4.0, width: imgSlide.size.width, height: imgSlide.size.height)
        btnSlide.addTarget(self, action: #selector(self.slide), for: .touchUpInside)
        
        self.addSubview(btnSlide)
    }
    
    func slide() {
        let headerView = parentView.rectForHeader(inSection: 1)
        parentView.setContentOffset(CGPoint(x: 0.0, y: headerView.origin.y), animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
