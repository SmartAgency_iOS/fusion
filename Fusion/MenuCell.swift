//
//  MenuCell.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    var lblTitle: UILabel!
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, title: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = Colors.DarkGray
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: 68.0, y: (MenuCell.cellHeight /^ 32.0) + 1.0, width: self.frame.size.width * 0.7, height: 32.0))
        lblTitle.text = title
        lblTitle.font = Fonts.Regular(20.0)
        lblTitle.textColor = Colors.White
        lblTitle.adjustWidth()
        
        self.addSubview(lblTitle)
        
        // White dash before title
        let viewDash = UIView(frame: CGRect(x: 28.0, y: lblTitle.center.y, width: 30.0, height: 1.0))
        viewDash.backgroundColor = Colors.White
        
        self.addSubview(viewDash)
        
        // Separator
        let viewLine = UIView(frame: CGRect(x: 0.0, y: MenuCell.cellHeight - 2.0, width: ScreenWidth * 0.7 , height: 2.0))
        viewLine.backgroundColor = Colors.LightGray
        
        self.addSubview(viewLine)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class var cellHeight: CGFloat {
        return 68.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
}
