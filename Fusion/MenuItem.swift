//
//  MenuItem.swift
//  Fusion
//
//  Created by Admin on 8/17/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class MenuItem: NSObject {
    
    var id: Int!
    var title: String!
    var titleEN: String!
    var category: Int!
    var price: Double!
    var excerpt: String!
    var excerptEN: String!
    var descr: String!
    var descrEN: String!
    var wineSuggestionTitle: String!
    var wineSuggestionTitleEN: String!
    var wineSuggestionDescription: String!
    var wineSuggestionDescriptionEN: String!
    var wineSuggestionPhoto: String!
    var wineSuggestionImage: UIImage!
    var wineSuggestionID: Int!
    var wineSuggestionPrice: Double!
    var wineSuggestionPriceBottle: Double!
    var wineSuggestionPriceGlass: Double!
    var isFavourite: Bool = false
    var hasSuggestion: Bool = false
    var image: Photo!
    var realImage: UIImage?
    var isWine: Bool = false
    
    class func parse(_ dictItem: [String: AnyObject]) -> MenuItem {
        let menuItem = MenuItem()
        
        if isNotNull(dictItem[WS.Params.id]) {
            menuItem.id = dictItem[WS.Params.id] as! Int
        }
        
        if isNotNull(dictItem[WS.Params.title]) {
            menuItem.title = dictItem[WS.Params.title] as! String
        }
        
        if isNotNull(dictItem[WS.Params.titleEN]) {
            menuItem.titleEN = dictItem[WS.Params.titleEN] as! String
        }
        
        if isNotNull(dictItem[WS.Params.description]) {
            menuItem.descr = dictItem[WS.Params.description] as! String
        }
        
        if isNotNull(dictItem[WS.Params.descriptionEN]) {
            menuItem.descrEN = dictItem[WS.Params.descriptionEN] as! String
        }
        
        if isNotNull(dictItem[WS.Params.category]) {
            menuItem.category = dictItem[WS.Params.category] as! Int
        }
        
        if isNotNull(dictItem[WS.Params.price]) {
            if let price = dictItem[WS.Params.price] as? Int {
                menuItem.price = Double(price)
            } else if let price = dictItem[WS.Params.price] as? Double {
                menuItem.price = price
            } else if let price = dictItem[WS.Params.price] as? String {
                menuItem.price = Double(price)
            }
        }
        
        if isNotNull(dictItem[WS.Params.excerpt]) {
            menuItem.excerpt = dictItem[WS.Params.excerpt] as! String
        }
        
        if isNotNull(dictItem[WS.Params.excerptEN]) {
            menuItem.excerptEN = dictItem[WS.Params.excerptEN] as! String
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionTitle]) {
            menuItem.wineSuggestionTitle = dictItem[WS.Params.wineSuggestionTitle] as! String
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionTitleEN]) {
            menuItem.wineSuggestionTitleEN = dictItem[WS.Params.wineSuggestionTitleEN] as! String
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionTitle]) && isNotNull(dictItem[WS.Params.wineSuggestionTitleEN]) {
            if menuItem.wineSuggestionTitle != "" && menuItem.wineSuggestionTitleEN != "" {
                menuItem.hasSuggestion = true
            }
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionDescription]) {
            menuItem.wineSuggestionDescription = dictItem[WS.Params.wineSuggestionDescription] as! String
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionDescriptionEN]) {
            menuItem.wineSuggestionDescriptionEN = dictItem[WS.Params.wineSuggestionDescriptionEN] as! String
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionPhoto]) {
            menuItem.wineSuggestionPhoto = dictItem[WS.Params.wineSuggestionPhoto] as! String
            let photo = Photo.parseURL(menuItem.wineSuggestionPhoto)
            menuItem.wineSuggestionImage = photo.getImage({ (image) in
                if image != nil {
                    menuItem.wineSuggestionImage = image
                }
            })
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionID]) {
            let stringID = dictItem[WS.Params.wineSuggestionID] as! String
            menuItem.wineSuggestionID = Int(stringID)
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionPrice]) {
            let stringPrice = dictItem[WS.Params.wineSuggestionPrice] as! String
            print(stringPrice)
            menuItem.wineSuggestionPrice = Double(stringPrice)
            print(Double(stringPrice))
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionPriceBottle]) {
            let stringPrice = dictItem[WS.Params.wineSuggestionPriceBottle] as! String
            menuItem.wineSuggestionPriceBottle = Double(stringPrice)
        }
        
        if isNotNull(dictItem[WS.Params.wineSuggestionPriceGlass]) {
            let stringPrice = dictItem[WS.Params.wineSuggestionPriceGlass] as! String
            menuItem.wineSuggestionPriceGlass = Double(stringPrice)
        }
        
        if isNotNull(dictItem[WS.Params.image]) {
            if let url = dictItem[WS.Params.image] as? String {
                menuItem.image = Photo.parseURL(url)
                
            }
        }
        
        return menuItem
    }
    
//    init(coder decoder: NSCoder) {
//        self.title = decoder.decodeObjectForKey("title") as! String
//        self.category = decoder.decodeObjectForKey("category") as! String
//        self.price = decoder.decodeDoubleForKey("price")
//        self.excerpt = decoder.decodeObjectForKey("excerpt") as! String
//        self.wineSuggestionTitle = decoder.decodeObjectForKey("wineSuggestionTitle") as! String
//        self.wineSuggestionDescription = decoder.decodeObjectForKey("wineSuggestionDescription") as! String
//        self.isFavourite = decoder.decodeBoolForKey("isFavourite")
//    }
//    
//    func encodeWithCoder(coder: NSCoder) {
//        coder.encodeObject(String(self.title), forKey: "title")
//        coder.encodeObject(String(self.category), forKey: "category")
//        coder.encodeDouble(self.price, forKey: "price")
//        coder.encodeObject(String(self.excerpt), forKey: "excerpt")
//        coder.encodeObject(String(self.wineSuggestionTitle), forKey: "wineSuggestionTitle")
//        coder.encodeObject(String(self.wineSuggestionDescription), forKey: "wineSuggestionDescription")
//        coder.encodeBool(self.isFavourite, forKey: "isFavourite")
//    }
    
    func toFavourite() {
        self.isFavourite = !self.isFavourite
        if isFavourite == true {
            arrFavourites.append(self)
        } else {
            for fav in arrFavourites {
                if fav == self {
                    arrFavourites.removeItem(fav)
                }
            }
//            if arrFavourites.contains(self) {
//                arrFavourites.removeItem(self)
//            }
        }
    }
    
    /*
     func insertItems() {
        let defaults = NSUserDefaults.standardUserDefaults()
 
         let data = NSKeyedArchiver.archivedDataWithRootObject(itemList)
         NSUserDefaults.standardUserDefaults().setObject(data, forKey: "myList")
     }
 
     func retrieveItems() {
        let defaults = NSUserDefaults.standardUserDefaults()
     
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("myList") as? NSData {
            let _mySavedList = NSKeyedUnarchiver.unarchiveObjectWithData(data) as [Item]
        }
     }
 */
 
}

func == (lhs: MenuItem, rhs: MenuItem) -> Bool {
    if lhs.id == nil || rhs.id == nil {
        return false
    }
    return (lhs.id == rhs.id)
}
