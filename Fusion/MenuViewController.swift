//
//  MenuViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let arrMenuItems: [String] = ["menu.cocktails".localized(), "menu.entries".localized(), "menu.meals".localized(), "menu.desserts".localized(), "menu.wines".localized(), "menu.daily".localized(), "menu.my.selection".localized(), "menu.philosophy".localized(), "menu.gallery".localized()]
    
    var tableView: UITableView!
    var headerView: UIView!
    var tapHeader: UITapGestureRecognizer!
    var lblHeader: UILabel!
    
    override func viewDidLoad() {
        
        self.view.backgroundColor = Colors.Dark
        
        // Header view
        self.headerView = UIView(frame: CGRect(x: 0.0, y: 25.0, width: ScreenWidth * 0.7, height: 80.0))
        self.headerView.backgroundColor = Colors.GreenOcean
        
        tapHeader = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.openHome))
        self.headerView.addGestureRecognizer(tapHeader)
        
        self.view.addSubview(headerView)
        
        // Header label
        self.lblHeader = UILabel(frame: CGRect(x: 28.0, y: self.headerView.frame.size.height /^ 30.0, width: self.headerView.frame.size.width * 0.7, height: 30.0))
        self.lblHeader.text = String(format: "menu.header".localized())
        self.lblHeader.textAlignment = .left
        self.lblHeader.textColor = Colors.White
        self.lblHeader.font = Fonts.Regular(24.0)
        
        self.headerView.addSubview(self.lblHeader)
        
        // Table view
        tableView = UITableView(frame: CGRect(x: 0.0, y: self.headerView.bottomYPoint, width: ScreenWidth * 0.7, height: ScreenHeight))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.bounces = false
        
        self.view.addSubview(tableView)
    }
    
    func openHome() {
        appDelegate.showCategoriesStoryboard()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = MenuCell(style: .default, reuseIdentifier: "MenuCell", title: arrMenuItems[(indexPath as NSIndexPath).row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        toggleLeft()
        
        if slideMenuController()?.mainViewController is DetailsViewController {
            slideMenuController()?.mainViewController?.dismiss(animated: false, completion: nil)
        }
        
        switch (indexPath as NSIndexPath).row {
        case 0:
            appDelegate.showCategoryStoryboard(SelectedCategory.Cocktails)
        case 1:
            appDelegate.showCategoryStoryboard(SelectedCategory.Entries)
        case 2:
            appDelegate.showMealsListStoryboard()
        case 3:
            appDelegate.showCategoryStoryboard(SelectedCategory.Desserts)
        case 4:
            appDelegate.showWineListStoryboard()
        case 5:
            appDelegate.showCategoryStoryboard(SelectedCategory.DailyMenu)
        case 6:
            appDelegate.showFavouritesStoryboard()
        case 7:
            appDelegate.showPhilosophyStoryboard()
        case 8:
            appDelegate.showGalleryStoryboard()
        default:
            print("default")
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MenuCell.cellHeight
    }
    
}
