//
//  NSDate+Extension.swift
//  GuessWhat
//
//  Created by Angel Antonov on 6/10/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

import UIKit

extension Date {
    var age: String {
        let dateComponents = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: self)
        let todayComponents = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: Date())
        
        var years = todayComponents.year! - dateComponents.year!
        
        if todayComponents.month == dateComponents.month {
            if todayComponents.day! < dateComponents.day! {
                years -= 1
            }
        }
        else if todayComponents.month! < dateComponents.month! {
            years -= 1
        }
        
        return String("\(years)")
    }
    
    func dateString(_ format: String) -> String {
        let df = DateFormatter()
        df.dateFormat = format
        
        return df.string(from: self)
    }
    
    func unitFor(_ unit: NSCalendar.Unit) -> Int {
        let dateComponents = (Calendar.current as NSCalendar).components([unit], from: self)
        
        if unit == .day {
            return dateComponents.day!
        }
        else if unit == .month {
            return dateComponents.month!
        }
        else if unit == .year {
            return dateComponents.year!
        }
        else if unit == .weekday {
            return dateComponents.weekday!
        }
        else if unit == .minute {
            return dateComponents.minute!
        }
        else if unit == .hour {
            return dateComponents.hour!
        }
        
        return 0
    }
    
    var isToday: Bool {
        let dateComponents = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: self)
        let todayComponents = (Calendar.current as NSCalendar).components([NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year], from: Date())
        
        if dateComponents.day == todayComponents.day && dateComponents.month == todayComponents.month && dateComponents.year == todayComponents.year {
            return true
        }
        
        return false
    }
    
    func isPartOfMonth(_ month: Int, andYear year: Int) -> Bool {
        let dateComponents = (Calendar.current as NSCalendar).components([NSCalendar.Unit.year, NSCalendar.Unit.month], from: self)
        
        return dateComponents.year == year && dateComponents.month == month
    }
    
    func isGreaterThanDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending
        {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    
    func isLessThanDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending
        {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    
    func isEqual2Date(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedSame
        {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func timeFromToday(_ eventDate: Date) -> TimeInterval {
        let eventTime = eventDate
        let timeInterval = eventTime.timeIntervalSince(self)
        
        return timeInterval
    }
    
    func stringTimeLeftUntilEvent(_ eventDate: Date) -> String {
        let interval: TimeInterval = self.timeFromToday(eventDate)
        var timeLeft = "-"
        
        let oneDayInSeconds: TimeInterval = 60 * 60 * 24
        
        if interval > oneDayInSeconds {
            var days = interval / oneDayInSeconds
            
            if days.truncatingRemainder(dividingBy: 1.0) > 0.5 {
                days += 0.5
            }
            
            timeLeft = "\(Int(days)) \(days > 1.999 ? "days".localized : "day".localized)"
        } else if interval > 0 {
            let minutes = (interval / 60).truncatingRemainder(dividingBy: 60)
            let hours = (interval / 3600)
            
            let strMinutes = minutes > 9 ? "\(Int(minutes))" : "0\(Int(minutes))"
            let strHours = hours > 9 ? "\(Int(hours))" : "0\(Int(hours))"
            
            timeLeft = "\(strHours):\(strMinutes)"
        } else {
            timeLeft = "-"
        }
        return timeLeft
    }
}
