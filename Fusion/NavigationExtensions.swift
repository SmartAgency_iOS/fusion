//
//  NavigationExtensions.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import Foundation

class NavigationExtensions: UIViewController {
    
    var customNavigationBar: UIView!
    var imgViewLogo: UIImageView!
    var btnLeft: UIButton!
    var btnRight: UIButton!
    var fromFavourites: Bool = false
    var fromCategory: Bool = false
    var categoryy: SelectedCategory?

    // Initialize custom navigation bar with/without menu button
    func customNavigationBarWith(menuButton: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        // Add bar
        let imgBar = UIImage(named: "topbar")!
        customNavigationBar = UIView(frame: CGRect(x: 0.0, y: 0.0, width: ScreenWidth, height: Constants.NavBarHeight))
        customNavigationBar!.backgroundColor = Colors.White
        
        self.view.addSubview(customNavigationBar!)
        
        // Add logo to navigation bar
        let imgLogo = UIImage(named: "topbar_logo")!
        imgViewLogo = UIImageView(image: imgLogo)
        imgViewLogo.frame = CGRect(x: ScreenWidth /^ imgLogo.size.width, y: Constants.NavBarHeight /^ imgLogo.size.height, width: imgLogo.size.width, height: imgLogo.size.height)
        
        customNavigationBar!.addSubview(imgViewLogo)
        
        // Add menu button
        if (menuButton) {
            self.addMenuButton()
        }

    }
    
    func addMenuButton() {
        btnLeft = UIButton(type: .custom)
        let imgMenu = UIImage(named: "menu")!
        btnLeft.setImage(imgMenu, for: UIControlState())
        btnLeft.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 0.0)
        btnLeft.frame = CGRect(x: 0.0, y: 0.0, width: imgMenu.size.width * 2, height: Constants.NavBarHeight)
        btnLeft.addTarget(self, action: #selector(UIViewController.toggleLeft), for: .touchUpInside)
        
        customNavigationBar!.addSubview(btnLeft)
    }
    
    func addBackButton() {
        btnRight = UIButton(type: .custom)
        let imgArrow = UIImage(named: "back_arrow")!
        btnRight.setImage(imgArrow, for: UIControlState())
        btnRight.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -10.0, bottom: 0.0, right: 0.0)
        btnRight.setTitle("categories.back".localized(), for: .normal)
        btnRight.setTitleColor(Colors.Black, for: UIControlState())
        btnRight.titleLabel?.font = Fonts.Medium(12.0)
        btnRight.frame = CGRect(x: ScreenWidth - 80.0, y: 0.0, width: 80.0, height: Constants.NavBarHeight)
        btnRight.addTarget(self, action: #selector(NavigationExtensions.goBack), for: .touchUpInside)
        
        customNavigationBar!.addSubview(btnRight)
    }
    
    func addCloseButton() {
        btnRight = UIButton(type: .custom)
        let imgArrow = UIImage(named: "back_arrow")!
        btnRight.setImage(imgArrow, for: UIControlState())
        btnRight.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: -10.0, bottom: 0.0, right: 0.0)
        btnRight.setTitle("categories.back".localized(), for: .normal)
        btnRight.setTitleColor(Colors.Black, for: UIControlState())
        btnRight.titleLabel?.font = Fonts.Medium(12.0)
        btnRight.frame = CGRect(x: ScreenWidth - 80.0, y: 0.0, width: 80.0, height: Constants.NavBarHeight)
        btnRight.addTarget(self, action: #selector(NavigationExtensions.close), for: .touchUpInside)
        
        customNavigationBar!.addSubview(btnRight)
    }
    
    func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func goBack() {
        if btnRight.parentViewController!.isKind(of: CategoriesViewController.self) {
            let alert = UIAlertController(title: "", message: "alert.delete.favourites".localized(), preferredStyle: UIAlertControllerStyle.alert)
            let alertActionOK = UIAlertAction(title: "alert.ok".localized, style: UIAlertActionStyle.default) { (alertAction: UIAlertAction) -> Void in
                arrFavourites.removeAll()
                appDelegate.showHomeStoryboard()
            }
            let alertActionCancel = UIAlertAction(title: "alert.cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(alertActionOK)
            alert.addAction(alertActionCancel)
            self.present(alert, animated: true, completion: nil)
        } else if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
            if fromFavourites == true {
                appDelegate.showCategoriesStoryboard()
            }
//            if self.navigationController?.isBeingDismissed() == false {
//
//            }
        } else {
            if fromCategory == true {
                self.dismiss(animated: true, completion: nil)
            } else {
                appDelegate.showCategoriesStoryboard()
            }
            

        }
    }
    
}
