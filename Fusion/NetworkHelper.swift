//
//  NetworkHelper.swift
//  GuessWhat
//
//  Created by Angel Antonov on 2/18/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension URL {
    //#if DEV
    static let RestServer = "http://fusion.smartinteractive.net/wp-json/fsn-api/" //"http://fusionpad.smartinteractive.net/wp-json/fsn-api/"
    //#elseif PROD
    //static let RestServer = "http://prod-dot-smart-guesswhaaat.appspot.com/rest/1"
    //#endif
}

// MARK: - Requests to server

func postRequest(_ params: [String: AnyObject]?) -> Request {
    //let req = request(Method.POST, URL.RestServer, parameters: params, encoding: ParameterEncoding.json, headers: nil)
    let req = Alamofire.request(URL.RestServer, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
    return req
}

func getRequest(_ operation: String, params: [String: AnyObject]?) -> Request {
    let req = Alamofire.request(URL.RestServer + "\(operation)", method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
    return req
}

// MARK: - Reponse method handlers

func validateAllData(_ response: DataResponse<Any>) -> AnyObject? {
    if response.response?.statusCode == 200 {
        if let result = response.result.value as? NSDictionary {
            return result
        }
    }
    
    return nil
}

func validateResponse(_ response: DataResponse<Any>) -> AnyObject? {
    if let result = response.result.value as? NSDictionary {
        if result[WS.Params.statusCode] as! Int == OperationResult.success.rawValue {
            return result
        }
    }
    
    return nil
}

func getErrorFromResponse(_ response: DataResponse<Any>) -> AnyObject? {
    if let result = response.result.value as? NSDictionary {
        return result
    }
    
    return nil
}

func getErrorTypeFromResult(_ result: AnyObject) -> String? {
    if let type = result["operationResult"] as? String {
        return type
    }
    
    return nil
}

// MARK: - Images

func imageFromUrl(_ url: Foundation.URL, callback: @escaping (_ image: UIImage?) -> () ) {
    let request = URLRequest(url: url)
    
    NSURLConnection.sendAsynchronousRequest(request, queue: .main) { (response, data, error) in
        if let imageData = data {
            callback(UIImage(data: imageData))
        }
        else {
            callback(nil)
        }
    }
    /*NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) {
        (response: URLResponse?, data: Data?, error: NSError?) -> Void in
        if let imageData = data {
            callback(image: UIImage(data: imageData))
        }
        else {
            callback(image: nil)
        }
    }*/
}

func imageFromUrlString(_ urlString: String, callback: @escaping (_ image: UIImage?) -> () ) {
    if let url = Foundation.URL(string: urlString) {
        let request = URLRequest(url: url)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: .main) { (response, data, error) in
            if let imageData = data {
                callback(UIImage(data: imageData))
            }
            else {
                callback(nil)
            }
        }
    }
}
