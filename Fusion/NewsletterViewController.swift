//
//  Newsletter.swift
//  Fusion
//
//  Created by Admin on 8/11/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import WebKit

class NewsletterViewController: NavigationExtensions {
    
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        self.view.backgroundColor = Colors.Black
        
        webView = WKWebView(frame: CGRect(x: 0.0, y: Constants.NavBarHeight, width: ScreenWidth, height: ScreenHeight - Constants.NavBarHeight))
        
        self.view.addSubview(webView)
        
        let url = Foundation.URL(string: "http://the-fusion.ch/newsletter/")
        let request = NSMutableURLRequest(url: url!)
        webView.load(request as URLRequest)
        
    }
    
}
