//
//  Object+Extension.swift
//  GuessWhat
//
//  Created by Angel Antonov on 5/28/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

import UIKit

extension NSObject {
    func log() {
        NSLog(self as! String)
    }
}

extension NSObject {
    func saveToUserDefaults(forKey key: String) {
        let archivedData = NSKeyedArchiver.archivedData(withRootObject: self)
        
        UserDefaults.standard.set(archivedData, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    static func loadObjectFromUserDefaults(forKey key: String) -> AnyObject? {
        if let data = UserDefaults.standard.object(forKey: key) as? Data {
            if let item = NSKeyedUnarchiver.unarchiveObject(with: data) {
                return item as AnyObject?
            }
        }
        
        return nil
    }
    
    static func removeObjectFromUserDefaults(forKey key: String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
}
