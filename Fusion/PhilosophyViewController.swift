//
//  PhilosophyViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import Alamofire

class PhilosophyViewController: NavigationExtensions {
    
    var lblTitle: UILabel!
    var lblSubtitle: UILabel!
    var lblDescription: UILabel!
    var imgViewBig: UIImageView!
    var imgViewSmall: UIImageView!
    
    var restaurantDescription: String = ""
    var restaurantTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.Black
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        self.fromFavourites = true
        
        //self.getInfo {
        
        
            // Title
            self.lblTitle = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: Constants.NavBarHeight + 100.0, width: ScreenWidth * 0.8, height: 86.0))
            self.lblTitle.text = String(format: "philosophy.title".localized())
            self.lblTitle.textColor = Colors.White
            self.lblTitle.textAlignment = .left
            self.lblTitle.font = Fonts.Bold(42.0)
            self.lblTitle.numberOfLines = 2
            
            self.view.addSubview(self.lblTitle)
            
            // Subtitle
            self.lblSubtitle = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: self.lblTitle.bottomYPoint + 16.0, width: ScreenWidth * 0.8, height: 24.0))
            self.lblSubtitle.text = String(format: "philosophy.subtitle".localized())
            self.lblSubtitle.textColor = Colors.LighterGray
            self.lblSubtitle.textAlignment = .left
            self.lblSubtitle.setFont(Fonts.Medium(22.0), string: "philosophy.fusion".localized())
            self.lblSubtitle.setFont(Fonts.Regular(22.0), string: "philosophy.by.sushizen".localized())
            
            self.view.addSubview(self.lblSubtitle)
            
            // Description
            self.lblDescription = UILabel(frame: CGRect(x: ScreenWidth * 0.1, y: self.lblSubtitle.bottomYPoint + 20.0, width: ScreenWidth * 0.4, height: 20.0))
            self.lblDescription.text = self.restaurantDescription.frenchalize
            self.lblDescription.textColor = Colors.White
            self.lblDescription.font = Fonts.Regular(18.0)
            self.lblDescription.textAlignment = .left
            self.lblDescription.adjustHeight()
            
            self.view.addSubview(self.lblDescription)
            
            // Big image
            let imgBig = UIImage(named: "portrait_image_example")!
            self.imgViewBig = UIImageView()
            self.imgViewBig.frame = CGRect(x: ScreenWidth - imgBig.size.width, y: Constants.NavBarHeight, width: imgBig.size.width, height: ScreenHeight - Constants.NavBarHeight)
            
            self.view.addSubview(self.imgViewBig)
            
            // Small image
            let imgSmall = UIImage(named: "small_image_example")!
            self.imgViewSmall = UIImageView()
            self.imgViewSmall.frame = CGRect(x: ScreenWidth - imgSmall.size.width, y: self.lblTitle.frame.origin.y, width: imgSmall.size.width + 5.0, height: imgSmall.size.height)
            self.imgViewSmall.layer.borderWidth = 10.0
            self.imgViewSmall.layer.borderColor = Colors.Black.cgColor
            
            self.view.addSubview(self.imgViewSmall)
        
            self.loadCachedData()
        //}
        
    }
    
//    func getInfo(_ success: @escaping (() -> ())) {
//        var info = [String: AnyObject]()
//        
//        Alamofire.request(URL.RestServer + OperationType.RESTAURANT_INFO.rawValue, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//            if let result = validateResponse(response) {
//                info = result as! [String: AnyObject]
//                if let descr = result["description"] as? String {
//                    self.restaurantDescription = descr.frenchalize
//                }
//                if let titlee = result["title"] as? String {
//                    self.restaurantTitle = titlee
//                }
//                if let photoPortrait = result["photoPortrait"] as? String {
//                    let photo = Photo.parseURL(photoPortrait)
//                    photo.getImage({ (downloadedImage) in
//                        self.imgViewBig.image = downloadedImage
//                    })
//                }
//                if let photoLandscape = result["photoLandscape"] as? String {
//                    let photo = Photo.parseURL(photoLandscape)
//                    photo.getImage({ (downloadedImage) in
//                        self.imgViewSmall.image = downloadedImage
//                    })
//                }
//                self.cacheData(info)
//                success()
//            } else {
//                let dict = self.loadCachedData()
//                self.setCachedItems(dict)
//                success()
//            }
//
//        }
//    
//    }
    
//    func setCachedItems(_ info: [String: AnyObject]) {
//        if let descr = info["description"] as? String {
//            self.restaurantDescription = descr.frenchalize
//        }
//        if let titlee = info["title"] as? String {
//            self.restaurantTitle = titlee
//        }
//        if let photoPortrait = info["photoPortrait"] as? String {
//            let photo = Photo.parseURL(photoPortrait)
//            photo.getImage({ (downloadedImage) in
//                self.imgViewBig.image = downloadedImage
//            })
//        }
//        if let photoLandscape = info["photoLandscape"] as? String {
//            let photo = Photo.parseURL(photoLandscape)
//            photo.getImage({ (downloadedImage) in
//                self.imgViewSmall.image = downloadedImage
//            })
//        }
//    }
    
    
    func loadCachedData() {
        let userDefaults = UserDefaults.standard
        var info = [String: AnyObject]()

        if let set = userDefaults.object(forKey: "philosophy") as? [String: AnyObject] {
            info = set
            
            if language == "en" {
                if let descr = info["descriptionEN"] as? String {
                    self.lblDescription.text = descr.frenchalize
                    self.lblDescription.adjustHeight()
                }
                
                if let title = info["titleEN"] as? String {
                    restaurantTitle = title
                }
            } else {
                if let descr = info["description"] as? String {
                    self.lblDescription.text = descr.frenchalize
                    self.lblDescription.adjustHeight()
                }
                
                if let title = info["title"] as? String {
                    restaurantTitle = title
                }
            }
            
            if let img1str = info["photoPortrait"] as? String {
                let photo = Photo.parseURL(img1str)
                if let img1 = photo.getImage({ (image) in
                    if image != nil {
                        self.imgViewBig.image = image!
                    }
                }) {
                    imgViewBig.image = img1
                }
            }
            
            if let img2str = info["photoLandscape"] as? String {
                let photo = Photo.parseURL(img2str)
                if let img2 = photo.getImage({ (image) in
                    if image != nil {
                        self.imgViewBig.image = image!
                    }
                }) {
                    imgViewSmall.image = img2
                }
            }

            
        }
        
//        if dictRestaurantImages["photoPortrait"] != nil {
//            self.imgViewBig.image = dictRestaurantImages["photoPortrait"]
//        }
//        if dictRestaurantImages["photoLandscape"] != nil {
//            self.imgViewSmall.image = dictRestaurantImages["photoLandscape"]
//        }

    }
    
//    func cacheData(_ info: [String: AnyObject]) {
//        let userDefaults = UserDefaults.standard
//        userDefaults.set(info, forKey: "philosophy")
//        userDefaults.synchronize()
//    }
    
}
