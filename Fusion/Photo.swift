//
//  Category.swift
//  Fusion
//
//  Created by Admin on 8/18/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

struct Photo {
    
    var url: Foundation.URL!
    var id: Int!
    var photoURL: String!
    var title: String!
    
    static func parse(_ dict: [String: AnyObject]) -> Photo {
        var newPhoto = Photo()
        
        if isNotNull(dict[WS.Params.id]) {
            newPhoto.id = dict[WS.Params.id] as! Int
        }
        
        if isNotNull(dict[WS.Params.title]) {
            newPhoto.title = dict[WS.Params.title] as! String
        }
        
        if isNotNull(dict[WS.Params.image]) {
            newPhoto.photoURL = dict[WS.Params.image] as! String
            newPhoto.url = Foundation.URL(string: newPhoto.photoURL)
        }
        
        return newPhoto
    }
    
    static func parseURL(_ url: String) -> Photo {
        var newPhoto = Photo()
        
        newPhoto.url = Foundation.URL(string: url)
        
        return newPhoto
    }
    
    /**
     Deletes photo from the file system
     */
    func delete() {
        PhotoUtil.deletePhoto(self)
    }
    
    /**
     Get an UIImage for Photo.
     If there is no image on the file system, download it from the server and send it back through the **downloadCallback**
     */
    func getImage(_ downloadCallback: @escaping (_ downloadedImage: UIImage?)->()) -> UIImage? {
        return PhotoUtil.getImageForPhoto(self) { (downloadedImage) in
            downloadCallback(downloadedImage)
        }
    }
}

class PhotoUtil {
    /** Download and save Photo locally */
    class func downloadAndSave(_ photo: Photo, callback: @escaping (_ image: UIImage?)->()) {
        guard let photoURL = photo.url else {
            callback(nil)
            return
        }
        
        imageFromUrl(photoURL) { (image) in
            if let downloadedImage = image {
                PhotoUtil.savePhoto(photo, image: downloadedImage)
                callback(downloadedImage)
            }
            else {
                callback(nil)
            }
        }
    }
    
    /** Save an image to the file system */
    class func savePhoto(_ photo: Photo, image: UIImage) {
        let profileImagePath = PhotoUtil.checkAndCreateFolderForPhotos(photo)
        print(profileImagePath)
        try? UIImagePNGRepresentation(image)!.write(to: Foundation.URL(fileURLWithPath: profileImagePath), options: [.atomic])
    }
    
    /** Check if there is a folder on the file system for Photos
     If not create the folder */
    class func checkAndCreateFolderForPhotos(_ photo: Photo) -> String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let photosPath = String("\(documentsPath)/Photos")
        
        if (!PhotoUtil.directoryExistsAtPath(photosPath!)) {
            do {
                try FileManager.default.createDirectory(atPath: photosPath!, withIntermediateDirectories: true, attributes: nil)
            } catch {
                
            }
        }
        var strUrl = photo.url.absoluteString
        strUrl = strUrl.components(separatedBy: "/").last!
        print(strUrl)
        let userProfileImagesPath = String("\(photosPath!)/\(strUrl)")
        
        return userProfileImagesPath!
    }
    
    class func directoryExistsAtPath(_ strPath: String) -> Bool {
        var isDir : ObjCBool = false
        if FileManager.default.fileExists(atPath: strPath, isDirectory: &isDir) {
            return isDir.boolValue ? true : false
        }
        
        return false
    }
    
    /**
     Get an UIImage for Photo.
     If there is no image on the file system download it from the server and send it back through the **downloadCallback**
     */
    class func getImageForPhoto(_ forPhoto: Photo, downloadCallback: @escaping (_ downloadedImage: UIImage?)->()) -> UIImage? {
        if let photo = PhotoUtil.getImageFromFileSystem(forPhoto) {
            return photo
        }
        
        PhotoUtil.downloadAndSave(forPhoto) { (image) in
            if let downloadedImage = image {
                PhotoUtil.savePhoto(forPhoto, image: downloadedImage)
                downloadCallback(downloadedImage)
            }
            else {
                downloadCallback(nil)
            }
        }
        
        return nil
    }
    
    class func getImageFromFileSystem(_ photo: Photo) -> UIImage? {
        let imagePath = PhotoUtil.checkAndCreateFolderForPhotos(photo)
        
        return UIImage(contentsOfFile: imagePath)
    }
    
    /**
     Deletes photo from the file system
     */
    class func deletePhoto(_ photo: Photo) {
        let profileImagePath = PhotoUtil.checkAndCreateFolderForPhotos(photo)
        
        do {
            try FileManager.default.removeItem(atPath: profileImagePath)
        } catch {
            
        }
    }
}
