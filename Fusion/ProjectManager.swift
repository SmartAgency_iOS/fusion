//
//  ProjectManager.swift
//  Fusion
//
//  Created by Admin on 10/20/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class ProjectManager {
    
    static var userDefaults = UserDefaults.standard
    
    class func fetchRestaurantData(dictJSON: [String: AnyObject], _ success: @escaping ((_ success: Bool) -> Void)) {
        let myGroup = DispatchGroup()
        userDefaults.set(dictJSON, forKey: "philosophy")
        userDefaults.synchronize()
        
        if let photoLandscape = dictJSON["photoLandscape"] as? String {
            if photoLandscape.characters.count > 2 {
                myGroup.enter()
                let photo = Photo.parseURL(photoLandscape)
                photo.getImage({ (downloadedImage) in
                    if downloadedImage != nil {
                        dictRestaurantImages["photoLandscape"] = downloadedImage!
                    }
                    myGroup.leave()
                })
            }
        }
        if let photoPortrait = dictJSON["photoPortrait"] as? String {
            if photoPortrait.characters.count > 2 {
                myGroup.enter()
                let photo = Photo.parseURL(photoPortrait)
                photo.getImage({ (downloadedImage) in
                    if downloadedImage != nil {
                        dictRestaurantImages["photoPortrait"] = downloadedImage!
                    }
                    myGroup.leave()
                })
            }
        }
        myGroup.notify(queue: DispatchQueue.main, execute: {
//            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:yourArrayToBeSave]];

            print("success")
            success(true)
        })
        
    }
    
    class func fetchGalleryPhotos(dictJSON: [String: AnyObject],  _ success: @escaping ((_ success: Bool) -> Void)) {
        if let items = dictJSON["items"] as? [[String: AnyObject]] {
            let myGroup = DispatchGroup()
            
            userDefaults.set(items, forKey: "gallery")
            userDefaults.synchronize()
            
            for item in items {
                myGroup.enter()
                let photo = Photo.parse(item)
                arrCachedPhotos.append(photo)
                photo.getImage({ (downloadedImage) in
                    if downloadedImage != nil {
                        arrCachedGalleryImages.append(downloadedImage!)
                    }
                    myGroup.leave()
                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                print("success")
                success(true)
            })
        }
    }
    
    class func fetchCocktailsData(dictJSON: [String: AnyObject],  _ success: @escaping ((_ success: Bool) -> Void)) {
        if let items = dictJSON["items"] as? [[String: AnyObject]] {
            // Save to user defaults
            userDefaults.set(items, forKey: "cocktails-and-appetizers-items")
            userDefaults.synchronize()
            
            // Load images and save them
            let myGroup = DispatchGroup()
            for item in items {
                if let image = item["image"] as? String {
                    myGroup.enter()
                    let menuItem = MenuItem.parse(item)
                    menuItem.image?.getImage({ (image) in
                        if image != nil {
                            dictCocktailsImages["\(menuItem.id!)"] = image
                        }
                        myGroup.leave()
                    })
                    myGroup.notify(queue: DispatchQueue.main, execute: {
                        
                    })
                }
            }

        }
        // Load settings' images and save them
        if let settings = dictJSON["settings"] as? [String: AnyObject] {
            
            userDefaults.set(settings, forKey: "cocktails-and-appetizers-settings")
            userDefaults.synchronize()
            
            let myGroup = DispatchGroup()
            myGroup.enter()
            let category = Category.parse(settings)
            category.photo1?.getImage({ (image) in
                if image != nil {
                    dictCocktailsImages["photo1"] = image
                }
                myGroup.leave()
            })
            myGroup.enter()
            category.photo2?.getImage({ (image) in
                if image != nil {
                    dictCocktailsImages["photo2"] = image
                }
                myGroup.leave()
            })
            myGroup.notify(queue: DispatchQueue.main, execute: {
                print("success")
                success(true)
            })
        }
    }
    
    class func fetchEntriesData(dictJSON: [String: AnyObject],  _ success: @escaping ((_ success: Bool) -> Void)) {
        if let items = dictJSON["items"] as? [[String: AnyObject]] {
            // Save to user defaults
            userDefaults.set(items, forKey: "starters-items")
            userDefaults.synchronize()
            
            // Load images and save them
            let myGroup = DispatchGroup()
            for item in items {
                if let image = item["image"] as? String {
                    myGroup.enter()
                    let menuItem = MenuItem.parse(item)
                    menuItem.image?.getImage({ (image) in
                        if image != nil {
                            dictEntriesImages["\(menuItem.id!)"] = image
                        }
                        myGroup.leave()
                    })
                    myGroup.notify(queue: DispatchQueue.main, execute: {
                    })
                }
            }

        }
        // Load settings' images and save them
        if let settings = dictJSON["settings"] as? [String: AnyObject] {
            
            userDefaults.set(settings, forKey: "starters-settings")
            userDefaults.synchronize()
            
            let myGroup = DispatchGroup()
            myGroup.enter()
            let category = Category.parse(settings)
            category.photo1?.getImage({ (image) in
                if image != nil {
                    dictEntriesImages["photo1"] = image
                }
                myGroup.leave()
            })
            myGroup.enter()
            category.photo2?.getImage({ (image) in
                if image != nil {
                    dictEntriesImages["photo2"] = image
                }
                myGroup.leave()
            })
            myGroup.notify(queue: DispatchQueue.main, execute: {
                print("success")
                success(true)
            })
        }
    }

    class func fetchDessertsData(dictJSON: [String: AnyObject],  _ success: @escaping ((_ success: Bool) -> Void)) {
        if let items = dictJSON["items"] as? [[String: AnyObject]] {
            // Save to user defaults
            userDefaults.set(items, forKey: "desserts-items")
            userDefaults.synchronize()
            
            // Load images and save them
            let myGroup = DispatchGroup()
            for item in items {
                if let image = item["image"] as? String {
                    myGroup.enter()
                    let menuItem = MenuItem.parse(item)
                    menuItem.image?.getImage({ (image) in
                        if image != nil {
                            dictDessertsImages["\(menuItem.id!)"] = image
                        }
                        myGroup.leave()
                    })
                }
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
            })
        }
        // Load settings' images and save them
        if let settings = dictJSON["settings"] as? [String: AnyObject] {
            
            userDefaults.set(settings, forKey: "desserts-settings")
            userDefaults.synchronize()
            
            let myGroup = DispatchGroup()
            myGroup.enter()
            let category = Category.parse(settings)
            category.photo1?.getImage({ (image) in
                if image != nil {
                    dictDessertsImages["photo1"] = image
                }
                myGroup.leave()
            })
            myGroup.enter()
            category.photo2?.getImage({ (image) in
                if image != nil {
                    dictDessertsImages["photo2"] = image
                }
                myGroup.leave()
            })
            myGroup.notify(queue: DispatchQueue.main, execute: {
                print("success")
                success(true)
            })
        }
    }
    
    class func fetchWinesData(dictJSON: [String: AnyObject],  _ success: @escaping ((_ success: Bool) -> Void)) {
        if let items = dictJSON["items"] as? [[String: AnyObject]] {
            // Save to user defaults
            userDefaults.set(items, forKey: "wines-items")
            userDefaults.synchronize()
            
            // Load images and save them
            let myGroup = DispatchGroup()
            for item in items {
                if let image = item["image"] as? String {
                    myGroup.enter()
                    let menuItem = MenuItem.parse(item)
                    menuItem.image?.getImage({ (image) in
                        if image != nil {
                            dictWinesImages["\(menuItem.id!)"] = image
                        }
                        myGroup.leave()
                    })
                    myGroup.notify(queue: DispatchQueue.main, execute: {
                    })
                }
            }
            
        }
        // Load settings' images and save them
        if let settings = dictJSON["settings"] as? [String: AnyObject] {
            
            userDefaults.set(settings, forKey: "wines-settings")
            userDefaults.synchronize()
            
            let myGroup = DispatchGroup()
            myGroup.enter()
            let category = Category.parse(settings)
            category.photo1?.getImage({ (image) in
                if image != nil {
                    dictWinesImages["photo1"] = image
                }
                myGroup.leave()
            })
            myGroup.enter()
            category.photo2?.getImage({ (image) in
                if image != nil {
                    dictWinesImages["photo2"] = image
                }
                myGroup.leave()
            })
            myGroup.notify(queue: DispatchQueue.main, execute: {
                print("success")
                success(true)
            })
        }
    }
    
    class func fetchDailyMenuData(dictJSON: [String: AnyObject],  _ success: @escaping ((_ success: Bool) -> Void)) {
        if let items = dictJSON["items"] as? [[String: AnyObject]] {
            userDefaults.set(items, forKey: "daily-menu-items")
            userDefaults.synchronize()
            
            // Load images and save them
            let myGroup = DispatchGroup()
            for item in items {
                if let image = item["image"] as? String {
                    myGroup.enter()
                    let menuItem = MenuItem.parse(item)
                    menuItem.image?.getImage({ (image) in
                        if image != nil {
                            dictDailyMenuImages["\(menuItem.id!)"] = image
                        }
                        myGroup.leave()
                    })
                    myGroup.notify(queue: DispatchQueue.main, execute: {
                    })
                }
            }
            
        } else if let items = dictJSON["items"] as? [String: AnyObject] {
            var arrItems = [[String: AnyObject]] ()
                        for key in items {
                            if let item = key.value as? [String: AnyObject] {
                                arrItems.append(item)
                            }
                        }
            userDefaults.set(arrItems, forKey: "daily-menu-items")
            userDefaults.synchronize()
            
            // Load images and save them
            let myGroup = DispatchGroup()
            for item in arrItems {
                if let image = item["image"] as? String {
                    myGroup.enter()
                    let menuItem = MenuItem.parse(item)
                    menuItem.image?.getImage({ (image) in
                        if image != nil {
                            dictDailyMenuImages["\(menuItem.id!)"] = image
                        }
                        myGroup.leave()
                    })
                    myGroup.notify(queue: DispatchQueue.main, execute: {
                    })
                }
            }

        }
        
        // Load settings' images and save them
        if let settings = dictJSON["settings"] as? [String: AnyObject] {
            
            userDefaults.set(settings, forKey: "daily-menu-settings")
            userDefaults.synchronize()
            
            let myGroup = DispatchGroup()
            
            let category = Category.parse(settings)
            category.photo1?.getImage({ (image) in
                myGroup.enter()
                if image != nil {
                    dictDailyMenuImages["photo1"] = image
                }
                myGroup.leave()
            })
            
            category.photo2?.getImage({ (image) in
                myGroup.enter()
                if image != nil {
                    dictDailyMenuImages["photo2"] = image
                }
                myGroup.leave()
            })
            myGroup.notify(queue: DispatchQueue.main, execute: {
                print("success")
                success(true)
            })
        }
    }

    class func fetchMealsData(dictJSON: [String: AnyObject],  _ success: @escaping ((_ success: Bool) -> Void)) {
        if let items = dictJSON["items"] as? [[String: AnyObject]] {
            // Save to user defaults
            userDefaults.set(items, forKey: "main-courses-items")
            userDefaults.synchronize()
            
            // Load images and save them
            let myGroup = DispatchGroup()
            for item in items {
                if let image = item["image"] as? String {
                    myGroup.enter()
                    let menuItem = MenuItem.parse(item)
                    menuItem.image?.getImage({ (image) in
                        if image != nil {
                            dictMealsImages["\(menuItem.id!)"] = image
                        }
                        myGroup.leave()
                    })

                }
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
            })
        }
        // Load settings' images and save them
        if let settings = dictJSON["settings"] as? [String: AnyObject] {
            
            userDefaults.set(settings, forKey: "main-courses-settings")
            userDefaults.synchronize()
            
            let myGroup = DispatchGroup()
            if let fishSettings = settings["fish"] as? [String: AnyObject] {
                let category = Category.parse(fishSettings)
                category.photo1?.getImage({ (image) in
                    myGroup.enter()
                    if image != nil {
                        dictMealsImages["photo1"] = image!
                    }
                    myGroup.leave()
                })
                
                category.photo2?.getImage({ (image) in
                    myGroup.enter()
                    if image != nil {
                        dictMealsImages["photo2"] = image!
                    }
                    myGroup.leave()
                })
                
                myGroup.notify(queue: DispatchQueue.main, execute: {
                    success(true)
                })
            }
            
            if let mealSettings = settings["meat"] as? [String: AnyObject] {
                let category = Category.parse(mealSettings)
                category.photo1?.getImage({ (image) in
                    myGroup.enter()
                    if image != nil {
                        dictMealsImages["photo3"] = image!
                    }
                    myGroup.leave()
                })
                category.photo2?.getImage({ (image) in
                    myGroup.enter()
                    if image != nil {
                        dictMealsImages["photo4"] = image!
                    }
                    myGroup.leave()
                })
                myGroup.notify(queue: DispatchQueue.main, execute: {
                    success(true)
                })
            }
        }
    }
    
}
