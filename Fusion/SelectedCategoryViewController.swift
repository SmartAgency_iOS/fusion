//
//  SelectedCategoryViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import Alamofire

enum SelectedCategory: String {
    case Cocktails = "cocktails-and-appetizers"
    case Entries = "starters"
    case Desserts = "desserts"
    case DailyMenu = "daily-menu"
    case Wines = "wines"
    case Meals = "main-courses"
}

class SelectedCategoryViewController: NavigationExtensions, UITableViewDelegate, UITableViewDataSource, CategoryDelegate {
    
    var tableView: UITableView!
    var category: SelectedCategory!
    var arrItems: [MenuItem] = [MenuItem]()
    var headerView: CategoryHeaderView!
    
    var arrSansAlcohol = [MenuItem]()
    var arrSignature = [MenuItem]()
    var arrClassic = [MenuItem]()
    var arrAperitif = [MenuItem]()
    
    var arrStarters = [MenuItem]()
    var arrMainCourses = [MenuItem]()
    var arrDesserts = [MenuItem]()
    var arrWines = [WineItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.Black
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        
        // First table
        tableView = UITableView(frame: CGRect(x: 0.0, y: Constants.NavBarHeight, width: ScreenWidth, height: ScreenHeight - Constants.NavBarHeight), style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Colors.Dark
        tableView.separatorStyle = .none
        tableView.bounces = false
        headerView = CategoryHeaderView(selectedCategory: category)
        headerView.delegate = self
        tableView.tableHeaderView = headerView
        
        self.view.addSubview(tableView)
        
        self.loadData(category: category.rawValue) { 
            //
        }
        
        //self.loadCachedImages()
        
        // Test - load cache before API call
        //let settings = self.loadCachedData().0
        //let items = self.loadCachedData().1
        //self.setCachedItems(settings, items: items)
        
        //self.getSelectedItems()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "REFRESH_ITEM"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadMission), name: NSNotification.Name(rawValue: "REFRESH_ITEM"), object: nil)
    }
    
    func reloadMission(notification: NSNotification) {
        if let item = notification.userInfo?["menuItem"] as? MenuItem {
            for itm in arrItems {
                if itm == item {
                    itm.isFavourite = item.isFavourite
                    self.tableView.reloadData()
                    return
                }
            }
        }
    }
    
    func getSelectedItems() {
        arrItems.removeAll()
        arrWines.removeAll()
        
        var set = [String: AnyObject]()
        var it = [[String: AnyObject]]()
        
        let language: String = "language".localized
        let params = ["lang" : language] as [String: String]
        
        Alamofire.request(URL.RestServer + category.rawValue, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = validateResponse(response) {
                print(result)
                if let resDict = result as? [String: AnyObject] {
                    // get category
                    if let settings = resDict["settings"] as? [String: AnyObject] {
                        set = settings
                        let menuCategory = Category.parse(settings)
                        self.headerView.lblDescription.text = menuCategory.descr != nil ? menuCategory.descr.frenchalize : ""
                        self.headerView.lblSubtitle.text = menuCategory.subtitle != nil ? menuCategory.subtitle.frenchalize : ""
                        menuCategory.photo1?.getImage({ (downloadedImage1) in
                                menuCategory.photo2?.getImage({ (downloadedImage2) in
                                self.headerView.imgViewPortrait.image = downloadedImage1
                                self.headerView.imgViewLandscape.image = downloadedImage2
                                if downloadedImage1 != nil && downloadedImage2 != nil {
                                    self.cacheImages(downloadedImage1!, image2: downloadedImage2!)
                                }
                            })
                        })
                    }
                    // get all items
                    if let items = resDict["items"] as? [[String: AnyObject]] {
                        it = items
                        for item in items {
                            if item["colour"] != nil {
                                let wineItem = WineItem.parse(item)
                                
                                // check if is in favourites
                                for fav in arrFavourites {
                                    let menuItem = MenuItem()
                                    menuItem.id = wineItem.id
                                    menuItem.title = wineItem.title
                                    menuItem.price = wineItem.price
                                    if menuItem == fav {
                                        wineItem.isFavourite = true
                                    }
                                }
                                
                                self.arrWines.append(wineItem)
                                
                            } else {
                                let menuItem = MenuItem.parse(item)
                                // Check if item is in favourites list
                                for fav in arrFavourites {
                                    if menuItem == fav {
                                        menuItem.isFavourite = true
                                    }
                                }
                                self.arrItems.append(menuItem)
                            }
                        }
                        self.tableView.reloadData()
                    }
                    self.cacheData(set, items: it)
                }
            }
            else {
                let settings = self.loadCachedData().0
                let items = self.loadCachedData().1
                self.setCachedItems(settings, items: items)
            }

        }
    }
    

    
    func setCachedItems(_ settings: [String: AnyObject], items: [[String: AnyObject]]) {
        let menuCategory = Category.parse(settings)
        if language == "en" {
            self.headerView.lblDescription.text = menuCategory.descrEN != nil ? menuCategory.descrEN : ""
            self.headerView.lblSubtitle.text = menuCategory.subtitleEN != nil ? menuCategory.subtitleEN : ""
        } else {
            self.headerView.lblDescription.text = menuCategory.descr != nil ? menuCategory.descr.frenchalize : ""
            self.headerView.lblSubtitle.text = menuCategory.subtitle != nil ? menuCategory.subtitle.frenchalize : ""
        }
//        menuCategory.photo1?.getImage({ (downloadedImage1) in
//            menuCategory.photo2?.getImage({ (downloadedImage2) in
//                self.headerView.imgViewPortrait.image = downloadedImage1
//                self.headerView.imgViewLandscape.image = downloadedImage2
//            })
//        })
        // get all items
        print(items)
        for item in items {
            if item["colour"] != nil {
                let wineItem = WineItem.parse(item)
                // check if is in favourites
                for fav in arrFavourites {
                    let menuItem = MenuItem()
                    menuItem.id = wineItem.id
                    menuItem.title = wineItem.title
                    menuItem.titleEN = wineItem.titleEN
                    menuItem.price = wineItem.price
                    if menuItem == fav {
                        wineItem.isFavourite = true
                    }
                }
                self.arrWines.append(wineItem)
            } else {
                let menuItem = MenuItem.parse(item)
                for fav in arrFavourites {
                    if menuItem == fav {
                        menuItem.isFavourite = true
                    }
                }
                self.arrItems.append(menuItem)
            }
        }
        self.tableView.reloadData()
    }
    
    func loadCachedImages() {
        var image1: UIImage?
        var image2: UIImage?
        let userDefaults = UserDefaults.standard
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            if let imagee1 = userDefaults.object(forKey: "cocktails-image-portrait") as? Data {
                image1 = UIImage(data: imagee1)
                self.headerView.imgViewPortrait.image = image1
            }
            if let imagee2 = userDefaults.object(forKey: "cocktails-image-landscape") as? Data {
                image2 = UIImage(data: imagee2)
                self.headerView.imgViewLandscape.image = image2
            }
        case SelectedCategory.DailyMenu.rawValue:
            if let imagee1 = userDefaults.object(forKey: "daily-menu-image-portrait") as? Data {
                image1 = UIImage(data: imagee1)
                self.headerView.imgViewPortrait.image = image1
            }
            if let imagee2 = userDefaults.object(forKey: "daily-menu-image-landscape") as? Data {
                image2 = UIImage(data: imagee2)
                self.headerView.imgViewLandscape.image = image2
            }
        case SelectedCategory.Desserts.rawValue:
            if let imagee1 = userDefaults.object(forKey: "desserts-image-portrait") as? Data {
                image1 = UIImage(data: imagee1)
                self.headerView.imgViewPortrait.image = image1
            }
            if let imagee2 = userDefaults.object(forKey: "desserts-image-landscape") as? Data {
                image2 = UIImage(data: imagee2)
                self.headerView.imgViewLandscape.image = image2
            }
        case SelectedCategory.Entries.rawValue:
            if let imagee1 = userDefaults.object(forKey: "entries-image-portrait") as? Data {
                image1 = UIImage(data: imagee1)
                self.headerView.imgViewPortrait.image = image1
            }
            if let imagee2 = userDefaults.object(forKey: "entries-image-landscape") as? Data {
                image2 = UIImage(data: imagee2)
                self.headerView.imgViewLandscape.image = image2
            }
        default:
            break
        }
    }
    
    func cacheImages(_ image1: UIImage, image2: UIImage) {
        let userDefaults = UserDefaults.standard
        let data1 = UIImagePNGRepresentation(image1)
        let data2 = UIImagePNGRepresentation(image2)
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            userDefaults.set(data1, forKey: "cocktails-image-portrait")
            userDefaults.set(data2, forKey: "cocktails-image-landscape")
        case SelectedCategory.DailyMenu.rawValue:
            userDefaults.set(data1, forKey: "daily-menu-image-portrait")
            userDefaults.set(data2, forKey: "daily-menu-image-landscape")
        case SelectedCategory.Desserts.rawValue:
            userDefaults.set(data1, forKey: "desserts-image-portrait")
            userDefaults.set(data2, forKey: "desserts-image-landscape")
        case SelectedCategory.Entries.rawValue:
            userDefaults.set(data1, forKey: "entries-image-portrait")
            userDefaults.set(data2, forKey: "entries-image-landscape")
        default:
            break
        }
    }
    
    func loadCachedData() -> ([String: AnyObject], [[String: AnyObject]]) {
        let userDefaults = UserDefaults.standard
        var settings = [String: AnyObject]()
        var items = [[String: AnyObject]]()
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            if let set = userDefaults.object(forKey: "cocktails-settings") as? [String: AnyObject] {
                settings = set
            }
            if let it = userDefaults.object(forKey: "cocktails-items") as? [[String: AnyObject]] {
                items = it
            }
            return (settings, items)
        case SelectedCategory.DailyMenu.rawValue:
            if let set = userDefaults.object(forKey: "daily-menu-settings") as? [String: AnyObject] {
                settings = set
            }
            if let it = userDefaults.object(forKey: "daily-menu-items") as? [[String: AnyObject]] {
                items = it
            }
            return (settings, items)
        case SelectedCategory.Desserts.rawValue:
            if let set = userDefaults.object(forKey: "desserts-settings") as? [String: AnyObject] {
                settings = set
            }
            if let it = userDefaults.object(forKey: "desserts-items") as? [[String: AnyObject]] {
                items = it
            }
            return (settings, items)
        case SelectedCategory.Entries.rawValue:
            if let set = userDefaults.object(forKey: "entries-settings") as? [String: AnyObject] {
                settings = set
            }
            if let it = userDefaults.object(forKey: "entries-items") as? [[String: AnyObject]] {
                items = it
            }
            return (settings, items)
        default:
            return (settings, items)
        }
    }
    
    func cacheData(_ settings: [String: AnyObject], items: [[String: AnyObject]]) {
        let userDefaults = UserDefaults.standard
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            userDefaults.set(settings, forKey: "cocktails-settings")
            userDefaults.set(items, forKey: "cocktails-items")
            userDefaults.synchronize()
        case SelectedCategory.DailyMenu.rawValue:
            userDefaults.set(settings, forKey: "daily-menu-settings")
            userDefaults.set(items, forKey: "daily-menu-items")
            userDefaults.synchronize()
        case SelectedCategory.Desserts.rawValue:
            userDefaults.set(settings, forKey: "desserts-settings")
            userDefaults.set(items, forKey: "desserts-items")
            userDefaults.synchronize()
        case SelectedCategory.Entries.rawValue:
            userDefaults.set(settings, forKey: "entries-settings")
            userDefaults.set(items, forKey: "entries-items")
            userDefaults.synchronize()
        default:
            break
        }
    }
    
    func slide() {
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if category == SelectedCategory.Desserts || category == SelectedCategory.Entries {
            
            if (indexPath as NSIndexPath).row >= arrItems.count {
                let cell = EmptyCell(style: .default, reuseIdentifier: "EmptyCell")
                return cell
            } else {
                let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrItems[(indexPath as NSIndexPath).row])
                return cell
            }
            
        } else {
            
            if category == SelectedCategory.DailyMenu {
                switch (indexPath as NSIndexPath).section {
                case 0:
                    let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrStarters[(indexPath as NSIndexPath).row])
                    return cell
                case 1:
                    let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrMainCourses[(indexPath as NSIndexPath).row])
                    return cell
                case 2:
                    let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrDesserts[(indexPath as NSIndexPath).row])
                    return cell
                case 3:
                    let cell = WineCell(style: .default, reuseIdentifier: "WineCell", wineItem: arrWines[(indexPath as NSIndexPath).row])
                    return cell
                default:
                    let cell = EmptyCell(style: .default, reuseIdentifier: "EmptyCell")
                    return cell
                }
            }
            
            if category == SelectedCategory.Cocktails {
                switch (indexPath as NSIndexPath).section {
                case 0:
                    let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrSansAlcohol[(indexPath as NSIndexPath).row])
                    return cell
                case 1:
                    let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrSignature[(indexPath as NSIndexPath).row])
                    return cell
                case 2:
                    let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrClassic[(indexPath as NSIndexPath).row])
                    return cell
                case 3:
                    let cell = CategoryCell(style: .default, reuseIdentifier: "CategoryCell", category: category, item: arrAperitif[(indexPath as NSIndexPath).row])
                    return cell
                default:
                    break
                }
            }
        }
        // will never be executed?
        let cell = EmptyCell(style: .default, reuseIdentifier: "EmptyCell")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            return 4
        case SelectedCategory.Entries.rawValue:
            return 1
        case SelectedCategory.Desserts.rawValue:
            return 1
        case SelectedCategory.DailyMenu.rawValue:
            return 4
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Header
        let sectionHeader = UIView(frame: CGRect(x: 30.0, y: 0.0, width: ScreenWidth - 60.0, height: 100.0))
        
        // Header title
        let lblSectionTitle = UILabel(frame: CGRect(x: 30.0, y: 70.0, width: ScreenWidth - 60.0, height: 24.0))
        lblSectionTitle.font = Fonts.Medium(22.0)
        lblSectionTitle.textAlignment = .left
        lblSectionTitle.textColor = Colors.GreenOcean
        
        sectionHeader.addSubview(lblSectionTitle)
        
        // Separator
        let viewLine = UIView(frame: CGRect(x: 30.0, y: sectionHeader.frame.size.height - 3.0, width: ScreenWidth - 60.0, height: 2.0))
        viewLine.backgroundColor = Colors.White
        
        sectionHeader.addSubview(viewLine)
        
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            lblSectionTitle.text = self.setCocktailsSectionTitles(section)
            return sectionHeader
        case SelectedCategory.Entries.rawValue:
            return nil
        case SelectedCategory.Desserts.rawValue:
            return nil
        case SelectedCategory.DailyMenu.rawValue:
            lblSectionTitle.text = self.setDailyMenuSectionTitle(section)
            return sectionHeader
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        if category == SelectedCategory.Cocktails || category == SelectedCategory.DailyMenu {
            return 100.0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch category.rawValue {
        case SelectedCategory.Cocktails.rawValue:
            return self.setCocktailsSectionRows(section)
        case SelectedCategory.DailyMenu.rawValue:
            return setDailyMenuSectionRows(section)
        default:
            if arrItems.count > 8 {
                return arrItems.count
            } else {
                return 9
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if category == SelectedCategory.DailyMenu && (indexPath as NSIndexPath).section == 3 {
            return WineCell.cellHeight
        } else {
            return CategoryCell.cellHeight
        }
    }
    
    func setCocktailsSectionRows(_ section: Int) -> Int {
        // clear arrays
        arrSansAlcohol.removeAll()
        arrSignature.removeAll()
        arrClassic.removeAll()
        arrAperitif.removeAll()
        
        // break down all items into category arrays
        for item in arrItems {
            if item.category == 2 { // Classic
                arrClassic.append(item)
            } else if item.category == 3 { // Signature
                arrSignature.append(item)
            } else if item.category == 4 { // Sans alcohol
                arrSansAlcohol.append(item)
            } else if item.category == 5 { // Aperitif
                arrAperitif.append(item)
            }
        }
        
        // return the exact number of items for different sections/categories
        switch section {
        case 0:
            return arrSansAlcohol.count
            //return arrClassic.count
        case 1:
            return arrSignature.count
        case 2:
            return arrClassic.count
            //return arrSansAlcohol.count
        case 3:
            return arrAperitif.count
        default:
            return arrItems.count
        }
    }
    
    func setDailyMenuSectionRows(_ section: Int) -> Int {
        // clear arrays
        arrStarters.removeAll()
        arrMainCourses.removeAll()
        arrDesserts.removeAll()
        arrWines.removeAll()
        
        // break down all items into category arrays
        for item in arrItems {
            if item.category == 21 { // Starters
                arrStarters.append(item)
            } else if item.category == 22 { // Main Courses
                arrMainCourses.append(item)
            } else if item.category == 23 { // Desserts
                arrDesserts.append(item)
//            } else if item.category == 3 { // Wines
//                arrWines.append(item)
            }
        }
        
        // return the exact number of items for different sections/categories
        switch section {
        case 0:
            return arrStarters.count
        case 1:
            return arrMainCourses.count
        case 2:
            return arrDesserts.count
        case 3:
            return arrWines.count
        default:
            return arrItems.count
        }
    }
    
    func setCocktailsSectionTitles(_ section: Int) -> String {
        switch section {
        case 0:
            return String(format: "cocktails.section1".localized())
        case 1:
            return String(format: "cocktails.section2".localized())
        case 2:
            return String(format: "cocktails.section3".localized())
        case 3:
            return String(format: "cocktails.section4".localized())
        default:
            return String(format: "")
        }
    }
    
    func setDailyMenuSectionTitle(_ section: Int) -> String {
        switch section {
        case 0:
            return String(format: "daily.menu.section1".localized())
        case 1:
            return String(format: "daily.menu.section2".localized())
        case 2:
            return String(format: "daily.menu.section3".localized())
        case 3:
            return String(format: "daily.menu.section4".localized())
        default:
            return String(format: "")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailsFromCategory" {
            let detailsVC = segue.destination as! DetailsViewController
            if let item = sender as? MenuItem {
                detailsVC.menuItem = item
                detailsVC.fromCategory = true
            }
        }
    }
    
    func loadData(category: String, success: (() -> Void)) {
        var itemsArray = [[String: AnyObject]] ()
        var settingsDict = [String: AnyObject]()
        var imagesDict = [String: UIImage]()
        var image1 = UIImage()
        var image2 = UIImage()
        
        if let items = UserDefaults.standard.object(forKey: "\(category)-items") as? [[String: AnyObject]] {
            itemsArray = items
        }
        if let settings = UserDefaults.standard.object(forKey: "\(category)-settings") as? [String: AnyObject] {
            settingsDict = settings
        }
        
        switch category {
        case SelectedCategory.Cocktails.rawValue:
            imagesDict = dictCocktailsImages
//            image1 = dictCocktailsImages["photo1"] != nil ? dictCocktailsImages["photo1"]! : UIImage()
//            image2 = dictCocktailsImages["photo2"] != nil ? dictCocktailsImages["photo2"]! : UIImage()
        case SelectedCategory.DailyMenu.rawValue:
            imagesDict = dictDailyMenuImages
//            image1 = dictDailyMenuImages["photo1"] != nil ? dictDailyMenuImages["photo1"]! : UIImage()
//            image2 = dictDailyMenuImages["photo2"] != nil ? dictDailyMenuImages["photo2"]! : UIImage()
        case SelectedCategory.Desserts.rawValue:
            imagesDict = dictDessertsImages
//            image1 = dictDessertsImages["photo1"] != nil ? dictDessertsImages["photo1"]! : UIImage()
//            image2 = dictDessertsImages["photo2"] != nil ? dictDessertsImages["photo2"]! : UIImage()
        case SelectedCategory.Entries.rawValue:
            imagesDict = dictEntriesImages
//            image1 = dictEntriesImages["photo1"] != nil ? dictEntriesImages["photo1"]! : UIImage()
//            image2 = dictEntriesImages["photo2"] != nil ? dictEntriesImages["photo2"]! : UIImage()
        default:
            break
        }
        
        let menuCategory = Category.parse(settingsDict)
        
        if language == "en" {
            self.headerView.lblDescription.text = menuCategory.descrEN != nil ? menuCategory.descrEN : ""
            self.headerView.lblSubtitle.text = menuCategory.subtitleEN != nil ? menuCategory.subtitleEN : ""
        } else {
            self.headerView.lblDescription.text = menuCategory.descr != nil ? menuCategory.descr.frenchalize : ""
            self.headerView.lblSubtitle.text = menuCategory.subtitle != nil ? menuCategory.subtitle.frenchalize : ""
        }

        if let img1 = menuCategory.photo1?.getImage({ (image) in
            if image != nil {
                self.headerView.imgViewPortrait.image = image
            }
        }) {
            self.headerView.imgViewPortrait.image = img1
        }
        
        if let img2 = menuCategory.photo2?.getImage({ (image) in
            if image != nil {
                self.headerView.imgViewLandscape.image = image
            }
        }) {
            self.headerView.imgViewLandscape.image = img2
        }
//        self.headerView.imgViewPortrait.image = image1
//        self.headerView.imgViewLandscape.image = image2
        
        for item in itemsArray {
            if item["colour"] != nil {
                let wineItem = WineItem.parse(item)
                // check if is in favourites
                for fav in arrFavourites {
                    let menuItem = MenuItem()
                    menuItem.id = wineItem.id
                    menuItem.title = wineItem.title
                    menuItem.titleEN = wineItem.titleEN
                    menuItem.price = wineItem.price
                    if menuItem == fav {
                        wineItem.isFavourite = true
                    }
                }
                self.arrWines.append(wineItem)
            } else {
                let menuItem = MenuItem.parse(item)
                menuItem.realImage = imagesDict["\(menuItem.id!)"] != nil ? imagesDict["\(menuItem.id!)"] : UIImage()
                for fav in arrFavourites {
                    if menuItem == fav {
                        menuItem.isFavourite = true
                    }
                }
                self.arrItems.append(menuItem)
            }
        }
        self.tableView.reloadData()
    }
    
}
