//
//  String+Extension.swift
//  GuessWhat
//
//  Created by Angel Antonov on 5/28/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

import UIKit
import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    var unfrenchalize: String {
        let dict_a: [String: [String]] = ["a": ["à", "á", "â"]]
        let dict_A: [String: [String]] = ["A": ["À", "Á"]]
        let dict_e: [String: [String]] = ["e": ["è", "é"]]
        let dict_E: [String: [String]] = ["E": ["È", "É"]]
        
        let arrFrenchLetters: [[String: [String]]] = [dict_a, dict_A, dict_e, dict_E]
        
        var strToFix = self
        
        for dict in arrFrenchLetters {
            for key in dict.keys {
                for frenchLetter in dict[key]! {
                    strToFix = strToFix.replacingOccurrences(of: frenchLetter, with: key)
                }
            }
        }
        
        return strToFix
    }
    
    var frenchalize: String {
        let dict_a: [String: [String]] = ["â": ["â"]]
        let dict_aa: [String: [String]] = ["à": ["à"]]
        let dict_aaa: [String: [String]] = ["á": ["á"]]
        let dict_A: [String: [String]] = ["À": ["À"]]
        let dict_AA: [String: [String]] = ["Á": ["Á"]]
        let dict_AAA: [String: [String]] = ["Â": ["Â"]]
        let dict_e: [String: [String]] = ["è": ["è"]]
        let dict_ee: [String: [String]] = ["é": ["é"]]
        let dict_eee: [String: [String]] = ["ê": ["ê"]]
        let dict_E: [String: [String]] = ["È": ["È"]]
        let dict_EE: [String: [String]] = ["É": ["É"]]
        let dict_EEE: [String: [String]] = ["Ê": ["Ê"]]
        let dict_o: [String: [String]] = ["ô": ["ô"]]
        let dict_O: [String: [String]] = ["Ô": ["Ô"]]
        let dict_oo: [String: [String]] = ["ö": ["ö"]]
        let dict_OO: [String: [String]] = ["Ö": ["Ö"]]
        let dict_ooo: [String: [String]] = ["ö": ["ö"]]
        let dict_u: [String: [String]] = ["û": ["û"]]
        let dict_uu: [String: [String]] = ["ù": ["ù"]]
        let dict_i: [String: [String]] = ["î": ["î"]]
        let dict_oe: [String: [String]] = ["œ": ["œ"]]
        let dict_ae: [String: [String]] = ["æ": ["æ"]]
        let dict_c: [String: [String]] = ["ç": ["ç"]]
        let dict_C: [String: [String]] = ["Ç": ["Ç"]]
        let dict_I: [String: [String]] = ["Î": ["Î"]]
        let dict_U: [String: [String]] = ["Û": ["Û"]]
        let dict_UU: [String: [String]] = ["Ù": ["Ù"]]
        let dict_AE: [String: [String]] = ["Œ": ["Œ"]]
        let dict_OE: [String: [String]] = ["Æ": ["Æ"]]
        
        let arrFrenchLetters: [[String: [String]]] = [dict_a, dict_aa, dict_aaa, dict_A, dict_AA, dict_AAA, dict_e, dict_ee, dict_eee, dict_E, dict_EE, dict_EEE, dict_o, dict_oo, dict_O, dict_OO, dict_ooo, dict_u, dict_uu, dict_i, dict_oe, dict_ae, dict_c, dict_C, dict_I, dict_U, dict_UU, dict_AE, dict_OE]
        
        var strToFix = self
        
        for dict in arrFrenchLetters {
            for key in dict.keys {
                for frenchLetter in dict[key]! {
                    strToFix = strToFix.replacingOccurrences(of: frenchLetter, with: key)
                }
            }
        }
        
        return strToFix
    }
    
    var imageName : String {
        if UIScreen.main.bounds.height == 667.0 && self.contains("ip6") == false { // if it's iPhone 6 get the correct image
            return String("\(self)_ip6")
        }
        
        return self
    }
    
    func toDate(_ format: String) -> Date? {
        let df = DateFormatter()
        df.dateFormat = format
        df.locale = Locale(identifier: "en-US")
        
        return df.date(from: self)
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func contains(_ strText: String) -> Bool {
        return self.lowercased().range(of: strText.lowercased()) != nil
    }
    
    var log: Void {
        print(self)
    }
    
    func toDouble() -> Double? {
        let nf = NumberFormatter()
        nf.locale = Locale(identifier: "en-US")
        
        return nf.number(from: self)?.doubleValue
    }
    
    func underline(_ color: UIColor) -> NSMutableAttributedString {
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: self)
        attString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(0, self.characters.count))
        attString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0, self.localized.characters.count))
        
        return attString
    }
    
    func insert(_ string:String,ind:Int) -> String {
        return  String(self.characters.prefix(ind)) + string + String(self.characters.suffix(self.characters.count-ind))
    }
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func getDateWithFormat(_ stringFormat: String) -> Date? {
        let df = DateFormatter()
        df.dateFormat = stringFormat
        df.timeZone = TimeZone.autoupdatingCurrent
        df.locale = Locale.current
        
        return df.date(from: self)
    }
}
