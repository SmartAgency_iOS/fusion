//
//  DetailsViewController.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/5/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class SubDetailsViewController: NavigationExtensions {
    
    var scrollView: UIScrollView?
    var imgViewItem: UIImageView!
    var lblTitle: UILabel!
    var imgViewFav: FavouriteView!
    var lblFavourite: UILabel!
    var lblExcerpt: UILabel!
    var lblDescription: UILabel!
    var lblPriceDescription: UILabel!
    var lblPrice: UILabel!
    var viewLine: UIView!
    var menuItem: MenuItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customNavigationBarWith(menuButton: false)
        self.addCloseButton()
        self.view.backgroundColor = Colors.Black
        
        // Item image
        let imgItem = UIImage(named: "detail_image_example")!
        imgViewItem = UIImageView(frame: CGRect(x: 0.0, y: Constants.NavBarHeight, width: ScreenWidth, height: imgItem.size.height))
        //self.loadDetailImage(menuItem.id)
//        if let img = menuItem.image {
//            img.getImage({ (downloadedImage) in
//                self.imgViewItem.image = downloadedImage
//                if downloadedImage != nil && self.menuItem.id != nil {
//                    self.cacheDetailImage(downloadedImage!, id: self.menuItem.id)
//                }
//            })
//        } else if menuItem.realImage != nil {
        imgViewItem.image = menuItem.realImage!
//        }
        self.view.addSubview(imgViewItem)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: 16.0, y: imgViewItem.bottomYPoint + 20.0, width: ScreenWidth * 0.8, height: 42.0))
        lblTitle.text = menuItem.title != nil ? menuItem.title : ""
        lblTitle.textColor = Colors.White
        lblTitle.textAlignment = .left
        lblTitle.font = Fonts.Bold(40.0)
        lblTitle.adjustsFontSizeToFitWidth = true
        
        self.view.addSubview(lblTitle)
        
        // Add to favourites image
        imgViewFav = FavouriteView(coordinates: CGPoint(x: 16.0, y: lblTitle.bottomYPoint + 8.0), item: menuItem)
        imgViewFav.frame.size = CGSize(width: imgViewFav.frame.size.width + 12.0, height: imgViewFav.frame.size.height + 12.0)
        
        self.view.addSubview(imgViewFav)
        
        // Add to favourites label
        lblFavourite = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 15.0, y: imgViewFav.frame.origin.y + 4.0, width: ScreenWidth * 0.7, height: 16.0))
        lblFavourite.text = String(format: "details.add.to.favourites".localized())
        lblFavourite.textColor = Colors.LightGray
        lblFavourite.textAlignment = .left
        lblFavourite.font = Fonts.Medium(14.0)
        
        self.view.addSubview(lblFavourite)
        
        // Excerpt
        lblExcerpt = UILabel(frame: CGRect(x: 16.0, y: lblFavourite.bottomYPoint + 18.0, width: ScreenWidth - 10.0, height: 20.0))
        lblExcerpt.text = menuItem.excerpt != nil ? menuItem.excerpt.frenchalize : ""
        lblExcerpt.textColor = Colors.White
        lblExcerpt.textAlignment = .left
        lblExcerpt.font = Fonts.Regular(18.0)
        lblExcerpt.adjustHeight()
        
        self.view.addSubview(lblExcerpt)
        
        // Description
        lblDescription = UILabel(frame: CGRect(x: 16.0, y: lblExcerpt.bottomYPoint + 10.0, width: ScreenWidth - 10.0, height: 20.0))
        lblDescription.text = menuItem.descr != nil ? menuItem.descr.frenchalize : ""
        lblDescription.textColor = Colors.White
        lblDescription.textAlignment = .left
        lblDescription.font = Fonts.Regular(18.0)
        lblDescription.adjustHeight()
        
        self.view.addSubview(lblDescription)
        
        // Price
        lblPrice = UILabel(frame: CGRect(x: ScreenWidth - 110.0, y: lblDescription.bottomYPoint + 60.0, width: 100.0, height: 32.0))
        lblPrice.text = menuItem.price != nil ? "\(menuItem.price!.cleanValue)" : ".-"
        lblPrice.textColor = Colors.White
        lblPrice.textAlignment = .right
        lblPrice.font = Fonts.Bold(30.0)
        
        self.view.addSubview(lblPrice)
        
        // Price description
        lblPriceDescription = UILabel(frame: CGRect(x: lblPrice.frame.origin.x - 300.0, y: lblPrice.bottomYPoint - 18.0, width: 290.0, height: 14.0))
        lblPriceDescription.text = String(format: "details.price.description".localized())
        lblPriceDescription.textColor = Colors.LightGray
        lblPriceDescription.textAlignment = .right
        lblPriceDescription.font = Fonts.Medium(12.0)
        
        self.view.addSubview(lblPriceDescription)
        
        // End of description line
        viewLine = UIView(frame: CGRect(x: 16.0, y: lblPriceDescription.bottomYPoint + 10.0, width: ScreenWidth - 22.0, height: 2.0))
        viewLine.backgroundColor = Colors.LightGray
        
        self.view.addSubview(viewLine)
        
    }
    
    func loadDetailImage(_ id: Int) {
        var image1: UIImage?
        let userDefaults = UserDefaults.standard
        if let imagee1 = userDefaults.object(forKey: "subdetail\(id)") as? Data {
            image1 = UIImage(data: imagee1)
            
        }
        self.imgViewItem.image = image1
    }
    
    
    func cacheDetailImage(_ image: UIImage, id: Int) {
        let userDefaults = UserDefaults.standard
        let data = UIImagePNGRepresentation(image)
        userDefaults.set(data, forKey: "subdetail\(id)")
    }
    
    
}
