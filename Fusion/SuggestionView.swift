//
//  SuggestionView.swift
//  Fusion
//
//  Created by Galin Yonchev on 8/9/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class SuggestionView: UIView {
    
    var lblTitle: UILabel!
    var imgViewItem: UIImageView!
    var lblItemTitle: UILabel!
    var lblItemDescription: UILabel!
    var imgViewFav: FavouriteView!
    var lblFavourites: UILabel!
    var btnDetails: UIButton!
    var lblPrice: UILabel!
    
    var item: MenuItem = MenuItem()
    
    init(frame: CGRect, suggestion: (String, String, UIImage, Int, Double)) {
        super.init(frame: frame)
        
        self.backgroundColor = Colors.Black
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: 10.0, y: 10.0, width: ScreenWidth, height: 22.0))
        lblTitle.text = String(format: "suggestion.title".localized())
        lblTitle.textColor = Colors.GreenOcean
        lblTitle.textAlignment = .left
        // TODO: Set italic font
        lblTitle.font = Fonts.Medium(20.0)
        
        self.addSubview(lblTitle)
        
        // Item image
        let imgItem = UIImage(named: "suggestion_img_example")!
        imgViewItem = UIImageView(frame: CGRect(x: 16.0, y: lblTitle.bottomYPoint + 14.0, width: imgItem.size.width, height: imgItem.size.height))
        imgViewItem.image = suggestion.2
        imgViewItem.backgroundColor = UIColor.clear
        
        self.addSubview(imgViewItem)
        
        // Item title
        lblItemTitle = UILabel(frame: CGRect(x: imgViewItem.mostRightPoint + 40.0, y: imgViewItem.frame.origin.y, width: ScreenWidth - (imgViewItem.mostRightPoint + 40.0) , height: 26.0))
        lblItemTitle.text = suggestion.0
        lblItemTitle.textColor = Colors.White
        lblItemTitle.textAlignment = .left
        lblItemTitle.font = Fonts.Bold(24.0)
        
        self.addSubview(lblItemTitle)
        
        // Item description
        lblItemDescription = UILabel(frame: CGRect(x: lblItemTitle.frame.origin.x, y: lblItemTitle.bottomYPoint + 10.0, width: lblItemTitle.frame.size.width , height: 100.0))
        lblItemDescription.text = suggestion.1
        lblItemDescription.textColor = Colors.White
        lblItemDescription.textAlignment = .left
        lblItemDescription.font = Fonts.Regular(18.0)
        lblItemDescription.numberOfLines = 0
        
        self.addSubview(lblItemDescription)
        
        // Line separator
        let viewLine = UIView(frame: CGRect(x: lblItemTitle.frame.origin.x, y: imgViewItem.bottomYPoint - 26.0, width: lblItemTitle.frame.size.width - 6.0, height: 2.0))
        viewLine.backgroundColor = Colors.LightGray
        
        self.addSubview(viewLine)
        
        // Favourites image
        item = MenuItem()
        item.descr = suggestion.1
        item.title = suggestion.0
        item.realImage = suggestion.2
        item.isWine = true
        item.id = suggestion.3
        item.price = suggestion.4
        for itm in arrFavourites {
            if itm == item {
                item.isFavourite = true
            }
        }
        imgViewFav = FavouriteView(coordinates: CGPoint(x: lblItemTitle.frame.origin.x, y: viewLine.bottomYPoint + 4.0), item: item)
        imgViewFav.frame.size = CGSize(width: imgViewFav.frame.size.width + 12.0, height: imgViewFav.frame.size.height + 12.0)
        
        self.addSubview(imgViewFav)
        
        // Price
        lblPrice = UILabel(frame: CGRect(x: ScreenWidth - 110.0, y: viewLine.frame.origin.y - 20.0, width: 100.0, height: 18.0))
        lblPrice.text = item.price != 0.0 ? "\(item.price!.cleanValue)" : ".-"
        lblPrice.textColor = Colors.White
        lblPrice.textAlignment = .right
        lblPrice.font = Fonts.Bold(16.0)
        
        self.addSubview(lblPrice)
        
        // Add to favourites label
        lblFavourites = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 5.0, y: imgViewFav.frame.origin.y, width: ScreenWidth * 0.4, height: 16.0))
        lblFavourites.text = String(format: "details.add.to.favourites".localized())
        lblFavourites.textColor = Colors.LightGray
        lblFavourites.textAlignment = .left
        lblFavourites.font = Fonts.Medium(14.0)
        lblFavourites.center.y = imgViewFav.center.y
        
        self.addSubview(lblFavourites)
        
        // Details button
        btnDetails = UIButton(type: .custom)
        btnDetails.frame = CGRect(x: self.frame.width - 100.0, y: imgViewFav.frame.origin.y, width: 94.0, height: 30.0)
        let attributes = [
            NSFontAttributeName : Fonts.Medium(16.0),
            NSForegroundColorAttributeName : Colors.GreenOcean,
            NSUnderlineStyleAttributeName : 1
        ] as [String : Any]
        let strButtonTitle = NSMutableAttributedString(string: "suggestion.details".localized(), attributes: attributes)
        btnDetails.setAttributedTitle(strButtonTitle, for: .normal)
        btnDetails.addTarget(self, action: #selector(FavouritesCell.openDetails), for: .touchUpInside)
        btnDetails.titleEdgeInsets = UIEdgeInsets(top: -10.0, left: "language".localized() == "en" ? 23.0 : -22.0, bottom: 0.0, right: 0.0)
        
        // TODO: Add button and open details
        self.addSubview(btnDetails)
        
    }
    
    func update(id: Int) {
        // Favourites image
        item = MenuItem()
        item.id = id
        for itm in arrFavourites {
            if itm == item {
                item.isFavourite = true
            }
        }
        imgViewFav.update(isFav: item.isFavourite)
    }
    
    func openDetails() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let winesVC = storyboard.instantiateViewController(withIdentifier: "WineListViewController") as! WineListViewController
        if self.parentViewController?.navigationController != nil {
            self.parentViewController?.navigationController?.pushViewController(winesVC, animated: true)
        } else {
            winesVC.modalTransitionStyle = .crossDissolve
            winesVC.fromCategory = true
            self.parentViewController?.present(winesVC, animated: true, completion: nil)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
