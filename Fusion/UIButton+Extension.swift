//
//  UIButton+Extension.swift
//  GuessWhat
//
//  Created by Angel Antonov on 7/31/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

// This extension helps wirting TouchUpInside events faster while developing
// Example: myBtn.setAction { println("TouchUpInside event occured.") }

import UIKit

var UIButtonTouchUpInsideHandleString = "UIButtonTouchUpInsideHandleString"

class VoidClosureWrapper {
    var closure: (() -> Void)?
    
    init(_ closure: (() -> Void)?) {
        self.closure = closure
    }
}

extension UIButton {
    var actionToPerform: (() -> Void)? {
        get {
            if let cl = objc_getAssociatedObject(self, &UIButtonTouchUpInsideHandleString) as? VoidClosureWrapper {
                return cl.closure
            }
            return nil
        }
        set {
            objc_setAssociatedObject(self, &UIButtonTouchUpInsideHandleString, VoidClosureWrapper(newValue), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    @available(*, deprecated, message: "Try not to use. In case you use it, make sure you release the actionToPerform with clearAction() method.")
    func setAction(_ action: @escaping () -> Void) {
        actionToPerform = action
        
        self.removeTarget(self, action: #selector(UIButton.performAction), for: UIControlEvents.touchUpInside)
        self.addTarget(self, action: #selector(UIButton.performAction), for: UIControlEvents.touchUpInside)
    }
    
    func performAction() {
        if actionToPerform != nil {
            actionToPerform!()
        }
    }
    
    func clearAction() {
        actionToPerform = nil
    }
    
    func setSizeFromBackgroundImage() {
        self.frame.size = CGSize(width: self.backgroundImage(for: UIControlState())!.size.width, height: self.backgroundImage(for: UIControlState())!.size.height)
    }
    
    func switchImageAndText() {
        let spacing: CGFloat = 3;
        let insetAmount: CGFloat = 0.5 * spacing;
        
        // First set overall size of the button:
        self.contentEdgeInsets = UIEdgeInsetsMake(0, insetAmount, 0, insetAmount);
        self.sizeToFit()
        
        // Then adjust title and image insets so image is flipped to the right and there is spacing between title and image:
        self.titleEdgeInsets   = UIEdgeInsetsMake(0, -self.imageView!.frame.size.width - insetAmount, 0,  self.imageView!.frame.size.width  + insetAmount);
        self.imageEdgeInsets   = UIEdgeInsetsMake(0, self.titleLabel!.frame.size.width + insetAmount, 0, -self.titleLabel!.frame.size.width - insetAmount);
    }
    
    func alignImageAndTitleVertically(_ padding: CGFloat = 6.0) {
        let imageSize = self.imageView!.frame.size
        let titleSize = self.titleLabel!.frame.size
        let totalHeight = imageSize.height + titleSize.height + padding
        
        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageSize.height),
            left: 0,
            bottom: 0,
            right: -titleSize.width
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: -imageSize.width,
            bottom: -(totalHeight - titleSize.height),
            right: 0
        )
    }
}
