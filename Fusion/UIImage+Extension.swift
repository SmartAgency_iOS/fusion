//
//  UIImage+Extension.swift
//  GuessWhat
//
//  Created by Angel Antonov on 7/1/15.
//  Copyright (c) 2015 Angel Antonov. All rights reserved.
//

import UIKit

extension UIImage {
    var rounded: UIImage {
        let imageView = UIImageView(image: self)
        imageView.layer.cornerRadius = size.height < size.width ? size.height/2 : size.width/2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    var circle: UIImage {
        let square = size.width < size.height ? CGSize(width: size.width, height: size.width) : CGSize(width: size.height, height: size.height)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width / 2
        imageView.layer.masksToBounds = true
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result!
    }
    
    func circle(_ radius: CGFloat) -> UIImage {
        let square = CGSize(width: radius, height: radius)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width / 2
        imageView.layer.masksToBounds = true
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result!
    }
    
    func middleRect(_ desiredSize: CGFloat) -> UIImage {
        let square = CGSize(width: desiredSize, height: desiredSize)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = self
        
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        imageView.image = nil
        
        return result!
    }
    
    func resize(_ scale:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width*scale, height: size.height*scale)))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func resizeToWidth(_ width:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func tintWithColor(_ color: UIColor) -> UIImage {
        // Construct new image with the same size as this one.
        var image: UIImage
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0); // 0.0 for scale means "scale for device's main screen".
        
        var rect = CGRect.zero
        rect.size = self.size
        
        // Tint the image
        self.draw(in: rect)
        color.set()
        UIRectFillUsingBlendMode(rect, CGBlendMode.screen)
        
        // Restore alpha channel
        self.draw(in: rect, blendMode: CGBlendMode.destinationIn, alpha: 1)
        
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    func tintWhiteColorWithColor(_ color: UIColor) -> UIImage {
        // Construct new image with the same size as this one.
        var image: UIImage
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0); // 0.0 for scale means "scale for device's main screen".
        
        var rect = CGRect.zero
        rect.size = self.size
        
        // Tint the image
        self.draw(in: rect)
        color.set()
        UIRectFillUsingBlendMode(rect, CGBlendMode.sourceIn)
        
        // Restore alpha channel
        self.draw(in: rect, blendMode: CGBlendMode.destinationIn, alpha: 1)
        
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    class func sizeOfImage(_ strImageName: String!) -> CGSize {
        guard let img = UIImage(named: strImageName) else {
            return CGSize(width: 0.0, height: 0.0)
        }
        
        return img.size
    }
}
