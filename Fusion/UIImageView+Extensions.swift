//
//  UIImageView+Extensions.swift
//  GuessWhat
//
//  Created by Angel Antonov on 4/11/16.
//  Copyright © 2016 SmartInteractive. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    class func imageViewWith(_ imageName: String) -> UIImageView {
        let img = UIImage(named: imageName)!
        let imgView = UIImageView(image: img)
        imgView.frame = CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height)
        
        return imgView
    }
}
