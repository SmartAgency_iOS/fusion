//
//  UILabel+Extensions.swift
//  GuessWhat
//
//  Created by iHustle on 2/17/16.
//  Copyright © 2016 iHome. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func adjustHeight() {
        self.numberOfLines = 0
        let newSize = self.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        
        self.frame.size.height = newSize.height
    }
    
    func adjustWidth() {
        self.numberOfLines = 0
        let newSize = self.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: self.frame.size.height))
        
        self.frame.size.width = newSize.width
    }
    
    var neededWidth: CGFloat {
        let currentNumberOfLines = self.numberOfLines
        
        self.numberOfLines = 0
        let neededSize = self.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: self.frame.size.height))
        self.numberOfLines = currentNumberOfLines
        
        return neededSize.width
    }
    
    var neededHeight: CGFloat {
        let currentNumberOfLines = self.numberOfLines
        
        self.numberOfLines = 0
        let neededSize = self.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        self.numberOfLines = currentNumberOfLines
        
        return neededSize.height
    }
    
    func fullRange() -> NSRange {
        return NSMakeRange(0, (text ?? "").characters.count)
    }
    
    // MARK: Range Formatter
    
    func setTextColor(_ color: UIColor, range: NSRange?) {
        guard let range = range else { return }
        let text = mutableAttributedString()
        text.addAttribute(NSForegroundColorAttributeName, value: color, range: range)
        attributedText = text
    }
    
    func setFont(_ font: UIFont, range: NSRange?) {
        guard let range = range else { return }
        let text = mutableAttributedString()
        text.addAttribute(NSFontAttributeName, value: font, range: range)
        attributedText = text
    }
    
    func setTextUnderline(_ color: UIColor, range: NSRange?) {
        setTextUnderline(color, range: range, byWord: false)
    }
    
    func setTextUnderline(_ color: UIColor, range: NSRange?, byWord: Bool) {
        guard let range = range else { return }
        let text = mutableAttributedString()
        var style = NSUnderlineStyle.styleSingle.rawValue
        if byWord { style = style | NSUnderlineStyle.byWord.rawValue }
        text.addAttribute(NSUnderlineStyleAttributeName, value: NSNumber(value: style as Int), range: range)
        text.addAttribute(NSUnderlineColorAttributeName, value: color, range: range)
        attributedText = text
    }
    
    func setTextWithoutUnderline(range: NSRange?) {
        guard let range = range else { return }
        let text = mutableAttributedString()
        text.removeAttribute(NSUnderlineStyleAttributeName, range: range)
        attributedText = text
    }
    
    // MARK: String Formatter
    
    func rangeOf(_ string: String) -> NSRange? {
        let range = NSString(string: text ?? "").range(of: string)
        return range.location != NSNotFound ? range : nil
    }
    
    func setTextColor(_ color: UIColor, string: String) {
        setTextColor(color, range: rangeOf(string))
    }
    
    func setFont(_ font: UIFont, string: String) {
        setFont(font, range: rangeOf(string))
    }
    
    func setTextUnderline(_ color: UIColor, string: String) {
        setTextUnderline(color, range: rangeOf(string))
    }
    
    func setTextUnderline(_ color: UIColor, string: String, byWord: Bool) {
        setTextUnderline(color, range: rangeOf(string), byWord: byWord)
    }
    
    func setTextWithoutUnderline(string: String) {
        setTextWithoutUnderline(range: rangeOf(string))
    }
        
    // MARK: Helpers
    
    fileprivate func mutableAttributedString() -> NSMutableAttributedString {
        if attributedText != nil {
            return NSMutableAttributedString(attributedString: attributedText!)
        } else {
            return NSMutableAttributedString(string: text ?? "")
        }
    }
}
