//
//  WineCell.swift
//  Fusion
//
//  Created by Admin on 8/12/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class WineCell: UITableViewCell {
    
    var imgViewFav: FavouriteView!
    var lblTitle: UILabel!
    var lblSmallPrice: UILabel!
    var lblBigPrice: UILabel!
    
    init(withSort: String) {
        super.init(style: UITableViewCellStyle.default, reuseIdentifier: "WineSubsectionCell")
        self.backgroundColor = Colors.Dark
        self.selectionStyle = .none
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: 30.0, y: WineCell.cellHeight - 30.0/*WineCell.cellHeight /^ 22.0*/, width: ScreenWidth * 0.5, height: 22.0))
        lblTitle.text = withSort
        lblTitle.font = Fonts.Medium(20.0)
        lblTitle.textAlignment = .left
        lblTitle.textColor = Colors.LighterGray
        lblTitle.adjustsFontSizeToFitWidth = true
        
        self.addSubview(lblTitle)
        
        // Big Price
        lblBigPrice = UILabel(frame: CGRect(x: ScreenWidth - 30.0 - (ScreenWidth * 0.18), y: WineCell.cellHeight - 30.0, width: ScreenWidth * 0.18, height: 15.0))
        lblBigPrice.text = String(format: "wine.bottle.price".localized())
        lblBigPrice.font = Fonts.Medium(13.0)
        lblBigPrice.textAlignment = .right
        lblBigPrice.textColor = Colors.LighterGray
        
        self.addSubview(lblBigPrice)
        
        // Separator
        let viewLine = UIView(frame: CGRect(x: 30.0, y: WineCell.cellHeight - 1.0, width: ScreenWidth - 60.0, height: 1.0))
        viewLine.backgroundColor = Colors.LighterGray
        
        self.addSubview(viewLine)
        
    }
    
    init(fromCountry: String) {
        super.init(style: UITableViewCellStyle.default, reuseIdentifier: "WineSubsectionCell")
        self.backgroundColor = Colors.Dark
        self.selectionStyle = .none
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: 30.0, y: WineCell.cellHeight - 30.0, width: ScreenWidth * 0.5, height: 22.0))
        lblTitle.text = fromCountry
        lblTitle.font = Fonts.Medium(20.0)
        lblTitle.textAlignment = .left
        lblTitle.textColor = Colors.LighterGray
        
        self.addSubview(lblTitle)
        
        // Big Price
        // Small price
        lblBigPrice = UILabel(frame: CGRect(x: ScreenWidth - 30.0 - (ScreenWidth * 0.18), y: WineCell.cellHeight - 30.0, width: ScreenWidth * 0.18, height: 15.0))
        lblBigPrice.text = "wine.big.price".localized()
        lblBigPrice.font = Fonts.Medium(13.0)
        lblBigPrice.textAlignment = .right
        lblBigPrice.textColor = Colors.LighterGray
        
        if fromCountry == "wine.france".localized {
            self.addSubview(lblBigPrice)
        }
        
        // Small price
        lblSmallPrice = UILabel(frame: CGRect(x: lblBigPrice.frame.origin.x - (ScreenWidth * 0.18), y: WineCell.cellHeight - 30.0, width: ScreenWidth * 0.18, height: 15.0))
        lblSmallPrice.text = "wine.small.price".localized
        lblSmallPrice.font = Fonts.Medium(13.0)
        lblSmallPrice.textAlignment = .right
        lblSmallPrice.textColor = Colors.LighterGray
        
        if fromCountry == "wine.france".localized {
            self.addSubview(lblSmallPrice)
        }
        
        // Separator
        let viewLine = UIView(frame: CGRect(x: 30.0, y: WineCell.cellHeight - 1.0, width: ScreenWidth - 60.0, height: 1.0))
        viewLine.backgroundColor = Colors.LighterGray
        
        self.addSubview(viewLine)
    }
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, wineItem: WineItem) {
        super.init(style: style, reuseIdentifier: "WineCell")
        self.backgroundColor = Colors.Dark
        self.selectionStyle = .none
        
        // Add to favourites
        // TODO: Set isFavourite real value
        let item = MenuItem()
        item.title = wineItem.title
        item.titleEN = wineItem.titleEN
        item.price = wineItem.bottlePrice != nil ? wineItem.bottlePrice : 0.0
        item.id = wineItem.id
        item.isWine = true
        item.isFavourite = wineItem.isFavourite
        imgViewFav = FavouriteView(coordinates: CGPoint(x: 30.0, y: 4.0 + (WineCell.cellHeight /^ 22.0)), item: item)
        imgViewFav.frame.size = CGSize(width: imgViewFav.frame.size.width + 12.0, height: imgViewFav.frame.size.height + 12.0)
        
        self.addSubview(imgViewFav)
        
        // Title
        lblTitle = UILabel(frame: CGRect(x: imgViewFav.mostRightPoint + 5.0, y: WineCell.cellHeight /^ 42.0, width: ScreenWidth * 0.5, height: 42.0))
        lblTitle.numberOfLines = 2
        var cellar = ""
        if language == "en" {
            cellar = wineItem.cellarEN != nil ? ", \(wineItem.cellarEN!)" : ""
            lblTitle.text = wineItem.titleEN != nil ? wineItem.titleEN + cellar.frenchalize : ""
        } else {
            cellar = wineItem.cellar != nil ? ", \(wineItem.cellar!.frenchalize)" : ""
            lblTitle.text = wineItem.title != nil ? wineItem.title.frenchalize + cellar.frenchalize : ""
        }
        lblTitle.font = Fonts.Medium(20.0)
        lblTitle.textAlignment = .left
        lblTitle.textColor = Colors.White
        lblTitle.setLineHeight(1.0)
        if wineItem.title != nil {
            lblTitle.setFont(Fonts.Regular(20.0), string: cellar)
        }
        
        self.addSubview(lblTitle)
        
        imgViewFav.center.y = lblTitle.center.y - 3.0
        
        // Big Price
        lblBigPrice = UILabel(frame: CGRect(x: ScreenWidth - 30.0 - (ScreenWidth * 0.18), y: WineCell.cellHeight /^ 22.0, width: ScreenWidth * 0.18, height: 22.0))
        if wineItem.category == 13 {
            lblBigPrice.text = wineItem.price != nil ? "\(wineItem.price!.cleanValue)" : ""
        } else {
            lblBigPrice.text = wineItem.glassPrice != nil ? "\(wineItem.glassPrice!.cleanValue)" : ""
        }
        lblBigPrice.font = Fonts.Medium(20.0)
        lblBigPrice.textAlignment = .right
        lblBigPrice.textColor = Colors.White
        
        self.addSubview(lblBigPrice)
        
        // Small price
        lblSmallPrice = UILabel(frame: CGRect(x: lblBigPrice.frame.origin.x - (ScreenWidth * 0.18), y: WineCell.cellHeight /^ 22.0, width: ScreenWidth * 0.18, height: 22.0))
        if wineItem.category == 17 {
            lblSmallPrice.text = wineItem.bottlePrice != nil ? "\(wineItem.bottlePrice!.cleanValue)" : ""
            self.addSubview(lblSmallPrice)
        }
        lblSmallPrice.font = Fonts.Medium(20.0)
        lblSmallPrice.textAlignment = .right
        lblSmallPrice.textColor = Colors.White
        
        // Separator
        let viewLine = UIView(frame: CGRect(x: 30.0, y: WineCell.cellHeight - 1.0, width: ScreenWidth - 60.0, height: 1.0))
        viewLine.backgroundColor = Colors.White
        
        self.addSubview(viewLine)
    }
    
    class var cellHeight: CGFloat {
        return 80.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


extension UILabel {
    func setLineHeight(_ lineHeight: CGFloat) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 6.0
        paragraphStyle.lineHeightMultiple = lineHeight
        paragraphStyle.alignment = self.textAlignment
        
        let attrString = NSMutableAttributedString(string: self.text!)
        attrString.addAttribute(NSFontAttributeName, value: self.font, range: NSMakeRange(0, attrString.length))
        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}
