//
//  WineItem.swift
//  Fusion
//
//  Created by Admin on 8/19/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit

class WineItem {
    
    var bottlePrice: Double!
    var glassPrice: Double!
    var price: Double!
    var category: Int!
    var cellar: String!
    var cellarEN: String!
    var colour: Int!
    var country: Int!
    var id: Int!
    var image: Photo!
    var title: String!
    var titleEN: String!
    var isFavourite: Bool = false
    
    class func parse(_ dictWine: [String: AnyObject]) -> WineItem {
        let wineItem = WineItem()
        
        if isNotNull(dictWine[WS.Params.bottlePrice]) {
            if let price = dictWine[WS.Params.bottlePrice] as? Int {
                wineItem.bottlePrice = Double(price)
            } else if let price = dictWine[WS.Params.bottlePrice] as? Double {
                wineItem.bottlePrice = price
            } else if let price = dictWine[WS.Params.bottlePrice] as? String {
                wineItem.bottlePrice = Double(price)
            }
        }
        
        if isNotNull(dictWine[WS.Params.glassPrice]) {
            if let price = dictWine[WS.Params.glassPrice] as? Int {
                wineItem.glassPrice = Double(price)
            } else if let price = dictWine[WS.Params.glassPrice] as? Double {
                wineItem.glassPrice = price
            } else if let price = dictWine[WS.Params.glassPrice] as? String {
                wineItem.glassPrice = Double(price)
            }
        }
        
        if isNotNull(dictWine[WS.Params.price]) {
            if let price = dictWine[WS.Params.price] as? Int {
                wineItem.price = Double(price)
            } else if let price = dictWine[WS.Params.price] as? Double {
                wineItem.price = price
            } else if let price = dictWine[WS.Params.price] as? String {
                wineItem.price = Double(price)
            }
        }
        
        if isNotNull(dictWine[WS.Params.category]) {
            wineItem.category = dictWine[WS.Params.category] as! Int
        }
        
        if isNotNull(dictWine[WS.Params.cellar]) {
            wineItem.cellar = dictWine[WS.Params.cellar] as! String
        }
        
        if isNotNull(dictWine[WS.Params.cellarEN]) {
            wineItem.cellarEN = dictWine[WS.Params.cellarEN] as! String
        }
        
        if isNotNull(dictWine[WS.Params.color]) {
            wineItem.colour = dictWine[WS.Params.color] as! Int
        }
        
        if isNotNull(dictWine[WS.Params.country]) {
            wineItem.country = dictWine[WS.Params.country] as! Int
        }
        
        if isNotNull(dictWine[WS.Params.id]) {
            wineItem.id = dictWine[WS.Params.id] as! Int
        }
        
        if isNotNull(dictWine[WS.Params.title]) {
            wineItem.title = dictWine[WS.Params.title] as! String
        }
        
        if isNotNull(dictWine[WS.Params.titleEN]) {
            wineItem.titleEN = dictWine[WS.Params.titleEN] as! String
        }
        
        if isNotNull(dictWine[WS.Params.image]) {
            if let url = dictWine[WS.Params.image] as? String {
                wineItem.image = Photo.parseURL(url)
            }
        }
        
        return wineItem
    }
    
}
