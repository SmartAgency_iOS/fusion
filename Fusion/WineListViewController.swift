//
//  WineListViewController.swift
//  Fusion
//
//  Created by Admin on 8/12/16.
//  Copyright © 2016 Smart Interactive. All rights reserved.
//

import UIKit
import Alamofire

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class WineListViewController: NavigationExtensions, UITableViewDelegate, UITableViewDataSource, CategoryDelegate {
    
    var tableView: UITableView!
    var category = SelectedCategory.Wines
    var arrWines: [WineItem] = [WineItem]()
    var headerView: CategoryHeaderView!
    
    var arrBigSized = [WineItem]()
    var bigRedCount = 0
    var bigWhiteCount = 0
    var bigRoseCount = 0
    
    var arrRedWines = [WineItem]()
    var redItalyCount = 0
    var redSwissCount = 0
    var redFranceCount = 0
    
    var arrWhiteAndRoseWines = [WineItem]()
    var wrItalyCount = 0
    var wrSwissCount = 0
    var wrFranceCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.Black
        self.customNavigationBarWith(menuButton: true)
        self.addBackButton()
        
        //self.getItems()
        
        // First table
        tableView = UITableView(frame: CGRect(x: 0.0, y: Constants.NavBarHeight, width: ScreenWidth, height: ScreenHeight - Constants.NavBarHeight), style: .grouped)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = Colors.Dark
        tableView.separatorStyle = .none
        tableView.bounces = false
        headerView = CategoryHeaderView(selectedCategory: category)
        headerView.delegate = self
        tableView.tableHeaderView = headerView
        
        self.view.addSubview(tableView)
        
        //self.loadCachedImages()
        
        // Test - load cache before API call
//        let settings = self.loadCachedData().0
//        let items = self.loadCachedData().1
//        self.setCachedItems(settings, items: items)
        
        self.loadData { 
            // print
        }
    }
    
    func getItems() {
        arrWines.removeAll()
        
        var set = [String: AnyObject]()
        var it = [[String: AnyObject]]()
        
        let language: String = "language".localized()
        let params = ["lang" : language] as [String: String]
        Alamofire.request(URL.RestServer + OperationType.WINES.rawValue, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = validateResponse(response) {
                print(result)
                // get category
                if let settings = result["settings"] as? [String: AnyObject] {
                    set = settings
                    let menuCategory = Category.parse(settings)
                    self.headerView.lblDescription.text = menuCategory.descr != nil ? menuCategory.descr : ""
                    self.headerView.lblSubtitle.text = menuCategory.subtitle != nil ? menuCategory.subtitle : ""
                    self.headerView.lblSubtitle.adjustsFontSizeToFitWidth = true
                    menuCategory.photo1?.getImage({ (downloadedImage1) in
                        menuCategory.photo2?.getImage({ (downloadedImage2) in
                            self.headerView.imgViewPortrait.image = downloadedImage1
                            self.headerView.imgViewLandscape.image = downloadedImage2
                            if downloadedImage1 != nil && downloadedImage2 != nil {
                                self.cacheImages(downloadedImage1!, image2: downloadedImage2!)
                            }
                        })
                    })
                }
                // get items
                if let items = result["items"] as? [[String: AnyObject]] {
                    it = items
                    for item in items {
                        
                        let wineItem = WineItem.parse(item)
                        // Check if is in favourites
                        for fav in arrFavourites {
                            let menuItem = MenuItem()
                            menuItem.id = wineItem.id
                            menuItem.title = wineItem.title
                            menuItem.price = wineItem.bottlePrice
                            if menuItem == fav {
                                wineItem.isFavourite = true
                            } else if menuItem.title == fav.title {
                                wineItem.isFavourite = true
                            }
                        }
                        self.arrWines.append(wineItem)
                    }
                    self.tableView.reloadData()
                }
                self.cacheData(set, items: it)
            } else {
                let settings = self.loadCachedData().0
                let items = self.loadCachedData().1
                self.setCachedItems(settings, items: items)
            }

        }
    }
    
    func setCachedItems(_ settings: [String: AnyObject], items: [[String: AnyObject]]) {
        let menuCategory = Category.parse(settings)
        self.headerView.lblDescription.text = menuCategory.descr != nil ? menuCategory.descr : ""
        self.headerView.lblSubtitle.text = menuCategory.subtitle != nil ? menuCategory.subtitle : ""
        self.headerView.lblSubtitle.adjustsFontSizeToFitWidth = true
//        menuCategory.photo1?.getImage({ (downloadedImage1) in
//            menuCategory.photo2?.getImage({ (downloadedImage2) in
//                self.headerView.imgViewPortrait.image = downloadedImage1
//                self.headerView.imgViewLandscape.image = downloadedImage2
//            })
//        })
        // get all items
        print(items)
        for item in items {
            // TODO: Check if favourite
            let wineItem = WineItem.parse(item)
            for fav in arrFavourites {
                let menuItem = MenuItem()
                menuItem.id = wineItem.id
                menuItem.title = wineItem.title
                menuItem.price = wineItem.bottlePrice
                if menuItem == fav {
                    wineItem.isFavourite = true
                }
            }
            self.arrWines.append(wineItem)
        }
        self.tableView.reloadData()
    }
    
    func cacheImages(_ image1: UIImage, image2: UIImage) {
        let userDefaults = UserDefaults.standard
        let data1 = UIImagePNGRepresentation(image1)
        let data2 = UIImagePNGRepresentation(image2)
        userDefaults.set(data1, forKey: "wines-image-portrait")
        userDefaults.set(data2, forKey: "wines-image-landscape")
    }
    
    func loadCachedImages() {
        var image1: UIImage?
        var image2: UIImage?
        let userDefaults = UserDefaults.standard
        if let imagee1 = userDefaults.object(forKey: "wines-image-portrait") as? Data {
            image1 = UIImage(data: imagee1)
            self.headerView.imgViewPortrait.image = image1
        }
        if let imagee2 = userDefaults.object(forKey: "wines-image-landscape") as? Data {
            image2 = UIImage(data: imagee2)
            self.headerView.imgViewLandscape.image = image2
        }
    }
    
    func loadCachedData() -> ([String: AnyObject], [[String: AnyObject]]) {
        let userDefaults = UserDefaults.standard
        var settings = [String: AnyObject]()
        var items = [[String: AnyObject]]()

        if let set = userDefaults.object(forKey: "wines-settings") as? [String: AnyObject] {
            settings = set
        }
        if let it = userDefaults.object(forKey: "wines-items") as? [[String: AnyObject]] {
            items = it
        }
        return (settings, items)
    }
    
    func cacheData(_ settings: [String: AnyObject], items: [[String: AnyObject]]) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(settings, forKey: "wines-settings")
        userDefaults.set(items, forKey: "wines-items")
        userDefaults.synchronize()
    }
    
    func slide() {
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Header
        let sectionHeader = UIView(frame: CGRect(x: 30.0, y: 0.0, width: ScreenWidth - 60.0, height: 100.0))
        
        // Header title
        let lblSectionTitle = UILabel(frame: CGRect(x: 30.0, y: 70.0, width: ScreenWidth - 60.0, height: 24.0))
        lblSectionTitle.font = Fonts.Medium(22.0)
        lblSectionTitle.textAlignment = .left
        lblSectionTitle.textColor = Colors.GreenOcean
        
        sectionHeader.addSubview(lblSectionTitle)
        
        // Separator
        let viewLine = UIView(frame: CGRect(x: 30.0, y: sectionHeader.frame.size.height - 3.0, width: ScreenWidth - 60.0, height: 2.0))
        viewLine.backgroundColor = Colors.White
        
        sectionHeader.addSubview(viewLine)
        
        switch section {
        case 0:
            lblSectionTitle.text = String(format: "wines.grands".localized())
        case 1:
            lblSectionTitle.text = String(format: "wines.blanc".localized())
        case 2:
            lblSectionTitle.text = String(format: "wines.rouges".localized())
        default:
            return sectionHeader
        }
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 100.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).section == 0 {
            return self.getBigSizedBottlesCell((indexPath as NSIndexPath).row)
        } else if (indexPath as NSIndexPath).section == 1 {
            return self.getWhiteAndRoseCell((indexPath as NSIndexPath).row)
        } else {
            return self.getRedWinesCell((indexPath as NSIndexPath).row)
        }
    }
    
    // Return exact cell for White and Rose wines
    func getWhiteAndRoseCell(_ index: Int) -> WineCell {
        var cell: WineCell
        
        if index == 0 {
            cell = WineCell(fromCountry: "wine.italy".localized())//"wine.france".localized())
        } else if arrWhiteAndRoseWines[index].id == 222 {
            cell = WineCell(fromCountry: "wine.switzerland".localized())//"wine.italy".localized())
        } else if arrWhiteAndRoseWines[index].id == 333 {
            cell = WineCell(fromCountry: "wine.france".localized())//"wine.switzerland".localized())
        } else {
            cell = WineCell(style: .default, reuseIdentifier: "WineCell", wineItem: arrWhiteAndRoseWines[index])
        }
        
        return cell
    }
    
    // Return exact cell for Red wines
    func getRedWinesCell(_ index: Int) -> WineCell {
        var cell: WineCell
        
        if index == 0 {
            cell = WineCell(fromCountry: "wine.italy".localized())//"wine.france".localized())
        } else if arrRedWines[index].id == 222 {
            cell = WineCell(fromCountry: "wine.switzerland".localized())//"wine.italy".localized())
        } else if arrRedWines[index].id == 333 {
            cell = WineCell(fromCountry: "wine.france".localized())//"wine.switzerland".localized())
        } else {
            cell = WineCell(style: .default, reuseIdentifier: "WineCell", wineItem: arrRedWines[index])
        }
        
        return cell
    }
    
    // Return exact cell for Big sized bottle wines
    func getBigSizedBottlesCell(_ index: Int) -> WineCell {
        var cell: WineCell
        
        if index == 0 {
            cell = WineCell(withSort: "wine.white".localized())
        } else if arrBigSized[index].id == 222 {
            cell = WineCell(withSort: "wine.rose".localized())
        } else if arrBigSized[index].id == 333 {
            cell = WineCell(withSort: "wine.red".localized())
        } else {
            cell = WineCell(style: .default, reuseIdentifier: "WineCell", wineItem: arrBigSized[index])
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.getBigSizedBottles()
        case 1:
            return self.getRoseAndWhiteWines()
        case 2:
            return self.getRedWines()
        default:
            return 5
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return WineCell.cellHeight
    }
    
    
    func getRoseAndWhiteWines() -> Int {
        arrWhiteAndRoseWines.removeAll()
        wrFranceCount = 0
        wrItalyCount = 0
        wrSwissCount = 0
        
        // break down items by color
        for item in arrWines {
            if item.category == 17 && (item.colour == 14 || item.colour == 15) {
                arrWhiteAndRoseWines.append(item)
                if item.country == 20 {
                    wrFranceCount += 1
                } else if item.country == 18 {
                    wrItalyCount += 1
                } else { // item.country == 19
                    wrSwissCount += 1
                }
            }
        }

        // sort array white, rose, red
        arrWhiteAndRoseWines = arrWhiteAndRoseWines.sorted { (item1, item2) -> Bool in
            return item1.country < item2.country
        }
        
        // create empty wine items with ID and insert in array with real items
        let firstSectionItem = WineItem()
        firstSectionItem.id = 111
        
        let secondSectionItem = WineItem()
        secondSectionItem.id = 222
        
        let thirdSectionItem = WineItem()
        thirdSectionItem.id = 333
        
//        // calculate section index in array
//        let secondSection = 1 + wrFranceCount
//        let thirdSection = 1 + wrFranceCount + 1 + wrItalyCount

        // calculate section index in array
        let secondSection = 1 + wrItalyCount
        let thirdSection = 1 + wrItalyCount + 1 + wrSwissCount

        
        // insert empty wine items with ID to know where to put subsection header
        arrWhiteAndRoseWines.insert(firstSectionItem, at: 0)
        arrWhiteAndRoseWines.insert(secondSectionItem, at: secondSection)
        arrWhiteAndRoseWines.insert(thirdSectionItem, at: thirdSection)
        
        return arrWhiteAndRoseWines.count

    }
    
    
    
    func getRedWines() -> Int {
        
        arrRedWines.removeAll()
        redFranceCount = 0
        redItalyCount = 0
        redSwissCount = 0
        
        // break down items by color
        for item in arrWines {
            if item.category == 17 && item.colour == 16 {
                arrRedWines.append(item)
                if item.country == 20 {
                    redFranceCount += 1
                } else if item.country == 18 {
                    redItalyCount += 1
                } else { // item.country == 19
                    redSwissCount += 1
                }
            }
        }
        
        // sort array white, rose, red
        arrRedWines = arrRedWines.sorted { (item1, item2) -> Bool in
            return item1.country < item2.country
        }
        
        // create empty wine items with ID and insert in array with real items
        let firstSectionItem = WineItem()
        firstSectionItem.id = 111
        
        let secondSectionItem = WineItem()
        secondSectionItem.id = 222
        
        let thirdSectionItem = WineItem()
        thirdSectionItem.id = 333
        
//        // calculate section index in array
//        let secondSection = 1 + redFranceCount
//        let thirdSection = 1 + redFranceCount + 1 + redItalyCount
        
        // calculate section index in array
        let secondSection = 1 + redItalyCount
        let thirdSection = 1 + redItalyCount + 1 + redSwissCount
        
        // insert empty wine items with ID to know where to put subsection header
        arrRedWines.insert(firstSectionItem, at: 0)
        arrRedWines.insert(secondSectionItem, at: secondSection)
        arrRedWines.insert(thirdSectionItem, at: thirdSection)
        
        return arrRedWines.count
    }
    
    
    func getBigSizedBottles() -> Int {
        
        arrBigSized.removeAll()
        
        bigWhiteCount = 0
        bigRedCount = 0
        bigRedCount = 0

        // break down items by color
        for item in arrWines {
            if item.category == 13 {
                arrBigSized.append(item)
                if item.colour == 14 {
                    bigWhiteCount += 1
                } else if item.colour == 15 {
                    bigRoseCount += 1
                } else if item.colour == 16 {
                    bigRedCount += 1
                }
            }
        }
        
        // sort array white, rose, red
         arrBigSized = arrBigSized.sorted { (item1, item2) -> Bool in
         return item1.colour > item2.colour
         }

        // create empty wine items with ID and insert in array with real items
        let firstSectionItem = WineItem()
        firstSectionItem.id = 111
        
        let secondSectionItem = WineItem()
        secondSectionItem.id = 222
        
        let thirdSectionItem = WineItem()
        thirdSectionItem.id = 333
        
        // calculate section index in array
        let secondSection = 1 + bigWhiteCount
        let thirdSection = 1 + bigRoseCount + 1 + bigWhiteCount
        
        // insert empty wine items with ID to know where to put subsection header
        arrBigSized.insert(firstSectionItem, at: 0)
        arrBigSized.insert(secondSectionItem, at: secondSection)
        arrBigSized.insert(thirdSectionItem, at: thirdSection)
        
        return arrBigSized.count
    }
    
    func loadData(success: (() -> Void)) {
        var itemsArray = [[String: AnyObject]] ()
        var settingsDict = [String: AnyObject]()
        var imagesDict = [String: UIImage]()
        var image1 = UIImage()
        var image2 = UIImage()
        
        if let items = UserDefaults.standard.object(forKey: "wines-items") as? [[String: AnyObject]] {
            itemsArray = items
            imagesDict = dictWinesImages
        }
        if let settings = UserDefaults.standard.object(forKey: "wines-settings") as? [String: AnyObject] {
            settingsDict = settings
//            image1 = dictWinesImages["photo1"] != nil ? dictWinesImages["photo1"]! : UIImage()
//            image2 = dictWinesImages["photo2"] != nil ? dictWinesImages["photo2"]! : UIImage()
        }
        
        let menuCategory = Category.parse(settingsDict)
        self.headerView.lblDescription.text = language == "fr" ? menuCategory.descr.frenchalize : menuCategory.descrEN
        self.headerView.lblSubtitle.text = language == "fr" ? menuCategory.subtitle.frenchalize : menuCategory.subtitleEN
        self.headerView.lblSubtitle.adjustsFontSizeToFitWidth = true
        
        if let img1 = menuCategory.photo1?.getImage({ (image) in
            if image != nil {
                self.headerView.imgViewPortrait.image = image
            }
        }) {
            self.headerView.imgViewPortrait.image = img1
        }
        
        if let img2 = menuCategory.photo2?.getImage({ (image) in
            if image != nil {
                self.headerView.imgViewLandscape.image = image
            }
        }) {
            self.headerView.imgViewLandscape.image = img2
        }
        
//        self.headerView.imgViewPortrait.image = image1
//        self.headerView.imgViewLandscape.image = image2

        for item in itemsArray {
            let wineItem = WineItem.parse(item)
            // Check if is in favourites
            for fav in arrFavourites {
                let menuItem = MenuItem()
                menuItem.id = wineItem.id
                menuItem.title = wineItem.title
                menuItem.titleEN = wineItem.titleEN
                menuItem.price = wineItem.bottlePrice
                if menuItem == fav {
                    wineItem.isFavourite = true
                } else if menuItem.title == fav.title {
                    wineItem.isFavourite = true
                }
            }
            self.arrWines.append(wineItem)
        }
        
        self.tableView.reloadData()
        
    }
    
}
// get items
